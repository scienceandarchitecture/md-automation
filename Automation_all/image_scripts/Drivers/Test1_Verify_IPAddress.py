
import csv
import datetime
import time
from Common_lib import *
from dev_function import *
import pandas as pd
import json
#import pytest
import re
import sys

# import Config_Parser
def addOutput(filepath,op, NMS_Param, tc, test_status):
	outp_file = open(filepath, 'a+')
	outp = csv.writer(outp_file)
	outp.writerow(["Test_No: "+str(tc)])
	outp.writerow(["Device output"+':'+op])
	outp.writerow(["NMS output"+':'+NMS_Param])
	outp.writerow(["Test Execution Status"+':'+test_status])
	outp_file.close()
	return

def Input_Data_Parser(tc):
	df = pd.read_csv("..//API//GeneralConfiguration.csv")
	tc = 0		
	API_Name = df.API_Name[tc]	
	Method = df.Method[tc]	
	Content_Type = df.Content_Type[tc]	
	arg1= df.arg1[tc]	
	Exp_st_code = df.Exp_st_code[tc]	
	Precondition = df.Precondition[tc]	
	Supported_protocol = df.Supported_protcol[tc]
	Interface_Name = df.Interface_Name[tc]
	Network_Name = df.Network_Name[tc]
	API_Server_IP = df.API_Server_IP[tc]

	test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, Supported_protocol, Interface_Name, Network_Name, API_Server_IP]
				 
	return test_params

def Setup_Parser_method(tc):
	count = 0
	while count<= tc:
		if count == tc:
			data = pd.read_csv("..//Data//Test1_Setup.csv")

			NAME = data.NAME[count]
			MAC = data.MAC[count]
			Dev_IP =  data.IP[count]
			TYPE = data.TYPE[count]
			MODEL = data.MODEL[count]
			NODE_USER = data.USER_NAME[count]
			setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
			return setup_values
		count = count+1

class Test1_Verify_IPAddress(object):
	def init(self):
		self.object = object

	def test1_Verify_IPAddress(self, tc):
		#try:
			val = Setup_Parser_method(tc)
			NAME = val[0]
			Dev_MAC_Address = val[1]
			NMS_Device_IP = val[2]
			Type = val[3]
			Model = val[4]
			Node_User= val[5]
	
			api_data = Input_Data_Parser(tc)
			api_data[0]
			API_Name = api_data[0]
			Method = api_data[1]
			Content_Type = api_data[2]
			Arg1 = api_data[3]
			Exp_st_code= api_data[4]
			Precondition = api_data[5]
			Supported_protocol= api_data[6]
			Interface_Name = api_data[7]
			Network_Name = api_data[8]
			API_Server_IP= api_data[9]
		
			#http://localhost:8080/NMS/getGeneralConfiguration?networkName=default&macAddress=30:14:4A:D8:89:49
		
			url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?interfaceName="+Interface_Name+"&networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
			print url
			Verif_para = "NMS_IP"
			Dev_IP= NMS_Device_IP 
			print "Test Details: "
			#print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
			s= CommonOperations()
			tc_strt = datetime.datetime.now()			
			p=s.commonOperations(tc, Method, url, Content_Type, Arg1, Exp_st_code, Precondition, Supported_protocol, Verif_para, Interface_Name, Network_Name, API_Server_IP) 
			op = json.dumps(p,ensure_ascii=True)
			op1 = json.loads(op)
			print "Request's response is : ",op	
			NMS_API_IP = op1["ipAddress"]
			print "IP Address from API is :", NMS_API_IP
			Test_output = "IP Address from API is : "+ str(NMS_API_IP)
			current_time = time.localtime()
			dev_Logfilename = "Test1_Verify_IPAddress_device_output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
			dev_filepath = "..//Output//"+dev_Logfilename
	                test_status_NMS = "NMS Verification completed..."
			addOutput(dev_filepath, op, Test_output, tc, test_status_NMS)	
			
			#nms_file = name = "Test1_Verify_IPAddress_NMS_output"+filename
			#nms_filepath = "..//Output//"+nms_filename+'.txt'
		        #nms_log = open(dev_filepath, 'a')
			#nms_log.write(op)
			#nms_log.close()
			print "NMS output saved in logfile "
			print "						   \n"
			""" Verification of Device Data """
			print "Device Data Verification Starts Now.."	
			#dev_Logfilename = "Test1_Verify_IPAddress_device_output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
			#dev_filepath = ".//Output//"+dev_Logfilename
	                
			telnet_user = "root"
			t = dev_function()
			print NMS_API_IP
			Node_ipconfig= t.get_Ifconfig(Node_User, dev_filepath, NMS_API_IP)
			print Node_ipconfig
		
			for line in Node_ipconfig.splitlines():
			#print line
			#  mat1=re.search(r'(HWaddr)(.*)',line)
				mat2=re.search(r'(inet addr)\:(([0-9]+){1,3}\.([0-9]+){1,3}\.([0-9]+){1,3}\.\d{1,3})',line)
		# 		if mat1:
		# 			print "Mac_Address =",mat1.group(2)
				if mat2:
					print "IP_Address from device: =",mat2.group(2)
					IP_Address_dev = mat2.group(2)
					print "IP Address from NMS   :=",NMS_API_IP
					if IP_Address_dev == NMS_API_IP:
						assert(IP_Address_dev == NMS_API_IP)
						print "IP address from API and Device matched successfully..."
						Test_result = "Test1_Result"+ str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
						test1_file_path= dev_filepath
						print "Success, NMS and Dev IP address Matched.."
						test_status = "Pass"
						addOutput(test1_file_path,Node_ipconfig, NMS_Device_IP, tc, test_status)
					else:
						print "IP address doesn't matched with the ip found from API.."
						Test_result = "Test1_Result"+ str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
						test1_file_path= dev_filepath
						print "Failure, NMS and Dev IP address are not matched.."
						test_status = "Fail"
						addOutput(test1_file_path,Node_ipconfig, NMS_Device_IP, tc, test_status)
		
	
			Test_result = "Test1_VerifyIP_Add_Result"+ str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
			test1_file_path= ".\\Output\\"+Test_result
			#print "Success, NMS and Dev IP address Matched.."
			test_status = "Success, NMS and Dev IP address Matched.."
			addOutput(dev_filepath,Node_ipconfig, NMS_Device_IP, tc, test_status)
			return test_status
			print "@@@@@@@@@@@@@@@@@@@@Test1_Verify_IPAddress Test case is over@@@@@@@@@@@@@@@@@@@@@@@@@@"
# 		except Exception as e:
# 			print "##########################################################################"			
# 			print("@@@@@@@@@@@@@@@@@@@@Test1_Verify_IPAddress Test case@@@@ error occured.")
# 			print("Data is not received or Check the Connectivity")
# 		
# 	        print(e)
# 	        return "Fail"
