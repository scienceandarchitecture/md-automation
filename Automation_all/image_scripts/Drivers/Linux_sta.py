import time
import re
import subprocess
import sys

supplicant_path = r"/home/benison/Source_downloads/wpa_supplicant-2.4/wpa_supplicant/"
cli_path = r"/home/benison/Source_downloads/wpa_supplicant-2.4/wpa_supplicant/"

class Linux_sta(object):
	
	def init (self):
        	self.object = object

	def Exec_linux_cmd(self,cmd):
		opstr = subprocess.check_output(cmd,shell = True)
		time.sleep(2)
		return opstr


	def LinuxSTA_Scan(self,ssid,bssid = None):
		matobj = re_exp = None
		global cli_path 
		cmd = cli_path + "wpa_cli scan"
		try:
			output_str = subprocess.check_output(cmd,shell = True)
			time.sleep(5)
			cmd = cli_path + "wpa_cli scan_results"
			output_str = subprocess.check_output(cmd,shell = True)
			time.sleep(5)
		except subprocess.CalledProcessError as errorno:
			print "\n Wpa_cli scan error", errorno.output, errorno.returncode
			return "Fail"
		if output_str :
			if bssid :
				re_exp = "(" + bssid +")" + r'\s*[ -~]*' + "(" + ssid + ")"
			else :
				re_exp = ssid
				matobj = re.search(re_exp,output_str)
				if matobj :
					return "found"
			
		return "notfound"
	
	def LinuxSTA_Disconnect(self):
		global cli_path
		cmd = cli_path + "wpa_cli disconnect"
		try:
			output_str = subprocess.check_output(cmd,shell = True)
			time.sleep(1)
		except subprocess.CalledProcessError as errorno:
			print "\n Wpa_cli status error", errorno.output, errorno.returncode
			return("Fail")
		return("Success")
		
		
	def LinuxSTA_Get_Status(self):
		global cli_path
		cmd = cli_path + "wpa_cli status"		
		try:
			output_str = subprocess.check_output(cmd,shell = True)
			time.sleep(1)
		except subprocess.CalledProcessError as errorno:
			print "\n Wpa_cli status error", errorno.output, errorno.returncode
			return "Fail"
		if (re.search(r"wpa_state=COMPLETED",output_str)):
			return("connected")
		elif (re.search(r"wpa_state=DISCONNECTED",output_str)):
			return("disconnected")
		
		return "couldn't determine"
	
	def LinuxSTA_Connect(self,ssid,auth,passphrase=None):
		matobj = re_exp = None
		global cli_path
		cmd = cli_path + "wpa_cli remove_network all"	
		try:
			output_str = subprocess.check_output(cmd,shell = True)
			time.sleep(1)
			cmd = cli_path + "wpa_cli add_network"
			output_str = subprocess.check_output(cmd,shell = True)
			time.sleep(1)
			cmd = cli_path + "wpa_cli set_network 0 ssid " + r'\"' \
					+ ssid + r'\"'
			output_str = subprocess.check_output(cmd,shell = True)
			time.sleep(1)
		except subprocess.CalledProcessError as errorno:
			print "\n Wpa_cli Linux STA connection error", errorno.output\
			, errorno.returncode
			return("Fail")   
		if(auth == "OPEN") or (auth == "open"):
			cmd = cli_path + "wpa_cli set_network 0 key_mgmt NONE" 
			try:
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(1)
				cmd = cli_path + "wpa_cli enable_network 0"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(3)
				cmd = cli_path + "wpa_cli select_network 0"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(3)
			except subprocess.CalledProcessError as errorno:
				print "\n Wpa_cli LinuxSTA Open connection error",\
				 errorno.output, errorno.returncode
				return("Fail")
		elif(auth == "WPA2") or (auth == "wpa2"): 
			cmd = cli_path + "wpa_cli set_network 0 key_mgmt WPA-PSK" 
			try:
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(1)
				cmd = cli_path + "wpa_cli set_network 0 pairwise CCMP"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(1)
				cmd = cli_path + "wpa_cli set_network 0 group CCMP"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(1)
				cmd = cli_path + "wpa_cli set_network 0 psk " + r'\"' + passphrase + r'\"'	
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(1)
				cmd = cli_path + "wpa_cli enable_network 0"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(3)
				cmd = cli_path + "wpa_cli select_network 0"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(3)
			except subprocess.CalledProcessError as errorno:
				print "\n Wpa_cli LinuxSTA WPA2-PSK connection error",\
				 	errorno.output, errorno.returncode
				return("Fail")
		elif(auth == "WPA") or (auth == "wpa"): 
			cmd = cli_path + "wpa_cli set_network 0 key_mgmt WPA-PSK" 
			try:
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(1)
				cmd = cli_path + "wpa_cli set_network 0 pairwise TKIP"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(1)
				cmd = cli_path + "wpa_cli set_network 0 group TKIP"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(1)
				cmd = cli_path + "wpa_cli psk " + r'\"' + passphrase + r'\"'
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(1)
				cmd = cli_path + "wpa_cli enable_network 0"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(3)
				cmd = cli_path + "wpa_cli select_network 0"
				output_str = subprocess.check_output(cmd,shell = True)
				time.sleep(3)
			except subprocess.CalledProcessError as errorno:
				print "\n Wpa_cli LinuxSTA WPA-PSK connection error",\
				 errorno.output, errorno.returncode
				return("Fail")	   
		return("Success") 

	def Connect_via_LinuxSTA(self,ssid,auth,bssid=None,passphrase=None):
		count = 0
		scan_res = None
		while count < 3:
			scan_res = self.LinuxSTA_Scan(ssid, bssid)
			if scan_res == "found" :
				break
			elif (scan_res == "Fail") or (scan_res == "notfound") :
				print "\nwpa_cli scan Failed!!"
				count += 1
				scan_res = None
				time.sleep(15)
				continue
		if (scan_res == "notfound") or (scan_res == "Fail"):
			return "Fail"	
		if ((self.LinuxSTA_Connect(ssid,auth,passphrase)) == "Success" ):
				if ((self.LinuxSTA_Get_Status()) == "connected"):
					return "Success"
				#elif((self.LinuxSTA_Get_Status()) == "disconnected") or \
				#	((self.LinuxSTA_Get_Status()) == "couldn't determine") :
				#	return "Fail"
		return "Fail"		
