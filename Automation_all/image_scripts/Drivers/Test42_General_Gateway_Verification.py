import json
#from requests.packages import urllib2
import datetime
#from CommonOperations import *
import time
import pandas as pd
import unittest
import csv
#import dev_function
from Common_lib  import CommonOperations
from dev_function import *
import re



def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'a+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+str(NMS_Param)])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return



def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//GeneralConfiguration.csv")
    #d_excel("config_data.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    Supported_protocol = df.Supported_protcol[tc]
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]

    test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, Supported_protocol, Interface_Name, Network_Name, API_Server_IP]
                 
    return test_params

def Setup_Parser_method(tc):
    count = 0
    while count <= tc:
        if count == tc:
            data = pd.read_csv("..//Data//Test1_Setup.csv")
            NAME = data.NAME[count]
            MAC = data.MAC[count]
            Dev_IP =  data.IP[count]
            TYPE = data.TYPE[count]
            MODEL = data.MODEL[count]
            NODE_USER = data.USER_NAME[count]
            setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
            print setup_values
            return setup_values
        count = count+1


                        
class Test42_General_Gateway_Verification(object):
    def init(self):
        self.object = object
    def test42_General_Gateway_Verification(self,intf, tc):
        try:
	    
	    val = Setup_Parser_method(tc)
	    NAME = val[0]
	    Dev_MAC_Address = val[1]
	    NMS_Device_IP = val[2]
	    Type = val[3]
	    Model = val[4]
	    Node_User= val[5]

	    api_data = Input_Data_Parser(tc)
	    api_data[0]
	    API_Name = api_data[0]
	    Method = api_data[1]
	    Content_Type = api_data[2]
	    Arg1 = api_data[3]
	    Exp_st_code= api_data[4]
	    Precondition = api_data[5]
	    Supported_protocol= api_data[6]
	    Interface_Name = intf
	    Network_Name = api_data[8]
	    API_Server_IP= api_data[9]
	    #Gateway = api_data[14]
	    
	    url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?interfaceName="+Interface_Name+"&networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
	    print url
	    
	    Dev_IP= NMS_Device_IP 
	    verify_api_param = 'gateWay'
	    #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
	    s= CommonOperations()
	    tc_strt = datetime.datetime.now()            
	    p=s.commonOperations(tc, Method, url, Content_Type, Arg1, Exp_st_code, Precondition, Supported_protocol, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
	    op = json.dumps(p,ensure_ascii=True)
	    op1 = json.loads(op)
	    
	    NMS_API_Gateway = op1["gateWay"]
	    print "gateway value from API Response : ", NMS_API_Gateway
	    #print "Request output is", op        
	    current_time = time.localtime()    
	    filename = str(time.strftime('%Y_%m_%d_%H_%M', current_time))
	    nms_filename = "Test42_Gateway_Verification_NMS_Output"+filename
	    nms_filepath = "../Output//"+nms_filename+'.csv'
	    nms_log = open(nms_filepath, 'w')
	    nms_log.write(op)
	    nms_log.close()
	    print "NMS output saved in logfile "
	    print "                           \n"
	    
	    """ Verification of Device Data """
	    print "Device Data Verification Starts Now.."
	    
	    dev_Logfilename =  "Test_Result"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
	    dev_filepath = "..//Output//"+dev_Logfilename
	    
	    telnet_user = "root"
	    devobj = dev_function()
	    print "ip is ",NMS_Device_IP
	    telnet_user = "root"
	    t = dev_function()
	    #print NMS_API_IP
	    dev_gateway =  devobj.get_Gateway(Node_User, dev_filepath, NMS_Device_IP, Interface_Name )
	    # mat1 =re.search(r'(Mask:)(([1-9]{1,3}\.){3}([0-9]{1,3}))',dev_gateway)
	    for line in dev_gateway.splitlines():
		    #  mat1=re.search(r'(HWaddr)(.*)',line)
		    if "gateway" in line:
		        if NMS_API_Gateway in line:
		            print "Gateway value on device is  ",line
		            print "NMS Gateway value matched with the value in etc/config/network file.."
	#                 mat2=re.search(r'(option gateway\s)(([0-9]+){1,3}\.([0-9]+){1,3}\.([0-9]+){1,3}\.\d{1,3})',dev_gateway,re.I)
	#                 if mat2:
	#                     print "Gateway from device: =",mat2.group()
	#                     dev_gateway = mat2.group(2)
	#         
	#     #     mat1 =re.search(r'(gateway\s)(([0-9]+){1,3}\.([0-9]+){1,3}\.([0-9]+){1,3}\.\d{1,3})',dev_gateway,re.I|re.MULTILINE)
	#     #     print mat1
	#     #     dev_gateway = mat1.group(2)
	#                     print "Device gateway is ", dev_gateway
	#                     if str(NMS_API_Gateway).strip() ==  str(dev_gateway).strip():
	#                         print "Success,Gateway On NMS & Device Matched.."
	#                         test_status = "Pass"
	#                         addOutput(dev_filepath,op, NMS_API_Gateway, tc, test_status)
	#                     #    return test_status
		        else:
		            print "Fail, NMS_API_Gateway  and Device Gateway are not matching"
		            test_status = "Fail"
		            addOutput(dev_filepath,op, NMS_API_Gateway, tc,test_status)
		        #print "checking the next line, value not in the current line ", line
		         #   return test_status
     	except Exception as e:
		 #print ("Error ....sys.exec_info()[0], has occured....")
		 print("Data is not received or Check the Connectivity")
		 print(e)
		 return "Fail"                

