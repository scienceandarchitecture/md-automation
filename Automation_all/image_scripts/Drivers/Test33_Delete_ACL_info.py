import csv
import datetime
import json
import re
from dev_function import *
import time
from Common_lib import CommonOperations
import Common_lib
from Test30_CreateNetwork import *
import pandas as pd



def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'w+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+str(NMS_Param)])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return "Error"


def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//DeleteACLInfo.csv")
    #d_excel("config_data.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]
    ACL_Mac_val = df.ACL_Mac[tc]
    _80211eCategoryIndex_val = df._80211eCategoryIndex[tc]
    allowEntry_val = df.allowEntry[tc]
    _80211eCategoryEnabled_val = df._80211eCategoryEnabled[tc]
    vlanTag_val = df.vlanTag[tc]
    test_params = [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, Interface_Name, Network_Name, API_Server_IP, ACL_Mac_val, _80211eCategoryIndex_val, allowEntry_val, _80211eCategoryEnabled_val,vlanTag_val ]
                 
    return test_params

def Setup_Parser_method(tc):
    	count = 0
	while count<= tc:
		if count == tc:
			data = pd.read_csv("..//Data//Test1_Setup.csv")
			NAME = data.NAME[count]
			MAC = data.MAC[count]
			Dev_IP =  data.IP[count]
			TYPE = data.TYPE[count]
			MODEL = data.MODEL[count]
			NODE_USER = data.USER_NAME[count]
			setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
			return setup_values
		count = count+1
        
class Test33_Delete_ACL_info(object):
    def init(self):
        self.object = object
	
    def test33_Delete_ACL_info(self,tc):
        try:
            
    	    val = Setup_Parser_method(tc)
    	    NAME = val[0]
    	    Dev_MAC_Address = val[1]
    	    NMS_Device_IP = val[2]
    	    Type = val[3]
    	    Model = val[4]
    	    Node_User= val[5]
    	    api_data = Input_Data_Parser(tc)
    	    api_data[0]
    	    API_Name = api_data[0]
    	    Method = api_data[1]
    	    Content_Type = api_data[2]
    	    Arg1 = api_data[3]
    	    Exp_st_code= api_data[4]
    	    Precondition = api_data[5]
    	    Interface_Name = api_data[6]
    	    Network_Name = api_data[7]
    	    API_Server_IP= api_data[8]
    	    ACL_Mac_val = api_data[9]
    	    _80211eCategoryIndex_val = api_data[10]
    	    allowEntry_val = api_data[11]
    	    _80211eCategoryEnabled_val = api_data[12]
    	    vlanTag_val = api_data[13]
    	    url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
    	    print url
    	    Data_value = '{'+'"MacAddress"'+':'+" "+'"'+ str(ACL_Mac_val)+'"'+" " ',' +'"_80211eCategoryIndex"'+':'+str(_80211eCategoryIndex_val)+' , ' +'"allowEntry"'+' : '+str(allowEntry_val)+ ' , '+'"_80211eCategoryEnabled"'+':'+ str(_80211eCategoryEnabled_val)+ ' , '+ '"vlanTag"'+':'+str(vlanTag_val)+'}'
    	    #Data_value = '{'+'"preferedParent"'+':'+" "+ str(preferedParent)+" " +'}'
    	    print Data_value 
    	    Dev_IP= NMS_Device_IP 
    	    verify_api_param = 'networkId'
    	    #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
    	    current_time = time.localtime()
    	    s= CommonOperations()
    	    tc_strt = datetime.datetime.now()            
    	    #tc, Method, url, Content_Type, Data_value, Exp_st_code, Precondition, Supported_protocol, Data_value, Interface_Name, Network_Name, API_Server_IP)
    	    p=s.commonOperations(tc, Method, url, Content_Type, Data_value, Arg1, Exp_st_code, ACL_Mac_val, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
    	    op = json.dumps(p,ensure_ascii=True)
    	    op1 = json.loads(op)
    	    print op1
    	 
    	    if op.find('"rebootRequired":No')or op.find('"rebootRequired":NO'):
    		print "reboot is not required"
    	     
    	     
    	    if op.find('"code":204,"message":"ACL Not Exist..."'):
    		print "ACL Info not found on Device which means ACL info deleted successfully..."
    		exit()
    		
    	    elif op.find('"rebootRequired":Yes') or op.find('"rebootRequired":YES'):
    		rebootAPI="reboot"
    		RebootMethod = "POST"
    		url = "http://"+API_Server_IP+":8080"+"/NMS/"+rebootAPI+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
    		print "Rebooting Node, please wait till node gets Up"
    		s= CommonOperations()
    		tc_strt = datetime.datetime.now()
    		p=s.commonOperations(tc, RebootMethod, url, Content_Type, Data_value, Arg1, Exp_st_code, ACL_Mac_val, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
    		op = json.dumps(p, ensure_ascii=True)
    		op1 = json.loads(op)
    		 
    		if op1.find('"Node rebooted  successfully"'):
    		    print "Node Rebooting Now, please wait while node reboots..."
    		devobj = dev_function()
    		reboot_Node = devobj.check_node_status(NMS_Device_IP)
    		print reboot_Node
    		if reboot_Node=="Success":
    		    print "Node Rebooted successfully..."
    		     
    		    #dev_Logfilename = "Test21_UpdateCountryCode_NMS_Output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
    
    	    elif op.find("rebootRequired== null"):
    		    print "Node giving null value"
    		    print "Which Means either value was updated but not rebooted for the previous execution"
    		    #print "Request output is", op        
    		    current_time = time.localtime()    
    		    filename = str(time.strftime('%Y_%m_%d_%H_%M', current_time))
    		    nms_filename = "Test33_Delete_ACL_info_NMS_Output"+filename
    		    nms_filepath = "..//Output//"+nms_filename+'.csv'
    		    nms_log = open(nms_filepath, 'w')
    		    nms_log.write(op)
    		    nms_log.close()
    		    print "NMS output saved in logfile"
    		    print "                           \n"                
    	    """ Verification of Device Data """
    	    
    	    
    	    print "Device Data Verification Starts Now.."     
    	    dev_Logfilename = "Test33_Delete_ACL_info_Device_output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
    	    dev_filepath = "..//Output//"+dev_Logfilename
    	    dev_obj = dev_function()
    	    block_str=dev_obj.get_from_ACL(Node_User,dev_filepath, NMS_Device_IP)
    	    ACL_Mac_val = ACL_Mac_val.lower()
    	    
    	    
    	    print ACL_Mac_val
    	    sub_para=find_between(str(block_str), "{","}")
    	    print "Sub Para", sub_para
    	    #dev_aclmac=re.search(ACL_Mac_val,sub_para, re.IGNORECASE|re.MULTILINE)
    	    dev_acl_mac = re.search(r'(([0-9-A-Z]{1,2}\:)(\w{2}\:){4}([0-9-A-Z]{1,2}))',sub_para, re.IGNORECASE|re.MULTILINE)
    	    print dev_acl_mac.group()
    	    dev_acl_mac = dev_acl_mac.group()
    	    print "Verifying if the aclinfo is removed or not..."
    	    if ACL_Mac_val == dev_acl_mac:
    		print "MAC address for ACL found"
    		print "ACL Info not deleted.."
    		#reqd_block=re.search(r'(dev_acl_mac)(\s*\{)(([ -~])|\n\s.*)*',sub_para, re.IGNORECASE|re.MULTILINE)
    		#print reqd_block
    		#To Extract Tag values
    	     
    		tag = re.search(r'(tag)\=(\w+)',sub_para)
    		dev_tag = tag.group(2)
    		if vlanTag_val == -1:
    		    if dev_tag == "untagged":
    		        print "vlan tag value is -1 so it is set as untagged..."
    		        print "Success, VLAN Tag value matched on Device and NMS..."
    		    else:
    		        print "VLAN TAG Values are not matching on NMS and device..."
    		 
    		else:
    		    if vlanTag_val == dev_tag:
    		        print "VLAN TAG Values are matching on NMS and device..."
    		    else:
    		        print "VLAN TAG Values are not matching on NMS and device..."
    		 
    		#To Extract Allow
    		allowEntry_val1=re.search(r'(allow)\=(\w+)',sub_para)
    		dev_allowEntry = allowEntry_val1.group(2)
    		if str(allowEntry_val) == str(dev_allowEntry):
    		    print "Success, allowEntry_val value matched on Device and NMS..."
    		else:
    		    print "allowEntry_val Values are not matching on NMS and device..." 
    		 
    		#To Extract dot11e_enabled..
    		_dot1e_en1=re.search(r'(dot11e_enabled)\=(\w+)',sub_para) 
    		dev_dot1e_en = _dot1e_en1.group(2)
    		if str(dev_dot1e_en) == str(_80211eCategoryIndex_val):
    		    print "Success, _80211eCategoryIndex_val value matched on Device and NMS..."
    		else:
    		    print "_80211eCategoryIndex_val Values are not matching on NMS and device..."
    		 
    		#To Extract dot11e_Category..
    		_dot1e_cat=re.search(r'(dot11e_category)\=(\w+)',sub_para) 
    		dev_dot11e_category= _dot1e_cat.group(2)
    		if str(dev_dot11e_category) == str(_80211eCategoryIndex_val):
    		    print "Success, _80211eCategoryIndex_val value matched on Device and NMS..."
		    test_status = "Pass"
		    return test_status
    		else:
    		    print "_80211eCategoryIndex_val Values are not matching on NMS and device..."
    	    else:
    		print "acl mac deleted Successfully"


        except Exception as e:
            print(e)
           
    
        
