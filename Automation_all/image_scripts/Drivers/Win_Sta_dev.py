import re
import binascii
import time
import subprocess
import paramiko
import os
from spur import ssh
from Log import *
from subprocess import check_output

class Win_Sta(object):
    'Windows STA library functions'
    
    def init (self):
        self.object = object
        
    
    def WinSTA_SCAN (self,logFH):
        '''This module scans for the available networks 
        and returns a dictionary of available networks
        '''
        logobj = Log()
        cmd = "netsh wlan show networks"
        logobj.AppendLog(logFH,cmd)
        opstr = check_output(cmd , shell=True)
        logobj.AppendLog(logFH,opstr)
        time.sleep(2)
        matobj = re.findall('SSID\s\d+\s\:\s[ -~]*',opstr,re.I|re.M)
        netdict = {}
        if matobj:
            for i in matobj:
                dictstr = i.split(':')
                dictstr[0] = dictstr[0].replace(" ","")
                dictstr[1] = dictstr[1].replace(" ","")
                netdict[dictstr[0]] = dictstr[1]
        else:
                return "no network found!!"
        
        return netdict
    
    def WinSTA_Get_Connection_Status(self,ssid):
        '''
        This module checks wheather the Wi-Fi connection status is 
        active or not.
        '''
        opstr = ''
        cmd = "netsh wlan show interfaces"
        opstr = check_output(cmd , shell=True)
        time.sleep(1)
        rexp = r"(\s|\t)*SSID(\s|\t)*\:\s*" + ssid
        if re.search(r"(\s|\t)*State(\s|\t)*\:\s*disconnected",opstr):
            return "disconnected" 
        elif re.search(r"(\s|\t)*State(\s|\t)*\:\s*connected",opstr):
            if re.search(rexp,opstr):
                return "connected"
        
        return "Fail"
    
    def WinSTA_Create_Profile(self,ssid,auth,passwd=None):
        '''
        This module create a wireless profile of the required 
        name and authentication type
        '''
        '''added below command to delete previously saved profiles'''
        cmd = "netsh wlan delete profile *"
        opstr = check_output(cmd , shell=True)
        profile_name= ssid + ".xml"
        opstr =''
        if (auth == "open" or auth == "OPEN"):
            fh = open ('.\\Wi-Fi-OPEN.xml','r')
            file_data = fh.read()
            hex_ssid = str(binascii.b2a_hex(ssid))
            repstr = re.sub(r'4F50454E',hex_ssid,file_data,re.IGNORECASE)
            repstr = re.sub(r'OPEN',ssid,repstr,re.IGNORECASE)
            fh.close()
        elif (auth == "wpa2" or auth == "WPA2"):
            fh = open ('..\\Wi-Fi-WPA2.xml','r')
            file_data = fh.read()
            hex_ssid = str(binascii.b2a_hex(ssid))
            repstr = re.sub(r'57504132',hex_ssid,file_data,re.IGNORECASE)
            repstr = re.sub(r'WPA2',ssid,repstr,re.IGNORECASE)
            repstr = re.sub(r'12345678',passwd,repstr,re.IGNORECASE)
            fh.close()
        else :
                return ("Authentication not Supported!!")
        
        file_path = '..\\Log\\'
        profile = file_path +"\\"+ profile_name
        fh= open(profile,'w')
        fh.write(repstr)
        fh.close()
        cmd = "netsh wlan add profile filename=" + profile 
        opstr = check_output(cmd , shell=True)
        time.sleep(2)
        ''' added to configure the profile for auto '''
        profile = file_path +"\\"+ ssid        
        cmd = "netsh wlan set profileparameter name=" +\
                ssid + " interface=\"Wi-Fi\" SSIDname=" + ssid +\
                  " ConnectionType=ESS ConnectionMode=auto"
        opstr = check_output(cmd , shell=True)        
        return

        
    def WinSTA_Connect(self,ssid):
        '''
        This module will connect the Windows STA to required network
        '''
        cmd = "netsh wlan connect ssid="+ ssid + " name=" + ssid
        outputstring = check_output(cmd , shell=True)
        time.sleep(5)
        
        
    def WinSTA_Config_IP(self,ipadd,netmask):
        ''' 
        This module will configure the IP address of the windows STA
        Please make sure that the name of the wireless interface is Wi-Fi
        '''
        cmd = "netsh interface ipv4 set address name=\"Wi-Fi\" static " + ipadd + " " + netmask
        opstr = check_output(cmd , shell=True)
        time.sleep(2)
        return
    
    def Exec_Win_Cmd(self,cmd):
        opstr = check_output(cmd , shell=True)
        time.sleep(2)
        return
        
    def WinSTA_Disconnect(self,ssid):
        '''
        This module will disconnect the Windows STA from the required network.
        Further it will delete the profile from  STA
        '''
        cmd = "netsh wlan disconnect"
        outputstring = check_output(cmd , shell=True)
        #time.sleep()
        cmd = "netsh wlan delete profile " + ssid 
        outputstring = check_output(cmd , shell=True)
        #time.sleep(3)    

    def Connect_Via_WinSTA(self,logFH,ssid,auth,passphrase=None):
        
        count = 0
        logobj = Log()
        self.WinSTA_Create_Profile(ssid,auth,passphrase)
        #time.sleep(10)

        while count <= 3:
            networksfound = self.WinSTA_SCAN(logFH)
            #time.sleep(10)
            if ssid in networksfound.values() :     
                self.WinSTA_Connect(ssid)
                time.sleep(2)
                if (self.WinSTA_Get_Connection_Status(ssid) \
                    == "connected"):
                    return 
                elif (self.WinSTA_Get_Connection_Status(ssid)\
                       == "disconnected"):
                    count = count + 1
                    self.WinSTA_Connect(ssid)
                    time.sleep(2)
            else :
                #print "\n Please wait!!the required networks is not found in scanned list \n"
                logobj.AppendLog(logFH,"\n Please wait!!the required networks is not found in scanned list \n")
                count = count + 1 
                time.sleep(10)   
                cmd = 'netsh interface set interface name=\"Wi-Fi\" admin=disabled'
                opstr = check_output(cmd , shell=True)
                time.sleep(5)
                cmd = 'netsh interface set interface name=\"Wi-Fi\" admin=enabled'
                opstr = check_output(cmd , shell=True)
                time.sleep(20)
                
        return    