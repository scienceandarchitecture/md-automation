import json
import datetime
import time
import pandas as pd
import csv
import Common_lib
import dev_function
from Common_lib  import CommonOperations
from dev_function import *
import re
import Test30_CreateNetwork
from Test30_CreateNetwork import *

def read_vlan_mesh(Node_User,dev_filepath, NMS_Device_IP, Vlan_Name):
    print Vlan_Name
    dev_obj= dev_function()
    block_str=dev_obj.read_meshapp(Node_User,dev_filepath, NMS_Device_IP)
    sub_para = find_between_including(block_str, Vlan_Name, "none" )
    return sub_para
    

def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'w+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+str(NMS_Param)])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return

def find_between_including( s, first, last ):
    try:
        start = s.index( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return "Error"


def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//get_Vlan_InfoName.csv")
    #d_excel("config_data.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]
    VLAN_Name_val = df.VLAN_Name[tc]
    essid_val = df.essid[tc]
    tag_val = df.tag[tc]
    dot11eEnabled_val = df.dot11eEnabled[tc]
    dot11eCategory_val = df.dot11eCategory[tc]
    dot1pPriority_val = df.dot1pPriority[tc]
    securityType_val = df.securityType[tc]
    rtsThreshold_val = df.rtsThreshold[tc]
    fragThreshold_val = df.fragThreshold[tc]
    beaconInt_val = df.beaconInt[tc]
    serviceType_val = df.serviceType[tc]
    txPower_val = df.txPower[tc]
    txRate_val = df.txRate[tc]
    ipAddress_val = df.ipAddress[tc]
    securityInfo_val = df.securityInfo[tc]
    
    tag_val = df.tag[tc]
    
    test_params = [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, Interface_Name, Network_Name, API_Server_IP, VLAN_Name_val,    essid_val,    tag_val,    dot11eEnabled_val,    dot11eCategory_val,    dot1pPriority_val,    securityType_val,    rtsThreshold_val,    fragThreshold_val,    beaconInt_val,    serviceType_val,    txPower_val,    txRate_val,    ipAddress_val,    securityInfo_val]
                 
    return test_params

def Setup_Parser_method(tc):
	count = 0
	while count<= tc:
		if count == tc:
			data = pd.read_csv("..//Data//Test1_Setup.csv")
			NAME = data.NAME[count]
			MAC = data.MAC[count]
			Dev_IP =  data.IP[count]
			TYPE = data.TYPE[count]
			MODEL = data.MODEL[count]
			NODE_USER = data.USER_NAME[count]
			setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
			return setup_values
		count = count+1

"""
The preferred parent is updated using nms api and
then re-checking the value on Device after restarting the node """              
          
class Test34_Get_Vlan_infoName(object):
    
    def init(self):
        self.object = object
    
    def test34_Get_Vlan_infoName(self,tc):
        try:
            print "This Test will perform the verification of VLAN info using VLAN Info by Name API.." 
    	    print "This Test will verify the VLAN info fetched from NMS with the values on device"
    	    val = Setup_Parser_method(tc)
    	    NAME = val[0]
    	    Dev_MAC_Address = val[1]
    	    NMS_Device_IP = val[2]
    	    Type = val[3]
    	    Model = val[4]
    	    Node_User= val[5]
    	    api_data = Input_Data_Parser(tc)
    	    api_data[0]
    	    API_Name = api_data[0]
    	    Method = api_data[1]
    	    Content_Type = api_data[2]
    	    Arg1 = api_data[3]
    	    Exp_st_code= api_data[4]
    	    Precondition = api_data[5]
    	    Interface_Name = api_data[6]
    	    Network_Name =  api_data[7]
    	    API_Server_IP= api_data[8]
    	    VLAN_Name_val=    api_data[9]
    	    essid_val=    api_data[10]
    	    tag_val=    api_data[11]
    	    dot11eEnabled_val=    api_data[12]
    	    dot11eCategory_val=    api_data[13]
    	    dot1pPriority_val=    api_data[14]
    	    securityType_val=    api_data[15]
    	    rtsThreshold_val=    api_data[16]
    	    fragThreshold_val=    api_data[17]
    	    beaconInt_val=    api_data[18]
    	    serviceType_val=    api_data[19]
    	    txPower_val=    api_data[20]
    	    txRate_val=    api_data[21]
    	    ipAddress_val=   api_data[22]
    	    securityInfo_val=api_data[23]
    	    url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?networkName="+Network_Name+"&name="+VLAN_Name_val+"&macAddress="+Dev_MAC_Address
    	    print url
    	    
    	    Data_value = '{'+'"name"'+':'+" "+'"'+ str(VLAN_Name_val)+'"'+" " ',' +'"essid"'+':'+" "+'"'+ str(essid_val)+'"'+" " ',' +'"ipAddress"'+':'+" "+'"'+str(ipAddress_val)+'"'+','+'"securityInfo"'+':'+str(securityInfo_val)+','+'"txRate"'+':'+str(txRate_val)+','+'"txPower"'+':'+str(txPower_val)+','+'"serviceType"'+':'+str(serviceType_val)+','+'"beaconInt"'+':'+str(beaconInt_val)+','+'"fragThreshold"'+':'+str(fragThreshold_val)+','+'"rtsThreshold"'+':'+str(rtsThreshold_val)+','+'"dot1pPriority"'+':'+str(dot1pPriority_val)+' , ' +'"tag"'+':'+str(tag_val)+' , ' +'"dot11eEnabled"'+' : '+str(dot11eEnabled_val)+ ' , '+'"dot11eCategory"'+':'+ str(dot11eCategory_val)+'}'
    	    #Data_value = '{'+'"preferedParent"'+':'+" "+ str(preferedParent)+" " +'}'
    	    print Data_value 
    	    
    	    Dev_IP= NMS_Device_IP 
    	    verify_api_param = 'networkId'
    	    #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
    	    current_time = time.localtime()
    	    s= CommonOperations()
    	    tc_strt = datetime.datetime.now()            
    	    
    	    #tc, Method, url, Content_Type, Data_value, Exp_st_code, Precondition, Supported_protocol, Data_value, Interface_Name, Network_Name, API_Server_IP)
    	    p=s.commonOperations(tc, Method, url, Content_Type, Data_value, Arg1, Exp_st_code, VLAN_Name_val, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
    	    op = json.dumps(p,ensure_ascii=True)
    	    op1 = json.loads(op)
    	    
    
    	    """ For Get API Reboot will never be required so disabling the rebooting part"""
    	    
    	#     if op.find('"rebootRequired":Yes') or op.find('"rebootRequired":YES'):
    	#         rebootAPI="reboot"
    	#         RebootMethod = "POST"
    	#         url = "http://"+API_Server_IP+":8080"+"/NMS/"+rebootAPI+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
    	#         print "Rebooting Node, please wait till node gets Up"
    	#         s= CommonOperations()
    	#         tc_strt = datetime.datetime.now()
    	#         p=s.commonOperations(tc, RebootMethod, url, Content_Type, Data_value, Arg1, Exp_st_code, VLAN_Name_val, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
    	#         op = json.dumps(p, ensure_ascii=True)
    	#         op1 = json.loads(op)
    	#         
    	#         if op1.find('"Node rebooted  successfully"'):
    	#             print "Node Rebooting Now, please wait while node reboots..."
    	#         devobj = dev_function()
    	#         reboot_Node = devobj.check_node_status(NMS_Device_IP)
    	#         print reboot_Node
    	#         if reboot_Node=="Success":
    	#             print "Node Rebooted successfully..."
    	#             
    	#             #dev_Logfilename = "Test21_UpdateCountryCode_NMS_Output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
    	#     
    	#     if op.find('"rebootRequired":No')or op.find('"rebootRequired":NO'):
    	#         print "reboot is not required"
    	#         
    		        
    	    
    	    if op.find("rebootRequired== null"):
    		    print "Node giving null value"
    		    print "Which Means either value was updated but not rebooted for the previous execution"
    		    #print "Request output is", op        
    		    current_time = time.localtime()    
    		    filename = str(time.strftime('%Y_%m_%d_%H_%M', current_time))
    		    nms_filename = "Test34_Get_Vlan_infoName_NMS_Output"+filename
    		    nms_filepath = "..//Output//"+nms_filename+'.csv'
    		    nms_log = open(nms_filepath, 'w')
    		    nms_log.write(op)
    		    nms_log.close()
    		    print "NMS output saved in logfile"
    		    print "                           \n"                
    	    """ Verification of Device Data """
    	    
    	    print "Device verification part starts now.."
    	    dev_Logfilename = "Test34_Get_Vlan_infoName_Device_output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
    	    dev_filepath = "..//Output//"+dev_Logfilename
    	    
    	    sub_para = read_vlan_mesh(Node_User, dev_filepath, NMS_Device_IP, VLAN_Name_val)
    	    print sub_para
    	    
    	    dev_vlan_name1 = re.match(VLAN_Name_val,sub_para)
    	    dev_vlan_name = dev_vlan_name1.group()
    	    if str(VLAN_Name_val) == str(dev_vlan_name) :
    		print "Vlan name matched with the value provided in the NMS"
    	    else:
    		print "Vlan name not found"
    	    #To Extract ip address
    	    ip_add = re.search(r'(ip)\=(([0-9]{1,3}\.){3}[0-9]{1,2})',sub_para,re.I|re.M)
    	    dev_vlan_ip_address= ip_add.group(2)  
    	    print dev_vlan_ip_address
    	    if str(ipAddress_val) ==  str(dev_vlan_ip_address):
    		print "IP address value set for vlan through API matched with the value on device..."
    	    else:
    		print "IP Address value set on vlan through API is not matching with the value on device"
    	    
    	    #To Extract Tag
    	    tag=re.search(r'(tag)\=(\w+)',sub_para)
    	    dev_vlan_tag=tag.group(2)
    	    if str(tag_val) ==  str(dev_vlan_tag):
    		print "vlan_tag value set for vlan through API matched with the value on device..."
    	    else:
    		print "vlan_tag value set on vlan through API is not matching with the value on device"
    	    
    	     
    	    #To Extract Allow
    	    dot11e_en=re.search(r'(dot11e_enabled)\=(\w+)',sub_para)
    	    dev_vlan_dot11e_enabled=dot11e_en.group(2) 
    	    if str(dot11eEnabled_val) ==  str(dev_vlan_dot11e_enabled):
    		print "vlan_dot11e_enabled value set for vlan through API matched with the value on device..."
    	    else:
    		print "vlan_dot11e_enabled value set on vlan through API is not matching with the value on device"
    	    
    	     
    	    #To Extract dot11e_Category..
    	    dot1p_prio=re.search(r'(dot1p_priority)\=(\w+)',sub_para) 
    	    dev_vlan_dot1p_priority= dot1p_prio.group(2)
    	    if str(dot1pPriority_val) ==  str(dev_vlan_dot1p_priority):
    		print "vlan_dot1p_priority value set for vlan through API matched with the value on device..."
    	    else:
    		print "vlan_dot1p_priority value set on vlan through API is not matching with the value on device"
    	   
    
    	    #To Extract dot11e_Category..
    	#     dev_dot11eCategory=re.search(r'(dot11eCategory)\=(\w+)',sub_para) 
    	#     dev_valn_dev_dot11eCategory=dev_dot11eCategory.group(2)
    	#     if str(dot11eCategory_val) ==  str(dev_valn_dev_dot11eCategory):
    	#         print "valn_dev_dot11eCategory value set for vlan through API matched with the value on device..."
    	#     else:
    	#         print "valn_dev_dot11eCategory value set on vlan through API is not matching with the value on device"
    	#    
    
    	    #To Extract Essid
    	    essid=re.search(r'(essid)\=(\w+)',sub_para) 
    	    dev_vlan_essid= essid.group(2)
    	    if str(essid_val) ==  str(dev_vlan_essid):
    		print "essid value set for vlan through API matched with the value on device..."
    	    else:
    		print "essid value set on vlan through API is not matching with the value on device"
    	   
    	    #To Extract rts_th
    	    rts_th=re.search(r'(rts_th)\=(\w+)',sub_para) 
    	    dev_vlan_rts_threshold=rts_th.group(2)
    	    if str(rtsThreshold_val) ==  str(dev_vlan_rts_threshold):
    		print "Vlan_rts_threshold value set for vlan through API matched with the value on device..."
    	    else:
    		print "Vlan_rts_threshold value set on vlan through API is not matching with the value on device"
    	 
    	    
    	    #To Extract frag_th
    	    frag_th=re.search(r'(frag_th)\=(\w+)',sub_para) 
    	    dev_vlan_frag_threshold=frag_th.group(2)
    	    if str(fragThreshold_val) ==  str(dev_vlan_frag_threshold):
    		print "Vlan_frag_threshold value set for vlan through API matched with the value on device..."
    	    else:
    		print "Vlan_frag_threshold value set on vlan through API is not matching with the value on device"
    	 
    	    #To Extract beacon_interval
    	    beacon_int=re.search(r'(beacon_interval)\=(\w+)',sub_para) 
    	    dev_vlan_beacon_interval=beacon_int.group(2)
    	    if str(beaconInt_val) ==  str(dev_vlan_beacon_interval):
    		print "Beacon Interval value set for vlan through API matched with the value on device..."
    	    else:
    		print "Beacon Interval value set on vlan through API is not matching with the value on device"
    	 
    	    
    	    # To Extract Service
    	    service=re.search(r'(service)\=(\w+)',sub_para) 
    	    dev_vlan_service=service.group(2)
    	    if str(serviceType_val) ==  str(dev_vlan_service):
    		print "Vlan_service value set for vlan through API matched with the value on device..."
    	    else:
    		print "Vlan_service value set on vlan through API is not matching with the value on device"
    	 
    	    # To Extract txpower
    	    txpower=re.search(r'(txpower)\=(\w+)',sub_para) 
    	    dev_vlan_txpower=txpower.group(2)
    	    if str(txPower_val) ==  str(dev_vlan_txpower):
    		print "Tx power value set for vlan through API matched with the value on device..."
    	    else:
    		print "Tx power value set on vlan through API is not matching with the value on device"
    	 
    	    
    	    # To Extract txrate
    	    txrate=re.search(r'(txrate)\=(\w+)',sub_para) 
    	    dev_vlan_txrate=txrate.group(2)
    	    if str(txRate_val) ==  str(dev_vlan_txrate):
    		print "Tx rate value set for vlan through API matched with the value on device..."
    	    else:
    		print "Tx rate value set on vlan through API is not matching with the value on device"
    	 
    	 
    		""" security info is not yet avialable so this will fail. commenting it"""
    	    
    	    # To Extract security
    	#     security=re.search(r'(security\s\{\s*)(\w+)',sub_para) 
    	#     dev_vlan_security=security.group(2)
    	#     if str(securityInfo_val) ==  str(dev_vlan_security):
    	#         print "Security value set for vlan through API matched with the value on device..."
    	#     else:
    	#         print "Security value set on vlan through API is not matching with the value on device"
    	#   
        except Exception as e:
            print(e)
        
