import copy
import os
import re
from subprocess import check_output
from subprocess import CalledProcessError
import subprocess
import sys
import telnetlib
import time
import pandas as pd
from ipaddress import ip_address
from Log import *


class Setup_Verify_linux(object):
	def init(self):
		self.object = object
		
	
	def check_node_type(self,user,ip_add): 
		user = self.user
		ip_add = self.ip_add
		telconn = telnetlib.Telnet(ip_add, timeout=600)
		val =telconn.read_until('login:')
		telconn.write(user+"\n")
		time.sleep(2)
		telconn.write("cat /proc/net/meshap/mesh/adhoc_mode\n")
		telconn.write("exit\n")
		time.sleep(10)
		#telconn.write("ping 192.168.0.104\n")
		device_log = telconn.read_until("^M", timeout = 15)	  
		  
		if "FFR" in device_log:
			print "Node "+ip_add+" is Root"
			return "FFR"
		if "FFN" in device_log!=-1:
			print "Node "+ip_add+" is Relay"
			return "FFN"
		else:
			print "Node Failed to Respond.."
		
	def setup_check(self,Node1_IP):
		pingstr = ''
		if(sys.platform == "linux2"):
			cmd = "ping -c 3 " + str(Node1_IP)
		elif(sys.platform == "win32") or (sys.platform == "cygwin"):	
			cmd = "ping -n 3 " + Node1_IP
		try:
			returncode = 0
			pingstr = check_output(cmd , shell=True)
			time.sleep(4)
		except CalledProcessError as errorno:
			output = errorno.output
			returncode = errorno.returncode
			if returncode :
				temppingstr = "Request timed out" * 3

		if pingstr is not None:
			   temppingstr=copy.copy(pingstr)
		ReqtimeOut = re.search('Request timed out',temppingstr,re.I)
		Destunreach = re.search\
				('Destination host unreachable',temppingstr,re.I) 
		if ReqtimeOut :
			print "\nPing Failed.."
			return "Failed timed out"
		
		if Destunreach : 
			print "\nPing Failed.."
			return "Failed Destination Unreachable"
		
		strlines = pingstr.splitlines()
		if(sys.platform == "linux2"):
			for line in strlines:
				if "packets" in line:
					print line
					return "Ping Successfull"
		else:
			for line in strlines:
				if "Packets" in line:
					print line
					line = line.replace("Packets:","")
					line = line.replace(" ","")
					line = re.sub('\(.*\),$',"",line)
					print line
					print "\nPing Successfull"
					return "Ping Successfull"
					
