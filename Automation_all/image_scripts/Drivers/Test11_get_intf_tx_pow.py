"""This function Writes in to each related log,Test_No,Device output, 
NMS output,Test Execution Status"""

import datetime
import json
import time
from Common_lib import CommonOperations
from dev_function import *
import pandas as pd

def addOutput(filepath, op, NMS_Param, tc_num, test_status):
    outp_file = open(filepath, 'w+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc_num)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+NMS_Param])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return

""" This function reads the Parameters from getInterface.csv using pandas
 & returns the test_params the values :
 API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, 
 Supported_protocol, Interface_Name, Network_Name, API_Server_IP,Interface2_Name"""
def input_data_parser(tc_num):
    df = pd.read_csv(".//..//API//getInterface.csv")
    tc_num = 0
    API_Name = df.API_Name[tc_num]
    Method = df.Method[tc_num]
    Content_Type = df.Content_Type[tc_num]
    arg1 = df.arg1[tc_num]
    Exp_st_code = df.Exp_st_code[tc_num]
    Precondition = df.Precondition[tc_num]
    Supported_protcol = df.Supported_protcol[tc_num]
    Interface_Name = df.Interface_Name[tc_num]
    Network_Name = df.Network_Name[tc_num]
    API_Server_IP = df.API_Server_IP[tc_num]
    Interface2_Name = df.Interface2_Name[tc_num]
    test_params = [API_Name, Method, Content_Type, arg1, Exp_st_code, 
                   Precondition, Supported_protcol, Interface_Name, 
                   Network_Name, API_Server_IP,Interface2_Name]
    return test_params
""" This function reads the Parameters from Test1_Setup.csv using pandas
 & returns the test_params the values :
 NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER""" 
def Setup_Parser_method(tc):
        count = 0
	while count<= tc:
		if count == tc:
			data = pd.read_csv("..//Data//Test1_Setup.csv")
			#NAME = "NAME"+str()
			NAME = data.NAME[count]
			MAC = data.MAC[count]
			Dev_IP =  data.IP[count]
			TYPE = data.TYPE[count]
			MODEL = data.MODEL[count]
			NODE_USER = data.USER_NAME[count]
			setup_values = [NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
			return setup_values
		count = count+1

"""This class verify the parameter Intf_Tx_pow from NMS using API and compare 
the value on Device. If values matc_numhed then Testc_numase is Passed else Failed"""
class Test11_Verify_Get_Intf_Tx_Pow(object):
    def init(self):
        self.object = object
    def test11_Verify_Get_Intf_Tx_Pow(self,intf1, tc_num):
        try : 
            val = Setup_Parser_method(tc_num)
            NAME = val[0]
            Dev_MAC_Address = val[1]
            NMS_Device_IP = val[2]
            Type = val[3]
            Model = val[4]
            Node_User = val[5]
            """ Retrieving values from input_data_parser 
            which is assigned to variable val"""
            api_data = input_data_parser(tc_num)
            API_Name = api_data[0]
            Method = api_data[1]
            Content_Type = api_data[2]
            Arg1 = api_data[3]
            Exp_st_code = api_data[4]
            Precondition = api_data[5]
            Supported_protocol = api_data[6]
            #Interface_Name = api_data[7]
            Interface_Name = intf1
            Network_Name = api_data[8]
            API_Server_IP = api_data[9]
            #Interface2_Name = api_data[10]
            Interface2_Name = intf1
            url = "http://"+API_Server_IP+":8080"+"/NMS/" +API_Name+"?interfaceName=" \
            +Interface2_Name+"&networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
            print url
            Dev_IP = NMS_Device_IP 
            print "Test Details: "
            verify_api_param = 'txPower'
            #return test_status
            s = CommonOperations()
            tc_num_strt = datetime.datetime.now()
            p =s.commonOperations(tc_num, Method, url, Content_Type, Arg1, Exp_st_code, 
                                  Precondition, Supported_protocol, verify_api_param, 
                                  Interface_Name, Network_Name, API_Server_IP)
            op = json.dumps(p, ensure_ascii = True)
            op1 = json.loads(op)
            txpower_NMS = op1["txPower"]
            print "Txpower value from API Response: ", txpower_NMS 
            """ Verification of Device Data """
            print "Device Data Verification Starts Now.."
            current_time = time.localtime()
            dev_Logfilename = "Test11_get_intf_tx_power_Device_output" \
            +str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
            dev_filepath = "..//Output//"+dev_Logfilename
            telnet_user = "root"
            dev_obj = dev_function()
            print "Verification of Parameters in meshapp.conf"
            tx_Power_dev = dev_obj.get_intf_tx_pow(Node_User, dev_filepath, NMS_Device_IP, Interface2_Name)
            #print tx_Power_dev
            """ Comparing the values from NMS and device output
            If result matc_numhed , declaring Test case as pass /Fail"""
            if int(txpower_NMS) ==  int(tx_Power_dev):
                print "Tx_power Matched successfully"
                test_status = "Pass"
                addOutput(dev_filepath, op, tx_Power_dev, tc_num, test_status)
                return test_status
            else:
                print "Tx_power doesn't match..."
                test_status = "Fail"
                
                addOutput(dev_filepath, op, tx_Power_dev, tc_num, test_status)
                return test_status
                
        except Exception as e:
            print ("Error ....sys.exec_info()[0], has occured....")
            print("Data is not received or Check the Connectivity")
            print(e)
            return "Fail"
