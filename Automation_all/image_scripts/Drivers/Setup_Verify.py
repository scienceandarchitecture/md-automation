from Log import *
from ipaddress import ip_address
from subprocess import check_output, check_output
import copy
import os
import pandas as pd
import re
import subprocess
import sys
import telnetlib
import time
class Setup_Verify(object):
    def init(self):
        self.object = object
        
    
    def check_node_type(self,user,ip_add): 
        user = self.user
        ip_add = self.ip_add
        telconn = telnetlib.Telnet(ip_add, timeout=600)
        telconn.read_until('login:')
        telconn.write(user+"\n")
        time.sleep(2)
        telconn.write("cat /proc/net/meshap/mesh/adhoc_mode\n")
        telconn.write("exit\n")
        time.sleep(10)
        #telconn.write("ping 192.168.0.104\n")
        device_log = telconn.read_until("^M", timeout = 15)      
          
        if "FFR" in device_log:
            print "Node "+ip_add+" is Root"
            return "FFR"
        if "FFN" in device_log!=-1:
            print "Node "+ip_add+" is Relay"
            return "FFN"
        else:
            print "Node Failed to Respond.."
        


    def setup_check(self,Node1_IP):
        return 
#         logobj = Log()
#         logstr =''
        cmd = "ping -n 3 " + Node1_IP
        pingstr = check_output(cmd , shell=True)
        time.sleep(13)
        #logobj.AppendLog(logFH, pingstr)
        temppingstr=copy.copy(pingstr)
        temppingstr=temppingstr.replace('\n','')
        temppingstr=temppingstr.replace('.','')
                 
        ReqtimeOut = re.match('(.*)(Request timed out){3,}(.*)',temppingstr,re.IGNORECASE|re.MULTILINE) 
        
        if ReqtimeOut:
            print "Ping Failed.."
            logstr = "Ping Failed.."
            #logobj.AppendLog(logFH, logstr) 
            return "Failed timed out"
        
        DestUnreach = temppingstr.count("Destination host unreachable")
        if DestUnreach >=3 :
            print " Ping Failed.." 
            logstr = "Ping Failed.."
            #logobj.AppendLog(logFH, logstr)
            return "Failed Destination Unreachable"
        
        strlines = pingstr.splitlines()
        
        for line in strlines:
            if "Packets" in line:
                print line
                line = line.replace("Packets:","")
                line = line.replace(" ","")
                line = re.sub('\(.*\),$',"",line)
                print line
                logstr = "Ping Successfull"
                #logobj.AppendLog(logFH, logstr)
                return logstr
                    
