import json
import datetime
import time
import pandas as pd
import unittest
import csv
from dev_function import *
from Common_lib  import *


import re
import telnetlib

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return "Error"


def execute_via_telnet (ipaddr, cmd):
        user= "root"
        count = 0
        tn = telnetlib.Telnet(ipaddr)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
            #print "please login to continue"
            tn.write("root"+'\n')
            # print "login successfully"
            console_val = tn.read_until(':')


            if 'Password:' in console_val:
                tn.write(""+"\n")
                log_val = tn.read_until('~#')
                #print log_val
        
        
        print "Login successfull..."  
        
        print cmd
        #time.sleep(5)
        tn.write(cmd+"\n")
        time.sleep(2)
#         valconsole = tn.read_until('INDEX')
#         print valconsole
        return "Pass"



def get_hostname_from_config_system(user,  dev_logfile, ipaddress):
    tn = telnetlib.Telnet(ipaddress)
    cmd_val = tn.read_until(':')
    if "root" in cmd_val :
        print "Login is not required.."
    else:
        time.sleep(2)
        #print "please login to continue"
        tn.write("root"+'\n')
        # print "login successfully"
        console_val = tn.read_until(':')
        
        if 'Password:' in console_val:
            tn.write(''+"\n")
            log_val = tn.read_until(':')
            print log_val
 
    print "Login successfull..."
    #cmd1 = "cat /etc/config/system"
    cmd1 = 'uname -n'
    tn.write(cmd1+"\n")
    tn.write("exit\n")
    dev_ip_add = tn.read_all()
    tn.close()
    logfile = open(dev_logfile, 'a')
    logfile.write(dev_ip_add)
    logfile.close()
    return dev_ip_add









def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'a+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+NMS_Param])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return




def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//updateGeneralConfiguration.csv")
    #d_excel("config_data.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    preferedParent = df.preferedParent[tc]
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]
    nodeDescription = df.nodeDescription[tc]
    hostName = df.nodeName[tc]

    test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, preferedParent, Interface_Name, Network_Name, API_Server_IP, nodeDescription, hostName]
                 
    return test_params


def Setup_Parser_method(tc):
        count = 0
        while count<= tc:
            if count == tc:
                data = pd.read_csv("..//Data//Test1_Setup.csv")
                NAME = "NAME"+str()
                NAME = data.NAME[count]
                MAC = data.MAC[count]
                Dev_IP =  data.IP[count]
                TYPE = data.TYPE[count]
                MODEL = data.MODEL[count]
                NODE_USER = data.USER_NAME[count]
                setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
                return setup_values
            count = count+1
            
              
class Test39_UpdateNVerifyHost_Name:
    def init(self):
        self.object = object

    def test39_UpdateNVerifyHost_Name(self,tc, hostname_val):
         try :
	    
	    val = Setup_Parser_method(tc)
	    NAME = val[0]
	    Dev_MAC_Address = val[1]
	    NMS_Device_IP = val[2]
	    Type = val[3]
	    Model = val[4]
	    Node_User= val[5]
	    api_data = Input_Data_Parser(tc)
	    api_data[0]
	    API_Name = api_data[0]
	    Method = api_data[1]
	    Content_Type = api_data[2]
	    Arg1 = api_data[3]
	    Exp_st_code= api_data[4]
	    Precondition = api_data[5]
	    Supported_protocol= api_data[6]
	    Interface_Name = api_data[7]
	    Network_Name = api_data[8]
	    API_Server_IP= api_data[9]
	    Interface2_Name = api_data[10]
	    
	   # API_Name, Method, Content_Type, arg1, Exp_st_code, 
	    #Precondition, preferedParent, Interface_Name, Network_Name, API_Server_IP, nodeDescription, hostName
	    
	    #hostname_nms = hostname_val
	    url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
	    print url
	    Dev_IP= NMS_Device_IP 
	    print "Test Details: "
	    verify_api_param = 'hostname'
	    Data_value = '{'+'"hostname"'+':'+'"'+ hostname_val +'"'+'}'
	    #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
	    s= CommonOperations()
	    tc_strt = datetime.datetime.now()            
	    p=s.commonOperations(tc, Method, url, Content_Type, Data_value, Arg1, Exp_st_code, hostname_val, verify_api_param, Interface_Name, Network_Name, API_Server_IP)
	    #p=s.commonOperations(tc, Method, url, Content_Type, Arg1, Exp_st_code, Precondition, Supported_protocol, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
	    op = json.dumps(p,ensure_ascii=True)
	    op1 = json.loads(op)
	    print op1
	    #NMS_API_ESSID = op1["essid"]
	    #Node_Name_NMS = op1["hostName"]
	    #print "Node Name value from API Response : ", Node_Name_NMS
	    
	    #print Data_value
	# Node needs to be rebooted, irrespective of response so adding a flag to reboot
	    reboot_required = True
	    if op.find('"rebootRequired":Yes') or op.find('"rebootRequired":"YES"') or  reboot_required == True :
		print "Rebooting the node..."
		cmd = "reboot"
		execute_via_telnet(Dev_IP, cmd)
		time.sleep(10)
		devobj = dev_function()
		reboot_Node = devobj.check_node_status(NMS_Device_IP)
		print reboot_Node
		if reboot_Node=="Success":
		    print "Node Rebooted successfully..."
		    time.sleep(8)
		    
		    #dev_Logfilename = "Test21_UpdateCountryCode_NMS_Output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
		
	    elif re.search(r'["rebootRequired":"No" |"rebootRequired":No]', op, flags=re.IGNORECASE):
		print "reboot is not required"
	    

	    
	    
	    """ Verification of Device Data """
	    print "Device Data Verification Starts Now.."
	    current_time = time.localtime()
	    dev_Logfilename =  "Test_Result"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
	    dev_filepath = "..//Output//"+dev_Logfilename
	   
	    telnet_user = "root"
	    
	    
	    print "Verification of Parameters in meshapp.conf"
	    
	    dev_obj = dev_function()
	    #testobj = Test39_VerifyHost_Name()
	    
	    print Dev_IP
	    Host_name_dev =  get_hostname_from_config_system(telnet_user,dev_filepath, str(Dev_IP))
	    val = find_between(Host_name_dev, "uname -n", Node_User)
	    print "string found is ",val
	    hostname_dev = str(val).strip()
	    print "hostnamev value from dev is ", hostname_dev
	    if str(hostname_val).strip() in str(hostname_dev).strip():
		print "Host name matched successfully...."
		test_status = "Pass"
		addOutput(dev_filepath,op, hostname_dev, tc, test_status)
	        return test_status
	    else:
		print "Host name not matched...."
		test_status = "Fail"
		addOutput(dev_filepath,op, hostname_dev, tc, test_status)
		return test_status

	 except Exception as e:
            print ("Error ....sys.exec_info()[0], has occured....")
            print("Data is not received or Check the Connectivity")
            print(e)
            return "Fail"
