#Three_Node_First_Step
import os
import sys
import re
from subprocess import check_output
import time
import Log
import telnetlib, copy
import time
import csv
import Setup_Verify
from dev_function_dev import *
from Win_Sta import *
from Setup_Verify import *
import pandas as pd

def Get_highest_mac(mac1,mac2,mac3):

	tempmac1 = mac1
	tempmac2 = mac2
	tempmac3 = mac3
	mac1 = mac1.replace("-","")
	mac1 = mac1.replace(":","")
	mac1 = mac1.lower()
	mac2 = mac2.replace("-","")
	mac2 = mac2.replace(":","")
	mac2 = mac2.lower()
	mac3 = mac3.replace("-","")
	mac3 = mac3.replace(":","")
	mac3 = mac3.lower()
	if mac1 > mac2 and mac1 > mac3:
		return tempmac1
	elif mac2 > mac1 and mac2 > mac3 :
		return tempmac2
	elif mac3 > mac1 and mac3 > mac2:
		return tempmac3
	else:
		return "same"


def Get_nodes():
	df = pd.read_csv("..\\Config\\Three_Node_transition_Config.csv")
	my_list = df["NAME"].tolist()
	return my_list

def Config_Parser_method(logFH):
	
	devobj = dev_function()	
	df = pd.read_csv("..\\Config\\Three_Node_transition_Config.csv")
	with open("..\\Config\\Three_Node_transition_Config.csv", 'r') as f:
		file_read = csv.DictReader(f)	 
		data = []
		for row in file_read:
			data.append(row)
		no_row = len(data)   
		count = 0
		while count < no_row :
			node_info = {}
			node_info['mesh_id'] = df.MESH_ID[count]
			node_info['essid'] = df.ESSID[count]
			node_info['channel'] = df.CHANNEL[count]
			node_info['name'] = df.NAME[count]
			node_interface = df.INTERFACE[count]
			node_ip = devobj.get_from_setup(node_info['name'],"IP")
			node_user = devobj.get_from_setup(node_info['name'],"USER_NAME")
			print"\n Please wait!! configuring node" + (str(count+1)) + " this may take time\n\n"
			
			config = devobj.config_node_via_telnet(logFH,node_interface,node_ip,node_info,node_user)
			if count != 0:
				time.sleep(2)
			count = count + 1
		
		return 

class THREE_NODE_mode_transition_md6000(object):
	
	def init(self):
		self.object = object
	
	#setupfname = "Two_node_Transition_Setup"
	#start_time = time.time()
	configfname = "Three_Node_transition_Config"
	logobj = Log()
	Nodes = Get_nodes()
	devobj = dev_function()
	setup = Setup_Verify()
	winstaob = Win_Sta()1111
	node_status = ''
	node1ip = devobj.get_from_setup(Nodes[0],"IP")
	node2ip = devobj.get_from_setup(Nodes[1],"IP")
	node3ip = devobj.get_from_setup(Nodes[2],"IP")
	node1_mac = devobj.get_from_setup(Nodes[0], "MAC")
	node2_mac = devobj.get_from_setup(Nodes[1], "MAC")
	node3_mac = devobj.get_from_setup(Nodes[2], "MAC")	
	user1 = devobj.get_from_setup(Nodes[0], "USER_NAME")
	user2 = devobj.get_from_setup(Nodes[1], "USER_NAME")
	node1ssid = devobj.get_from_config(configfname,Nodes[0], "ESSID")
	node1auth = devobj.get_from_config(configfname,Nodes[0], "AUTHENTICATION")
	node2ssid = devobj.get_from_config(configfname,Nodes[1], "ESSID")
	node2auth = devobj.get_from_config(configfname,Nodes[1], "AUTHENTICATION")
	node3ssid = devobj.get_from_config(configfname,Nodes[2], "ESSID")
	node3auth = devobj.get_from_config(configfname,Nodes[2], "AUTHENTICATION")
	scriptname = os.path.basename(__file__)
	logFH = logobj.CreateLog(scriptname)
	logobj.AppendLog(logFH,("\nExecuting Test script "+scriptname))
	logobj.AppendLog(logFH,"\nVerifying if Setup is ready... ")
	
	for i in Nodes :
		ipaddr = devobj.get_from_setup(i,"IP")
		check_setup = setup.setup_check(ipaddr)   
		if check_setup == "Ping Successfull" :
			logobj.AppendLog(logFH,(" Node "+ i +" is up"))
				
		else:
			logobj.AppendLog(logFH,(" Node "+ i +" is Down"))
			exit
		
	logobj.AppendLog(logFH,("\n Configuring the Nodes"))
	Config_Parser_method(logFH)
	
	print "\n Trying to connect to Nodes..!please wait\n"
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 failed to bootup within expected time")
		exit()
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 failed to bootup within expected time")
		exit() 
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 failed to bootup within expected time")
		exit()		  
		
	logobj.AppendLog(logFH,("\nVerifying nodes mode!!\n"))
	cmd = 'cat /proc/net/meshap/mesh/adhoc_mode'
	dev_node_mode = ''
	for i in Nodes :
		user = devobj.get_from_setup(i, "USER_NAME")
		ipaddr = devobj.get_from_setup(i,"IP")
		logobj.AppendLog(logFH,("Trying to telnet to" + ipaddr)) 
		dev_node_mode = devobj.execute_via_telnet(ipaddr,cmd)
		if re.search("FFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("node" +(i)+" is FFR\n"))
			
		else :
			logobj.AppendLog(logFH,("node" +(i)+" is not FFR\n"))
			exit	
	
	print "\n Step1 verified successfully!!"
	#######################################################################
	print "\n Executing step2\n"
	logobj.AppendLog(logFH,("Disabling eth0 of Node3\n"))
	cmd = "ifconfig eth0 down"
	out =''
	out = devobj.execute_via_telnet(node3ip, cmd)
	logobj.AppendLog(logFH,out)
	logobj.AppendLog(logFH,("Please wait,Mesh formation is in progress!\n"))
	time.sleep(10)
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	print "\n\n\nMesh Table output is \n",dev_node_mode
	if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node3 connected to Node1 as FFN successfully!!" ))
		cmd = "ping -c 10 " + node3ip
		out = ''
		out = devobj.execute_via_telnet(node1ip, cmd)	
		out = devobj.check_positive_ping(out)
		if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
			print "ping from node 1 to node 3 Success!!"
		else:
			print "Step2 Failed,ping from node 1 to node 3 Fail!!"
			exit()	
	else:
		cmd = "cat /proc/net/meshap/mesh/table"
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		print "\n\n\nMesh Table output is \n",dev_node_mode
		if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Node3 connected to Node2 as FFN successfully!!" ))
			cmd = "ping -c 10 " + node3ip
			out = ''
			out = devobj.execute_via_telnet(node2ip, cmd)	
			out = devobj.check_positive_ping(out)
			if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
				print "ping from node 2 to node 3 Success!!"
			else:
				print "Step2 Failed,ping from node 2 to node 3 Fail!!"
				exit()	
	logobj.AppendLog(logFH,("\n Step2 verified successfully!!"))
	#######################################################################
	print "\n Executing step3\n"
	logobj.AppendLog(logFH,("Disabling eth0 of Node2\n"))
	cmd = "ifconfig eth0 down"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	logobj.AppendLog(logFH,("Please wait,Mesh formation is in progress!\n"))
	time.sleep(10)
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	print "\nMesh Table output is \n",dev_node_mode
	if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node3 connected to \
				Node1 as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit() 
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node2 connected as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit()   
	cmd = "ping -c 10 " + node3ip
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 1 to node 3 Success!!"
	else:
		print "Step2 Failed,ping from node 1 to node 3 Fail!!"
		exit()
	cmd = "ping -c 10 " + node2ip
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 1 to node 2 Success!!"
	else:
		print "Step2 Failed,ping from node 1 to node 2 Fail!!"
		exit()		
	logobj.AppendLog(logFH,("\n Step3 verified successfully!!"))
	#######################################################################
	logobj.AppendLog(logFH,("\n Executing step4\n"))
	logobj.AppendLog(logFH,("enabling eth0 of Node2\n"))
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 and eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	logobj.AppendLog(logFH,("\nNode2 is rebooting!!\n"))
	logobj.AppendLog(logFH,("disabling eth1 of Node3\n"))
	winstaob.Connect_Via_WinSTA(logFH,node3ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node3ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
		exit()
	cmd = "ifconfig eth1 down"
	print "\n\n disabling eth0 and eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node3ip, cmd)
	logobj.AppendLog(logFH,out)
	logobj.AppendLog(logFH,("Please wait,Mesh formation is in progress!\n"))
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"in step4,Sorry the Node2 is unreachable ")
		exit()
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	print "\n\n\nMesh Table output is \n",dev_node_mode
	if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node3 connected to Node1 as FFN successfully!!" ))
		cmd = "ping -c 10 " + node3ip
		out = ''
		out = devobj.execute_via_telnet(node1ip, cmd)	
		out = devobj.check_positive_ping(out)
		if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
			print "ping from node 1 to node 3 Success!!"
		else:
			print "Step2 Failed,ping from node 1 to node 3 Fail!!"
			exit()	
	else:
		cmd = "cat /proc/net/meshap/mesh/table"
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		print "\n\n\nMesh Table output is \n",dev_node_mode
		if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Node3 connected to Node2 as FFN successfully!!" ))
			cmd = "ping -c 10 " + node3ip
			out = ''
			out = devobj.execute_via_telnet(node2ip, cmd)	
			out = devobj.check_positive_ping(out)
			if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
				print "ping from node 2 to node 3 Success!!"
			else:
				print "Step2 Failed,ping from node 2 to node 1 Fail!!"
				exit()	
	logobj.AppendLog(logFH,("\n Step4 verified successfully!!"))
	#####################################################################
	'''Executing Step5'''
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	cmd = "ifconfig eth1 down"
	print "\n\n disabling eth0 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)	
	print "\n Please wait!! mesh network formation is in progress"
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=enabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(2)
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	node2_mode = ''	
	node2_mode = devobj.Get_node_mode(node2ip)
	if node2_mode != "FFN" :
		print "\nNode2 is not FFN!!"
		exit()
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node2 connected as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit() 
	if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node3 connected as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit()
	cmd = "ping -c 10 " + node2ip
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 1 to node 2 Success!!"
	else:
		print "Step5 Failed,ping from node 1 to node 2 Fail!!"
		exit()	
	logobj.AppendLog(logFH,("\n Step5 verified successfully!!"))
	#####################################################################
	'''Executing Step7'''
	'''connecting to Node2 and rebooting '''
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(2)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	cmd = "ifconfig eth1 up"
	print "\n\n enabling eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	'''connecting to Node3 and rebooting '''
	winstaob.Connect_Via_WinSTA(logFH,node3ssid,node3auth)
	time.sleep(2)
	winstaob.WinSTA_Connect(node3ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
		exit()
	cmd = "ifconfig eth1 up"
	print "\n\n enabling eth1 of node3"
	out =''
	out = devobj.execute_via_telnet(node3ip, cmd)
	logobj.AppendLog(logFH,out)
	'''connecting to Node1 and rebooting '''
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(3)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable ")
		exit()
	cmd = "ifconfig eth1 up"
	print "\n\n enabling eth1 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	#print "\n Please wait!! mesh network formation is in progress"
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=enabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
		exit()
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	logobj.AppendLog(logFH,("Making eth0 of Node2 down \n"))
	cmd = 'ifconfig eth0 down'	
	dev_node_mode = devobj.execute_via_telnet(node2ip,cmd)
	logobj.AppendLog(logFH,("Making eth0 of Node3 down \n"))
	dev_node_mode = devobj.execute_via_telnet(node3ip,cmd)
	print "\n Please wait!! mesh network formation is in progress"
	time.sleep(5)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable ")
		exit()		
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	print "\n\n\nMesh Table output is \n",dev_node_mode
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node2 connected as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit() 
	if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node3 connected as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit()
	cmd = "ping -c 10 " + node2ip
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 1 to node 2 Success!!"
	else:
		print "Step7 Failed,ping from node 1 to node 2 Fail!!"
		exit()	
	logobj.AppendLog(logFH,("\n Step7 verified successfully!!"))
	#####################################################################
	'''Executing Step8'''
	'''connecting to Node2 and rebooting '''
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(2)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	'''connecting to Node3 and rebooting '''
	winstaob.Connect_Via_WinSTA(logFH,node3ssid,node3auth)
	time.sleep(2)
	winstaob.WinSTA_Connect(node3ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 of node3"
	out =''
	out = devobj.execute_via_telnet(node3ip, cmd)
	logobj.AppendLog(logFH,out)
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=enabled"
	winstaob.Exec_Win_Cmd(cmd)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 failed to \
					boot within expected time\n")
		exit()
	node1_mode = ''	
	node1_mode = devobj.Get_node_mode(node1ip)
	if node1_mode != "FFR" :
		print "\nStep8 failed,Node1 is not FFR!!"
		exit()	
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 failed to\
				 boot within expected time\n")
		exit()
	node3_mode = ''	
	node3_mode = devobj.Get_node_mode(node3ip)
	if node3_mode != "FFR" :
		print "\nStep8 failed,Node3 is not FFR!!"
		exit()	
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 failed to\
				 boot within expected time\n")
		exit()
	node2_mode = ''	
	node2_mode = devobj.Get_node_mode(node2ip)
	if node2_mode != "FFR" :
		print "\nStep8 failed,Node2 is not FFR!!"
		exit()
	logobj.AppendLog(logFH,("\n Step8 verified successfully!!"))
	#####################################################################
	'''Executing Step9'''
	cmd = "ifconfig eth0 down"
	logobj.AppendLog(logFH,("\n\n diabling eth0 of node1"))
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,("\n\n diabling eth0 of node2"))
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,("\n\n diabling eth0 of node3"))
	out =''
	out = devobj.execute_via_telnet(node3ip, cmd)
	''' converting lower case to upper case for sorting '''
	node1_mac = node1_mac.upper()
	node2_mac = node2_mac.upper()
	node3_mac = node3_mac.upper()
	sorted_mac_list = [ node1_mac , node2_mac ,node3_mac ]
	''' below list will be sorted in ascending order '''
	sorted_mac_list.sort()
	if (sorted_mac_list[2] == node1_mac ):
		''' connecting to node3 and check its mode should be LFN '''
		winstaob.Connect_Via_WinSTA(logFH,node3ssid,node3auth)
		time.sleep(2)
		winstaob.WinSTA_Connect(node3ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node3ip)
		if node_status == "Timeout":
			logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
			exit()
		node3_mode = ''	
		node3_mode = devobj.Get_node_mode(node3ip)
		if node3_mode != "LFN" :
			print "\nNode3 is not LFN!!"
			exit()
		''' connecting to node2 and check its mode should be LFN '''	
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(2)
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout":
			logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
			exit()
		node2_mode = ''	
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFN" :
			print "\nNode2 is not LFN!!"
			exit()
		''' connecting to node1 and check its mode should be LFRS '''	
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(2)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout":
			logobj.AppendLog(logFH,"Sorry the Node1 is unreachable ")
			exit()
		node1_mode = ''	
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFRS" :
			print "\nNode1 is not LFRS!!"
			exit()
		dev_node_mode = ''
		cmd = "cat /proc/net/meshap/mesh/table"
		dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
		if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Node2 connected as LFN successfully!!" ))	
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit() 
		if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Node3 connected as LFN successfully!!" ))	
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()				
	
	elif (sorted_mac_list[2] == node2_mac ):
		''' connecting to node1 and check its mode should be LFN '''	
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(2)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout":
			logobj.AppendLog(logFH,"Sorry the Node1 is unreachable ")
			exit()
		node1_mode = ''	
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFN" :
			print "\nNode1 is not LFN!!"
			exit()
		''' connecting to node3 and check its mode should be LFN '''
		winstaob.Connect_Via_WinSTA(logFH,node3ssid,node3auth)
		time.sleep(2)
		winstaob.WinSTA_Connect(node3ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node3ip)
		if node_status == "Timeout":
			logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
			exit()
		node3_mode = ''	
		node3_mode = devobj.Get_node_mode(node3ip)
		if node3_mode != "LFN" :
			print "\nNode3 is not LFN!!"
			exit()
		''' connecting to node2 and check its mode should be LFRS '''	
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(2)
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout":
			logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
			exit()
		node2_mode = ''	
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFRS" :
			print "\nNode2 is not LFRS!!"
			exit()
		dev_node_mode = ''
		cmd = "cat /proc/net/meshap/mesh/table"
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		if re.search(node1_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Node1 connected as LFN successfully!!" ))	
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit() 
		if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Node3 connected as LFN successfully!!" ))	
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()
	else:
		''' connecting to node1 and check its mode should be LFN '''	
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(2)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout":
			logobj.AppendLog(logFH,"Sorry the Node1 is unreachable ")
			exit()
		node1_mode = ''	
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFN" :
			print "\nNode1 is not LFN!!"
			exit()
		''' connecting to node2 and check its mode should be LFN '''	
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(2)
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout":
			logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
			exit()
		node2_mode = ''	
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFN" :
			print "\nNode2 is not LFN!!"
			exit()
		''' connecting to node3 and check its mode should be LFRS '''
		winstaob.Connect_Via_WinSTA(logFH,node3ssid,node3auth)
		time.sleep(2)
		winstaob.WinSTA_Connect(node3ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node3ip)
		if node_status == "Timeout":
			logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
			exit()
		node3_mode = ''	
		node3_mode = devobj.Get_node_mode(node3ip)
		if node3_mode != "LFRS" :
			print "\nNode3 is not LFRS!!"
			exit()	
		dev_node_mode = ''
		cmd = "cat /proc/net/meshap/mesh/table"
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		if re.search(node1_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Node1 connected as LFN successfully!!" ))	
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit() 
		if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Node2 connected as LFN successfully!!" ))	
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()
	cmd = "ping -c 10 " + node2ip
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 1 to node 2 Success!!"
	else:
		print "Step9 Failed,ping from node 1 to node 2 Fail!!"
		exit()	
	logobj.AppendLog(logFH,("\n Step9 verified successfully!!"))
	#####################################################################		
	"""
	#######################################################################
	print "\n checking step2(FFR,FFN,FFN)\n"
	logobj.AppendLog(logFH,("Making eth0 of Node2 down \n"))
	cmd = 'ifconfig eth0 down'	
	dev_node_mode = devobj.execute_via_telnet(node2ip,cmd)
	time.sleep(5)
	logobj.AppendLog(logFH,("Making eth0 of Node3 down \n"))
	cmd = 'ifconfig eth0 down'	
	dev_node_mode = devobj.execute_via_telnet(node3ip,cmd)
	time.sleep(15)
	node1_mode = devobj.Get_node_mode(node1ip)
	if node1_mode != "FFR" :
		print "\nStep2 failed,Node1 is not FFR!!"
		exit()
	
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
		exit()		
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	print "\n\n\nMesh Table output is \n",dev_node_mode
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node2 connected as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit() 
	if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node3 connected as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit()   
	print "\n Step2 is successfully verified!!!"
	#########################################################################
	print "\n checking step3(LFRS,LFN,LFN)\n"
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=disabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(5)
	'''connecting to node1 '''
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(5)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable ")
		exit()	
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(2)
	'''connecting to node2 '''
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 and eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	winstaob.WinSTA_Connect(node2ssid)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	cmd = "ifconfig eth1 down"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)	
	winstaob.WinSTA_Connect(node2ssid)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	########################################################
	'''connecting to node 3'''
	winstaob.Connect_Via_WinSTA(logFH,node3ssid,node3auth)
	time.sleep(10)
	winstaob.WinSTA_Connect(node3ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
		exit()
		
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 and eth1 of node3"
	out =''
	out = devobj.execute_via_telnet(node3ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	winstaob.WinSTA_Connect(node3ssid)
	time.sleep(5)
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
		exit()
	cmd = "ifconfig eth1 down"
	out =''
	out = devobj.execute_via_telnet(node3ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)  
	#############################################################  
	''' checking mesh formation '''
	print "\n Please wait mesh network formation"
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable ")
		exit()
	node1_mode = ''	
	node1_mode = devobj.Get_node_mode(node1ip)
	if node1_mode != "LFRS" :
		print "\nNode1 is not LFRS!!"
		exit()
			
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	#print "\n\n\n++++++++++++++++ \n",dev_node_mode
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node2 connected as LFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit() 
	if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node3 connected as LFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit()
	print "\n Step3 verified successfully!!"
	
	''' Executing Step4!!To verify (FFR,FFN,FFN) '''
	print "\n Executing Step4!!To verify (FFR,FFN,FFN) \n"
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable ")
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	print "\n Please wait untill the Node1 reboots"
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=enabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable ")
		exit()
	node1_mode = ''	
	node1_mode = devobj.Get_node_mode(node1ip)
	if node1_mode != "FFR" :
		print "\nNode1 is not FFR!!"
		exit()
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node2 connected as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit() 
	if re.search(node3_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Node3 connected as FFN successfully!!" ))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit()
	logobj.AppendLog(logFH,("\n Step4 verified successfully!!"))
	
	'''Step5 checking all the Nodes are FFR after rebooting'''
	logobj.AppendLog(logFH,("\nExecuting Step5!!To verify (FFR,FFR,FFR)"))
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable ")
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	winstaob.Connect_Via_WinSTA(logFH,node3ssid,node3auth)
	time.sleep(10)
	winstaob.WinSTA_Connect(node3ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 is unreachable ")
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 of node3"
	out =''
	out = devobj.execute_via_telnet(node3ip, cmd)
	logobj.AppendLog(logFH,out)
	print "\n Trying to connect to Nodes..!please wait\n"
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 failed to bootup within expected time")
		exit()
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 failed to bootup within expected time")
		exit() 
	node_status = ''
	node_status = devobj.check_node_status(node3ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node3 failed to bootup within expected time")
		exit()
	logobj.AppendLog(logFH,("\nVerifying nodes mode!!\n"))
	cmd = 'cat /proc/net/meshap/mesh/adhoc_mode'
	dev_node_mode = ''
	for i in Nodes :
		user = devobj.get_from_setup(i, "USER_NAME")
		ipaddr = devobj.get_from_setup(i,"IP")
		logobj.AppendLog(logFH,("Trying to telnet to" + ipaddr)) 
		dev_node_mode = devobj.execute_via_telnet(ipaddr,cmd)
		#time.sleep(2)
		if re.search("FFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\nnode" +(i)+" is FFR\n"))
			
		else :
			logobj.AppendLog(logFH,("\nnode" +(i)+" is not FFR\n"))
			exit
	"""			
	logobj.AppendLog(logFH,"\n\n Test case execution complete!!!")
	logFH.close()  
	exit()