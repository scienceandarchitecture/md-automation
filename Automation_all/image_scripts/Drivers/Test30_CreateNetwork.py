import json
import datetime
import time
import pandas as pd
import csv
import Common_lib
import dev_function
from Common_lib  import CommonOperations
from dev_function import *
import re

def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'w+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+str(NMS_Param)])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return



def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//createNetwork.csv")
    #d_excel("config_data.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    networkId = df.Network_Name[tc]
    Interface_Name = df.Interface_Name[tc]
    networkId = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]
    networkKey = networkId
    

    test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, networkId, Interface_Name, networkId, API_Server_IP,networkKey]
                 
    return test_params

def Setup_Parser_method(tc):
    count = 0
    while count <= tc:
        if count == tc:
            data = pd.read_csv("..//Data//Test1_Setup.csv")
            NAME = data.NAME[count]
            MAC = data.MAC[count]
            Dev_IP =  data.IP[count]
            TYPE = data.TYPE[count]
            MODEL = data.MODEL[count]
            NODE_USER = data.USER_NAME[count]
            setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
            return setup_values
        count = count+1

"""
The preferred parent is updated using nms api and
then re-checking the value on Device after restarting the node """              
          
class Test30_CreateNetwork(object):
    def init(self):
        self.object = object
    def Test30_CreateNewNetwork(self,tc):    
        #def Create_network(self):
        tc = 0
        val = Setup_Parser_method(tc)
        NAME = val[0]
        Dev_MAC_Address = val[1]
        NMS_Device_IP = val[2]
        Type = val[3]
        Model = val[4]
        Node_User= val[5]
        api_data = Input_Data_Parser(tc)
        api_data[0]
        API_Name = api_data[0]
        Method = api_data[1]
        Content_Type = api_data[2]
        Arg1 = api_data[3]
        Exp_st_code= api_data[4]
        Precondition = api_data[5]
        networkId= api_data[6]
        Interface_Name = api_data[7]
        Network_Name = api_data[8]
        API_Server_IP= api_data[9]
        networkKey = api_data[10]
        url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
        print url
        
    
        Data_value = '{'+'"networkId"'+':'+'"'+networkId+'"' + " , "+ '"networkKey"'+':'+'"'+networkKey+'"'+'}'
        #Data_value = '{'+'"preferedParent"'+':'+" "+ str(preferedParent)+" " +'}'
        print Data_value 
        print networkId
        Dev_IP= NMS_Device_IP 
        verify_api_param = 'networkId'
        #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
        current_time = time.localtime()
        s= CommonOperations()
        tc_strt = datetime.datetime.now()            
        """here instead of interface name we have modified the common 
        operation and instead sending the PP"""
        #tc, Method, url, Content_Type, Data_value, Exp_st_code, Precondition, Supported_protocol, Data_value, Interface_Name, Network_Name, API_Server_IP)
        p=s.commonOperations(tc, Method, url, Content_Type, Data_value, Arg1, Exp_st_code, networkId, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
        op = json.dumps(p,ensure_ascii=True)
        op1 = json.loads(op)
        
        if op.find('"rebootRequired":No'):
            print "reboot is not required"
            print "New network is successfully created..."
        
        elif op.find('"rebootRequired":Yes'):
            rebootAPI="reboot"
            RebootMethod = "POST"
            url = "http://"+API_Server_IP+":8080"+"/NMS/"+rebootAPI+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
            print "Rebooting Node, please wait till node gets Up"
            s= CommonOperations()
            tc_strt = datetime.datetime.now()
            p=s.commonOperations(tc, Method, url, Content_Type, Data_value, Arg1, Exp_st_code, networkId, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
            op = json.dumps(p, ensure_ascii=True)
            op1 = json.loads(op)
            
            if op1.find('"Node rebooted  successfully"'):
                print "Node Rebooting Now, please wait while node reboots..."
            devobj = dev_function()
            reboot_Node = devobj.check_node_status(NMS_Device_IP)
            print reboot_Node
            if reboot_Node=="Success":
                print "Node Rebooted successfully..."
                print "The networkId is now ", networkId
                #dev_Logfilename = "Test21_UpdateCountryCode_NMS_Output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
                print "New network created successfuly"
        
        elif op.find("rebootRequired== null"):
                print "Node giving null value"
                print "Which Means either preferedParent was updated but not rebooted or not updated properly"
                #print "Request output is", op        
        current_time = time.localtime()    
        filename = str(time.strftime('%Y_%m_%d_%H_%M', current_time))
        nms_filename = "Test30_CreateNetwork_NMS_Output"+filename
        nms_filepath = ".//Output//"+nms_filename+'.csv'
        nms_log = open(nms_filepath, 'w')
        nms_log.write(op)
        nms_log.close()
        print "NMS output saved in logfile"
                       
        """ Verification of Device Data """
        dev_Logfilename = "Test30_CreateNetwork_Device_output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
        dev_filepath = ".//Output//"+dev_Logfilename
    
        verify_param = networkId
        telnet_user = "root"
        t = dev_function()
        dev_meshId2 = t.get_mesh_id(telnet_user, dev_filepath,Dev_IP)
        
        print dev_meshId2
        if str(dev_meshId2) == str(networkId):
            print "Success, MeshId matched on Device and NMS..."
            print "New Network create successfully on device..."
            test_status = "Pass"
            addOutput(dev_filepath,op, networkId, tc, test_status)
            
            
        else:
            print "Fail, Node_essid  and Device Node_essid are not matching"
            test_status = "Fail"
            addOutput(dev_filepath,op, networkId, tc,test_status )
        
        
        #     print "Device Data Verification Starts Now.."
        #     dev_Logfilename = "Test22_preferedParent_NMS_Output"\
        #     +str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
        #     dev_filepath = ".\\Output\\"+dev_Logfilename
        #     
        #     telnet_user = "root"
        #     interface = "global"
        #     devobj = dev_function()
        #     dev_preferedParent = devobj.Get_Preferred_Parent(telnet_user, dev_filepath, NMS_Device_IP, interface)
        #     print dev_preferedParent
        #       
        #     if str(networkId.upper()) in str(dev_preferedParent.upper()):
        #         print "Success, Prefered Parent On NMS & Device Matched.."
        #         test_status = "Pass"
        #         addOutput(dev_filepath,op, networkId, tc, test_status)
        # 
        #     else:
        #         print "Fail, Prefered Parent on NMS  and Device Node_CountryCode are not matching"
        #         test_status = "Fail"
        #         addOutput(dev_filepath,op, networkId, tc,test_status )
