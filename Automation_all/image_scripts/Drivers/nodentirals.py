import datetime
import time
import pandas as pd
import csv
import sys
#import dev_function_dev
from Common_lib  import *

import re
import json
import dev_function
#import Win_Sta
from dev_function import *
import os
import telnetlib
import copy
from subprocess import CalledProcessError, check_output



def Get_node_mode(ipaddr):
        cmd = 'cat /proc/net/meshap/mesh/adhoc'
        dev_node_mode = ''
        user= "root"        
        tn = telnetlib.Telnet(ipaddr)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            tn.write("root"+'\r\n')
            print "in login successfully"
    
            console_val = tn.read_until('Password:')
            print console_val


            if 'Password:' in console_val:
                print "Password is needed"
                tn.write('\n')
        


        print "Login successfull..."  
        
        #print cmd
        tn.write(cmd+'\n')
        time.sleep(5)
        valconsole = tn.read_until('INDEX')
        #print valconsole
        
        dev_node_mode = valconsole
        time.sleep(2)
        if re.search("FFN",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
            return ("FFN")
        elif re.search("FFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
            return ("FFR")
        elif re.search("LFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
            return ("LFR")
        elif re.search("LFN",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
            return ("LFN")
#         elif re.search("LFRS",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
#             return ("LFRS") 
        else:
            return "Not found" 












def get_from_setup(num_node, column_name):
    count =0
    
#     column_name = "MAC"
    maclist = []
    while count < num_node:
    
        data = pd.read_csv("..//Data//Test1_Setup.csv")
        NAME = data.NAME[count]
        MAC = data.MAC[count]
        maclist.append(MAC)
        count = count+1
        
        

    
    return maclist
    
#     with open("..//Data//Test1_Setup.csv", 'r') as f:
#         file_read = csv.DictReader(f)
#         flag = 0     
#         for row in file_read:
#             #print row
#             if row['NAME'] == rowname :
#                 flag = 1
#                 res = row[column_name]
#                 return res


def checktable (ipaddr, cmd):
        user= "root"
        count = 0
        tn = telnetlib.Telnet(ipaddr)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
            #print "please login to continue"
            tn.write("root"+'\n')
            # print "login successfully"
            console_val = tn.read_until(':')


            if 'Password:' in console_val:
                tn.write(""+"\n")
                log_val = tn.read_until('OpenWrt')
                #print log_val
        
        
        print "Login successfull..."  
        
        print cmd
        #time.sleep(5)
        tn.write(cmd+'\n')
        time.sleep(4)
        valconsole = tn.read_until('OpenWrt')
#         print valconsole
        return valconsole



def execute_via_telnet (ipaddr, cmd):
        user= "root"        
        tn = telnetlib.Telnet(ipaddr)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            tn.write("root"+'\r\n')
            print "in login successfully"
    
            console_val = tn.read_until('Password:')
            print console_val


            if 'Password:' in console_val:
                print "Password is needed"
                tn.write('\n')
        


        print "Login successfull..."   
        
        print cmd
        #time.sleep(5)
        tn.write(cmd)
        time.sleep(2)
	tn.write(cmd)
        time.sleep(2)
	tn.write(cmd)
        time.sleep(2)
	tn.write(cmd)
        time.sleep(2)


#         valconsole = tn.read_until('INDEX')
#         print valconsole
        return "Pass"





def check_node_status(ipadrr):
        toolbar_width = 41
        flag = cptr = 0 #cptr is cursor pointer
        cmd = "ping -c 5 " + ipadrr
        sys.stdout.write("[%s]" % (" " * toolbar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (toolbar_width+1))
        while cptr < (toolbar_width - 1) :
            pingstr = None
            try:
                pingstr = check_output(cmd , shell=True)
                returncode = 0
            except CalledProcessError as errorno:
                output = errorno.output
                returncode = errorno.returncode
                if returncode :
                    temppingstr = "Request timed out" * 3
            if pingstr is not None : 
                temppingstr=copy.copy(pingstr)
            ReqtimeOut = re.search('Request timed out',temppingstr,re.I)
            Destunreach = re.search('Destination host unreachable',temppingstr,re.I)
            if ReqtimeOut:
                cptr = cptr + 4
                sys.stdout.write("====")
                sys.stdout.flush()
                continue
            elif Destunreach:
                cptr = cptr + 4
                sys.stdout.write("====")
                sys.stdout.flush()
                continue
            else :
                flag = 1
                sys.stdout.write(("=" * (toolbar_width - cptr)))
                sys.stdout.flush()
                sys.stdout.write("\n")
                return "Success"
                #break
        if flag==1:
            return "Success"
        return "Timeout"

def find_Wlan2mac(ip, iface):
    #dev_obj = dev_function()
    
    cmd = 'ifconfig '+ iface +"\n" 
    print cmd  
    tn = telnetlib.Telnet(ip)
    tn.write(cmd)
    val = tn.read_until('Mask', 20) 
    print val
    for line in val.splitlines():
        mat=re.search(r'Link encap\:',line)
        if mat:
            mat1 = re.search(r'(HWaddr\s)(([0-9-A-Z]{1,2}\:)(\w{2}\:){4}([0-9-A-Z]{1,2}))',line)
            print mat1.group(2)


def get_config_values(count):
    
    df = pd.read_csv("..//Config//Three_Node_transition_Config.csv")    
    node_MeshId= df.MESH_ID[count]
    node_essid = df.ESSID[count]
    node_channel = df.CHANNEL[count]
    node_name = df.NAME[count]
    node_interface = df.INTERFACE[count]
    return [node_MeshId, node_essid, node_channel, node_name, node_interface]

def Setup_Parser_method(count):
    
    data = pd.read_csv("..//Data//Test1_Setup.csv")
    NAME = data.NAME[count]
    MAC = data.MAC[count]
    Dev_IP =  data.IP[count]
    TYPE = data.TYPE[count]
    MODEL = data.MODEL[count]
    NODE_USER = data.USER_NAME[count]
    Node_eth1_ip = data.ETH1_IP[count]
    setup_values = [NAME, MAC, Dev_IP, TYPE, MODEL, NODE_USER, Node_eth1_ip]
    return setup_values


class N_NodeTransition():
    def init(self):
        self.object = object
    tc = 0
    dev = dev_function()
    #logFH = logobj.CreateLog(scriptname)
    configfname = "Three_Node_transition_Config"
    #logobj.AppendLog(("\nExecuting Test script "+scriptname))
    
    
    
    print "Testcase begins now"
    sttime = datetime.datetime.now()
    print "start time of testcase", sttime
    data = pd.read_csv("..//Data//Test1_Setup.csv")
    num_node = len(data)
    num_node = 3
    node = []
    node_config=[]
    print num_node
    
    row_num=0
    node_setup = []
    node_config=[]
    row_num=0
 
    
    while row_num < num_node:
        #Config_Parser_method(row_num)
        val = Setup_Parser_method(row_num)
        NAME = "Name"+str(row_num) 
        NAME_val = val[0]
        Dev_MAC_Address = "Dev_MAC_Address"+str(row_num)
        Dev_MAC_Address_val = val[1]
        NMS_Device_IP = "NMS_Device_IP"+str(row_num)
        NMS_Device_IP_val = val[2]
        Type = "Type"+str(row_num)
        Type_val = val[3]
        Model = "Model"+str(row_num)
        Model_val = val[4]
        Node_User = "Node_User"
        Node_User_val = val[5]
        Node_eth1_ip = "Node_Eth1_ip"+str(row_num)
        Node_eth1_IP_val = val[6]
        nodeval="nodeval"
        nodeval = nodeval+str(row_num)
        nodeval = {NAME:NAME_val, Dev_MAC_Address:Dev_MAC_Address_val, NMS_Device_IP:NMS_Device_IP_val,Type:Type_val, Model:Model_val,Node_User:Node_User_val, Node_eth1_ip : Node_eth1_IP_val}
        ''' For all the node the value will be set in this loop '''
        
        node_setup.append(nodeval)
        print nodeval,"\n"
        
        conf_val = get_config_values(row_num)
        node_meshId = "Node_MeshId"+str(row_num)
        node_meshId_val = conf_val[0]
        node_essid = "node_essid"+str(row_num)
        node_essid_val = conf_val[1]
        node_channel = "node_channel"+str(row_num)
        node_channel_val = conf_val[2]
        node_interface = "node_interface"+str(row_num)
        node_interface_val = conf_val[4]
        
        node_info = {}
        node_info['mesh_id'] = node_meshId
        node_info['essid'] = node_essid
        
        #config = devobj.config_node_via_telnet(node_interface,Dev_MAC_Address, NMS_Device_IP, node_info,Node_User)
        node_info = "node_info"+str(row_num)
        node_info = {node_meshId:node_meshId_val, node_essid:node_essid_val, node_channel:node_channel_val, node_interface:node_interface_val}
        ''' all the config info of node will be set in this loop '''
        node_config.append(node_info)
        print node_info,"\n"
        row_num = row_num+1
    
#    print "setup info ",node_setup
#    print "config info ",node_config 

#    ncount = 0
#     ''' Step1 starts here '''
#     while ncount < num_node:
#           
#          nmsip = "NMS_Device_IP"+str(ncount)
#           
#          vals = node_setup[ncount]
#          print "vals is",vals
#          nodeip= vals[nmsip]
#     #         print nodeip
#          node_status = check_node_status(nodeip)
#          if node_status == "Success":
#              print "For node with ip "+ nodeip +" ping status -->> ", node_status
#     #         else: 
#              print "For node with ip "+ nodeip +" ping status -->> ", node_status
#              print "Node is down, Execution halted"
#               exit()
#          ncount=ncount+1
#     print "step1 ends here"
#     print "=========**************==============***********============="
#     print '\n\n'
     
     
#     '''  Step 2 starts here, checking if all are set to FFR or not '''
#     print "Step 2 starts here, checking if all are set to FFR or not..."
#     node_count=0
#     while node_count < num_node:
#         nmsip = "NMS_Device_IP"+str(node_count)
#         nmsmac = "Dev_MAC_Address"+str(node_count)
#         vals = node_setup[node_count]
#         nodeip= vals[nmsip]
#         nodemac = vals[nmsmac]
#         node_eth1_ip = vals[Node_eth1_IP_val]
#         nodemode = dev.Get_node_mode(nodeip) 
#         if nodemode == "FFR":
#             print "Node "+str(node_count)+" with Ip address "+str(nodeip)+" is "+"FFR"
#         if nodemode != "FFR":
#             print "Invalid setup, cannot proceed"
#             print "please wait, making necessary changes to the setup..."
#             cmd = "reboot"+"\n"
#             dev.execute_via_telnet(nodeip, cmd)
#             node_status = check_node_status(nodeip)
#             if node_status == "Success":
#                 print "For node with ip "+ nodeip +" the  setup is now ready"
#             else:
#                 print "Node is down, Execution halted"
#                 exit()
#           
#         node_count = node_count+1
#     print "All the nodes are set as FFR"
#     print "Step-2 ends"
#     print "==========**************=============**************=============*********************"
#     print '\n' 
 

#     '''  Step 3 begins here, to make last node as FFR checking rest will be set to FFN   '''
#     print "checking FFR and FFN Case.."
#     node_count3 = 0
#     while node_count3 < num_node:
#         maclist = []
#         ip_list=[]
#         node4list=[]
#         node4_values = {}
#         mip0_mac_list = get_from_setup(node_count3, "MAC")
#         wlan0_mac_list = get_from_setup(node_count3, "WLAN0_MAC")
#         Node_eth1_IP_val_list = get_from_setup(node_count3, "Node_eth1_IP_val")
#         val = Setup_Parser_method(node_count3)
#         nmsip = val[2]        
#         nmsmac = val[1]
#         nodeip =val[2]
#         nodemac = val[1]
# #         #nmsmac = "Dev_MAC_Address"+str(node_count3)
# # #         vals = node_setup[node_count3]
# #         Node_eth1_IP_val = val[6] 
# #         nodeip= vals[nmsip]
# #         nodemac = vals[nmsmac]
#  
#  
#        #""" Remove from below"""
#         print Node_eth1_IP_val
#         #nodemode = Get_node_mode(Node_eth1_IP_val) 
#         if node_count3 == 0:
#                
#             print Node_eth1_IP_val
#             cmd = "ifconfig "+"eth0"+ " down \n"
#             disableInterstatus = execute_via_telnet(Node_eth1_IP_val, cmd)
#                 
#             time.sleep(30)
#            # node1_down=dev.Disable_interface(nodeip, "eth0")
#             print "Status of disable interface", disableInterstatus
# #             node2_status = dev.check_node_status(Node_eth1_IP_val)
# #             print "waiting till node1 get up"
# #             time.sleep(20)
#             node_mode = Get_node_mode(Node_eth1_IP_val)
#             print "Node1 mode is now set to ", node_mode
#                 
#             if node_mode == "FFN":
#                 print "Node mode is set as expected..."
#                 print "Node "+str(ncount)+" with Ip address "+str(Node_eth1_IP_val)+" is "+node_mode
#                 #node_mode = dev.Get_node_mode(nodeip)
# #             else:
# #                 print "Node is not set correctly..."
#         else:
#             if node_count3 > 0:
#                 node_mode = Get_node_mode(Node_eth1_IP_val)
#                 print "Elsed part nodemode", node_mode
#                 if node_mode == "FFR":
#                     print "Node set to FFR as expected.."
#                     print "performing the next step of disabling eth0"
#                            
#                     print node_mode
#                 cmd = "ifconfig "+"eth0"+ " down \n"
#                 disableInterstatus = execute_via_telnet(Node_eth1_IP_val, cmd)
#                 print "Status of disable interface = ", Node_eth1_IP_val,disableInterstatus 
#                 time.sleep(29)
#                   
#                    
#            # if node_count3 == num_node:
#            # print "Eth0 of the last node is also down now.."
#             #print "Checking the nodemode set to LFR or Not..."
#               
#               
#               
#             print "Node which has eth0 was set as FFR and node which has eth0 down was set as FFN"
#             
#             print "=============**************=============**************=============***************"
#             print '\n' 
#   
             
    print "Step4 Begins Now"
    print '\n'
    print "Verifying all the nodes are FFN except one node"
    #print "Making sure all are down..."
    node_count3 =0
    while node_count3 < num_node:
        maclist = []
        ip_list=[]
        node4list=[]
        node4_values = {}
        mip0_mac_list = get_from_setup(node_count3, "MAC")
        wlan0_mac_list = get_from_setup(node_count3, "WLAN0_MAC")
        Node_eth1_IP_val_list = get_from_setup(node_count3, "Node_eth1_IP_val")
        val = Setup_Parser_method(node_count3)
        nmsip = val[2]        
        nmsmac = val[1]
        nodeip =val[2]
        nodemac = val[1]
	Node_eth1_IP_val = val[6]
        
        print Node_eth1_IP_val
        #nodemode = Get_node_mode(Node_eth1_IP_val) 
        if node_count3 == 0:
                
            print Node_eth1_IP_val

            cmd = "ifconfig eth0 up"
            enableInterface = execute_via_telnet(Node_eth1_IP_val, cmd)
            FFR_Node = Node_eth1_IP_val     
            time.sleep(60)
            # node1_down=dev.Disable_interface(nodeip, "eth0")
            print "FFR node is ",FFR_Node
#             node2_status = dev.check_node_status(Node_eth1_IP_val)
#             print "waiting till node1 get up"
#             time.sleep(20)
            node_mode = Get_node_mode(Node_eth1_IP_val)
            print "node mode is", node_mode
                 
            if node_mode == "FFR":
                print "Node mode is set as expected..."
                print "Node "+str(node_count3)+" with Ip address "+str(Node_eth1_IP_val)+" is "+node_mode
                #node_mode = dev.Get_node_mode(nodeip)
            else:
                print "Node is not set correctly... to FFR"
                enableInterface = execute_via_telnet(Node_eth1_IP_val, cmd)
                FFR_Node = Node_eth1_IP_val 
                time.sleep(80)
                print "Checking the node mode after enabling eth0..."        
                node_mode = Get_node_mode(Node_eth1_IP_val)
                print "after enabling from else part node mode is : ", FFR_Node
                
        else:
            print "Now Verifying all the other nodes are set to FFN"

            if node_count3 > 0:
                val = Setup_Parser_method(node_count3)
                #Node_eth1_IP_val = get_from_setup(node_count3, "Node_eth1_IP_val")
                node_mode = Get_node_mode(Node_eth1_IP_val)                
                if node_mode == "FFR":
                   
                    print "performing the next step of disabling eth0"                        
                    print node_mode
                cmd = "ifconfig eth0 down"
                disableInterstatus = execute_via_telnet(Node_eth1_IP_val, cmd)
                time.sleep(5)
                print "Disabled the eth0 interface for node with IP"+ ": "+Node_eth1_IP_val+" and the status of disabled interface is: "+ disableInterstatus 
                time.sleep(80)
                print "Checking the node mode after deisabling eth0 of remaining ndoes..."        
                node_mode = Get_node_mode(Node_eth1_IP_val)
                print "node mode is set to "+node_mode+" for node : "+str(Node_eth1_IP_val)
		 #print "Node which has eth0 was set as FFR and node which has eth0 down was set as FFN"
         
		print "=============**************=============**************=============***************"
		print '\n' 


		print "Setting the node to LFR, which was set to FFR", FFR_Node
		print "Disabling the ETH0 of FFR Node..."
	    
		cmd = "ifconfig eth0 down"
		disableInterstatus = execute_via_telnet(FFR_Node, cmd)
		time.sleep(90)
    
        node_count3 = node_count3 + 1
                    
        #if node_count3 == num_node-1:
         #   print "Last node status checked, Verifying the LFR and LFN Case now..."
            
            
           
           
       
    
        
    #     ncount4 = 0
    #     print "LFN and LFR Verificaiton starts now..."
    #     while ncount4 < num_node:
    #         maclist = []
    #         ip_list=[]
    #         node4list=[]
    #         node4_values = {}
    #         mip0_mac_list = get_from_setup(ncount4, "MAC")
    #         wlan0_mac_list = get_from_setup(ncount4, "WLAN0_MAC")
    #         Node_eth1_IP_val_list = get_from_setup(ncount4, "Node_eth1_IP_val")
    #         val = Setup_Parser_method(ncount4)
    #         nmsip = val[2]        
    #         nmsmac = val[1]
    #         nodeip =val[2]
    #         nodemac = val[1]
    #         print nodemac, nodeip, nmsip, nmsmac
            
         
        print "Now Verifying that the FFR is set to LFR..."
        node_mode = Get_node_mode(FFR_Node)
        time.sleep(2)
        #node_mode = Get_node_mode(Node_eth1_IP_val) 
        print "Node mode is ",node_mode
        nsetupcnt = 0
        if node_mode == "LFR":
            print "After disbling eth0 interface for FFR Node, node mode should changed to LFR"
            
            print "Node mode is set to LFR for node with IPAddresse: ",FFR_Node
            print "Now checking the LFN Nodes using Table command..."
            cmd = "cat /proc/net/meshap/mesh/table"
            opstr = checktable(Node_eth1_IP_val, cmd)
            print opstr
            for line in opstr.splitlines():
                print line
                  
            maclist1  = get_from_setup(num_node, "MAC")
            #macaddsetup = str(maclist1).Upper()
            #print "mac list is ",maclist1
            mat = re.search(r'(([0-9-A-Z]{1,2}\:)(\w{2}\:){4}([0-9-A-Z]{1,2}))',line, re.I)
            #print "mat", mat
             
            if mat != None:
                mat = mat.group()
                mat = str(mat).upper()
                print mat
                for mac in maclist1:
                    mac = str(mac).upper()
                    if mat == mac:
                        #matobj = re.search(macid,opstr,re.IGNORECASE|re.MULTILINE) 
                        #if mat.group(1):
                        print " Node with mac address"+" "+mat+" is part of mesh network"
                        #continue
            #                 else:
            #                      print  "Node mac not found in table..."
                #nsetupcnt = nsetupcnt +1    
            #                     LFR_node_name = Get_node_name_from_setup(nodeip)
            #                     LFR_Node_mip0_mac = get_from_setup(LFR_node_name,"MAC")
            #                     if (mat.upper() == LFR_Node_mip0_mac.upper()):
            #                         continue
            #                     else:
            #                         print "\n The node with ",mat," is not part of mesh network!!"
                   # print "\n step 4 Failed"
                #for line in opstr.splitlines():     
            #                 mat = re.search(r'(([0-9-A-Z]{1,2}\:)(\w{2}\:){4}([0-9-A-Z]{1,2}{6}))',line, re.I| re.MULTILINE)
            #                 mat = mat.group()
            #                 print mat
            #                 count =1
                
                 
            else :
                print "node is set as ", node_mode
            
             
         
        print "Step 4 sucessfully completed here..."
        print "/n"
        print "==========**************=============**************=============*********************"
        print '\n' 
    
    
    print "Now verifying that nodes mode sets to FFR after rebooting..."
    node_count3 =0
    while node_count3 < num_node:
        
        #val = Setup_Parser_method(node_count3)
        data = pd.read_csv("..//Data//Test1_Setup.csv")
        try:
            
            Node_eth1_IP_val = data.ETH1_IP[node_count3]
            Node_eth1_IP_val_list=[Node_eth1_IP_val]
            cmd= "reboot \n"
            execute_via_telnet(Node_eth1_IP_val, cmd)
            print "Node reboted: ",Node_eth1_IP_val
        except:
            print "Node not yet rebooted, again rebooting.."
            cmd= "ifconfig eth0 up \n"
            execute_via_telnet(Node_eth1_IP_val, cmd)
	    node_status = dev.check_node_status(Node_eth1_IP_val)
	    print "Node status after eth0 up is",node_status
        
        node_count3 =node_count3+1
        
        
    print "All node rebooted to make them ffr"    
    node_count3 =0
    while node_count3 < num_node:
        #val = Setup_Parser_method(node_count3)
        try:
            data = pd.read_csv("..//Data//Test1_Setup.csv")
            
            Node_eth1_IP_val = data.ETH1_IP[node_count3]
            
            
            #Node_eth1_IP_val_list=[Node_eth1_IP_val]        
            time.sleep(90)
            print "Status of rebooted node is ",check_node_status(Node_eth1_IP_val)
            node_mode = Get_node_mode(Node_eth1_IP_val)
            print node_mode
            if node_mode == "FFR":
                print "node_mode set as FFR for the IP ", Node_eth1_IP_val
            else:
                print "Node mode is ", node_mode
            if check_node_status=="Success":
                time.sleep(60)
                node_mode = Get_node_mode(Node_eth1_IP_val)
                print node_mode
                if node_mode == "FFR":
                    print "node_mode set as FFR for the IP ", Node_eth1_IP_val
                else:
                    print "Node mode is ", node_mode
        except:
            print "Node is pingable not yet reachable..."
            
        node_count3 = node_count3+1
        
    endtime = datetime.datetime.now()
    timetaken = endtime-sttime
    print "End time of testcase", endtime
    print "Total Execution Time is : ",timetaken
    print "Step 5 Ends Now"
    print "==========**************=============**************=============*********************"
    print '\n' 

    
    print "Transition cycle is completed.."
    print "Test completed now.."
    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
 
