import telnetlib
import time
import serial
import copy
import re
import sys
import csv
from subprocess import check_output
from subprocess import CalledProcessError
from pip._vendor.requests.packages.urllib3.util.timeout import current_time
import os
from telnetlib import Telnet
import operator
from Log_linux import *

class dev_function_linux(object):	

	def init(self):
		object = self.object
		return
	
	def check_node_status(self,ipadrr):
		toolbar_width = 41
		flag = cptr = 0 #cptr is cursor pointer
		if(sys.platform == "linux2"):
			cmd = "ping -c 5 " + ipadrr
		elif(sys.platform == "win32") or (sys.platform == "cygwin") :	
			cmd = "ping -n 5 " + ipadrr
		sys.stdout.write("[%s]" % (" " * toolbar_width))
		sys.stdout.flush()
		sys.stdout.write("\b" * (toolbar_width+1))
		while cptr < (toolbar_width - 1) :
			pingstr = None
			try:
				pingstr = check_output(cmd , shell=True)
				returncode = 0
			except CalledProcessError as errorno:
				output = errorno.output
				returncode = errorno.returncode
				if returncode :
					temppingstr = "Request timed out" * 3
			
			if pingstr is not None :	
				temppingstr=copy.copy(pingstr)
				#temppingstr = temppingstr.replace('\n','')
				#temppingstr=temppingstr.replace('.','')
			ReqtimeOut = re.search('Request timed out',temppingstr,re.I)
			Destunreach = re.search('Destination host unreachable',temppingstr,re.I)
			if ReqtimeOut:
				cptr = cptr + 4
				sys.stdout.write("====")
				sys.stdout.flush()
				continue
			elif Destunreach:
				cptr = cptr + 4
				sys.stdout.write("====")
				sys.stdout.flush()
				continue
			else :
				flag = 1
				sys.stdout.write(("=" * (toolbar_width - cptr)))
				sys.stdout.flush()
				sys.stdout.write("\n")
				break				
		if flag:
			return "Success"
		
		return "Timeout"	
	def read_meshapp(self, user, dev_filepath,ipaddress):
		tn = telnetlib.Telnet(ipaddress)
		readval = tn.read_until("login")
		tn.write(user+"\r\n")
		cmd = "cat /etc/meshap.conf"
		tn.write(cmd_essid+"\n")  
		tn.write("exit\n")
		time.sleep(2)
		cmd_res = tn.read_all()
		logfile = open(dev_filepath, 'a')
		logfile.write(cmd_essid)
		logfile.close()
		return cmd_res
	
	
	def Get_SubnetMask(telnet_user,dev_filepath, NMS_Device_IP,interface="global"):
		dev_obj = dev_function()
		meshapconf = dev_obj.read_meshapp(telnet_user , dev_filepath, NMS_Device_IP)
		if (interface == "global"):
			matchObj = re.search('mask\s*\=\s*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', meshapconf, re.M|re.I)
			if matchObj:
				return matchObj.group(1)
		return None
	
	
	def Get_Gateway(telnet_user,dev_filepath, NMS_Device_IP,interface="global"):
		dev_obj = dev_function()
		meshapconf = dev_obj.read_meshapp(telnet_user , dev_filepath, NMS_Device_IP)
		if (interface == "global"):
			matchObj = re.search('gateway\s*\=\s*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', meshapconf, re.M|re.I)
			if matchObj:
				return matchObj.group(1)
		return None
	
	
	def Get_Country_Code(telnet_user,dev_filepath, NMS_Device_IP,interface="global"):
		dev_obj = dev_function()
		meshapconf = dev_obj.read_meshapp(telnet_user , dev_filepath, NMS_Device_IP)
		if (interface == "global"):
			matchObj = re.search( '\s*country_code\s+\=\s(.*)', meshapconf, re.M|re.I)
			if matchObj:
				return matchObj.group(1)
		return None
	
	def Get_Description(telnet_user,dev_filepath, NMS_Device_IP,interface="global"):
		dev_obj = dev_function()
		meshapconf = dev_obj.read_meshapp(telnet_user , dev_filepath, NMS_Device_IP)
		if (interface == "global"):
			matchObj = re.search('\s*description\s+\=\s(.*)', meshapconf, re.M|re.I)
			if matchObj:
				return matchObj.group(1)
		return None
	
	def Get_Heartbeat_Interval(telnet_user,dev_filepath, NMS_Device_IP,interface="global"):
		dev_obj = dev_function()
		meshapconf = dev_obj.read_meshapp(telnet_user , dev_filepath, NMS_Device_IP)
		if (interface == "global"):
			matchObj = re.search('\s*heartbeat_interval\s+\=\s(.*)', meshapconf, re.M|re.I)
			if matchObj:
				return matchObj.group(1)
			return None
	
			
	def essid_verification(self, user, dev_filepath,ipaddress, verify_param):   
	# 		dev_Logfilename = "Test2_ESSID_Device_output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
	# 		dev_filepath = ".\\Output\\"+dev_Logfilename
	 	 	tn = telnetlib.Telnet(ipaddress)
		 	readval = tn.read_until("login")
			#tn.read_until("login: ")
			tn.write(user+"\r\n")
			#tn.write("ls\n")
			#tn.write("ifconfig mip0\n")
			cmd_essid = "cat /etc/meshap.conf"
			tn.write(cmd_essid+"\n")		
			tn.write("exit\n")
			time.sleep(2)
			cmd_res = tn.read_all()
			print cmd_res
			logfile = open(dev_filepath, 'a')
			logfile.write(cmd_essid)
			logfile.close()
			#ping_result = tn.read_until("bytes from")
	 		
			dev_log = open(dev_filepath, 'w')
			dev_log.write(cmd_res)
			dev_log.write('\n')
			dev_log.close()
	 		return cmd_res
			matobj = re.search(verify_param+'\s*\=\s*([ -~]*)','meshapconf',re.IGNORECASE|re.MULTILINE)
			print matobj.group(1)
			if matobj:
				status = "Success"
				return status
				(matobj.group(1))
			else:
				status = "Failed"
				return status

	

	def get_ping_status(self,user, dev_logfile, ipaddress):
		tn = telnetlib.Telnet(ipaddress)
		#tn1 = tn.open(host, port=23, timeout=None)
		readval = tn.read_until("login:  ")
		#tn.read_until("login: ")
		tn.write(user+"\r\n")
		#tn.write("ls\n")
		#tn.write("ifconfig mip0\n")
		cmd = "ping"+ ipaddress
		tn.write(cmd+"\n")		
		tn.write("exit\n")
		ping_res = tn.read_all()
		#ping_result = tn.read_until("bytes from")
		log_file  = dev_logfile+str(time.strftime('%Y_%m_%d_%H_%M', current_time))
		dev_log = open(log_file, 'w')
		dev_log.write(ping_res)
		dev_log.write('\n')
		dev_log.close()
		
		
		
	#		 logfile = open("E:\\mesh_dynamics\\Automation_framework\\src\\Output\\log_console.txt", 'a')
	#		 logfile.write(vals)
	#		 logfile.close()
		return ping_res
	
	
	def get_Ifconfig(self, user,  dev_logfile, ipaddress):	
		tn = telnetlib.Telnet(ipaddress)
		
		tn.read_until("login: ")
		#log_file="get_ping_node.csv"#+str(time.strftime('%Y_%m_%d_%H_%M', current_time))
		tn.write(user+"\n")
		cmd1 = "ifconfig mip0"
		tn.write(cmd1+"\n")
		tn.write("exit\n")
		dev_ip_add = tn.read_all()
		
		#tn.write("exit\n")
		#print dev_ip_add
		tn.close()
		logfile = open(dev_logfile, 'a')
		logfile.write(dev_ip_add)
		logfile.close()
		return dev_ip_add
		
	def get_MAC_Address(self, host, user,  dev_logfile, ipaddress):
		
			
		tn = telnetlib.Telnet(ipaddress)
		#tn1 = tn.open(host, port=23, timeout=None)
		readval = tn.read_until("login:")
		print readval
		cmd_mac = "ifconfig eth0"
		##tn.read_until("login: ")
		#log_file="get_ping_node.csv"#+str(time.strftime('%Y_%m_%d_%H_%M', current_time))
		tn.write(user+"\n")
		tn.write(cmd_mac+"\n")
		tn.write("exit\n")
		time.sleep(2)
		dev_ifconfig = tn.read_all()
		
		#tn.write("exit\n")
		print dev_ifconfig
		tn.close()
		logfile = open(dev_logfile, 'a')
		logfile.write(ipaddress)
		logfile.close()
		return cmd_mac
	
	def execute_via_serial(self,port,cmd,baud = 115200):
		
		ser = serial.Serial(port , baud , timeout=10)
		# open the serial port
		if ser.is_open :
			print "\nSerial port is opened successfully"
		else:
			print "Fail!!Couldn't open serial port"
		
		print "Command Recieved: ",cmd,"\n"
		ser.write('\r\n\r\n')
		time.sleep(2)
		ser.reset_output_buffer()
		ser.write(cmd.encode('ascii')+'\r\n')
		time.sleep(2)
		out = ser.read(size=1024)
		ser.close()
		if out:
			return ("success " + out )
		return ("Failed") 	
			
	def config_node_via_serial(self,port,interface,infele,baud = 115200):
		
		ser = serial.Serial(port, baud, timeout=10)
		# open the serial port
		if ser.is_open :
			print "\nSerial port is opened successfully"
		else:
			print "Fail!!Couldn't open serial port"
		
		print "\n printing infele",infele,""
		time.sleep(2)	
		for k,v in infele.iteritems():
			v = str(v)
			out = ''
			if k.lower() == 'mesh_id':
				cmd = "alconfset mesh_id "+ v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				print " configured mesh_id",out,""
				#ser.reset_input_buffer()
			elif k.lower() == 'name':
				cmd = "alconfset name "+ v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				#ser.reset_input_buffer()
			elif k.lower() == 'essid':
				cmd = "alconfset essid " + interface + "  " + v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				print " configured essid",out,""
				#ser.reset_input_buffer()
			elif k.lower() == 'channel':
				cmd = "alconfset channel "+ interface + "  " + v
				print "\n interface channel is ",cmd,""
				time.sleep(20)
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				print " configured channel",out,""
				#ser.reset_input_buffer()	
			elif k.lower() == 'rts':
				cmd = "alconfset rts "+ interface + "  " + v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				ser.reset_input_buffer()
			elif k.lower() == 'frag':
				cmd = "alconfset frag "+ interface + "  " + v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				ser.reset_input_buffer()
			elif k.lower() == 'hidessid':
				cmd = "alconfset hidessid "+ interface + "  " + v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				ser.reset_input_buffer()
			elif k.lower() == 'subtype':
				cmd = "alconfset subtype "+ interface + "  " + v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				ser.reset_input_buffer()
			elif k.lower() == 'beacint':
				cmd = "alconfset beacint "+ interface + "  " + v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				ser.reset_input_buffer()
			elif k.lower() == 'dca':
				cmd = "alconfset dca "+ interface + "  " + v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				ser.reset_input_buffer()
			elif k.lower() == 'preamble':
				cmd = "alconfset preamble "+ interface + "  " + v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				ser.reset_input_buffer()
			elif k.lower() == 'acktime':
				cmd = "alconfset acktime "+ interface + "  " + v
				ser.write('\r\n\r\n')
				time.sleep(2)
				ser.reset_output_buffer()
				ser.write(cmd.encode('ascii')+'\r\n')
				time.sleep(2)
				out = ser.read(size=1024)
				ser.reset_input_buffer()
		
		cmd = "ifconfig mip0"
		ser.reset_output_buffer()
		#ser.reset_intput_buffer()
		ser.write(cmd.encode('ascii')+'\r\n')	
		time.sleep(5)
		out = ""
		out = ser.read(size=512)
		print " here it is!!!!",out,"n\n"
		time.sleep(60)
		matobj = re.search('mip0\s*Link\s*encap:Ethernet\s*HWaddr\s*(([0-9A-F]{2}[:-]){5}([0-9A-F]{2}))',out,re.I)
		mac_add = matobj.group(1)
		meshid = infele['mesh_id']
		cmd = "patcher -k " + meshid + " " + mac_add
		ser.reset_output_buffer()
		ser.write(cmd.encode('ascii')+'\r\n')
		time.sleep(3)
		cmd = "reboot"
		ser.reset_output_buffer()
		ser.write(cmd.encode('ascii')+'\r\n')
		time.sleep(20)
		ser.close()
		return 
	
	def get_from_config(self,fname,rowname,column_name):
		if(sys.platform == "linux2"):
			filename = "..//Config//" + fname + ".csv"
		else:
			filename = "..\\Config\\" + fname + ".csv"
		with open(filename, 'r') as f:
			file_read = csv.DictReader(f)
			flag = 0 
			res =''   
			for row in file_read:
				if (str(row['NAME'])) == (str(rowname)) :
					flag = 1
					res = row[column_name]
					break
			
			f.close()	
			if flag :
				return res	
			else :
				return "Sorry Node name not found"

	
	def Get_sorted_nodes(self,node_list) :
	    i = ''
	    mac_addr = ''
	    mac_list = {}
	    for i in node_list :
	        mac_addr = self.get_from_setup(i,"MAC")
	        mac_list[i] = mac_addr
	    mac_list = sorted(mac_list.items(), key=operator.itemgetter(1))
	    return mac_list
	
	
	
	def Get_node_mode(self,ipaddr):
		cmd = 'cat /proc/net/meshap/mesh/adhoc'
		dev_node_mode = ''
		dev_node_mode =self.execute_via_telnet(ipaddr,cmd)
		print "\n in dev_function",dev_node_mode
		time.sleep(2)
		if re.search(r"\b FFN \b",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			return ("FFN")
		elif re.search(r"\b FFR \b",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			return ("FFR")
		elif re.search(r"\b LFR \b",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			return ("LFR")
		elif re.search(r"\b LFN \b",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			return ("LFN")
		elif re.search(r"\b LFRS \b",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			return ("LFRS")
			
		return ("cannot be determined")
	
# 	def get_test_server_info(self,col_name):
# 		if(sys.platform == "linux2"):
# 			rowname = "linux"
# 			server_file = "..//Setup//Test_server_info.csv"
# 		elif(sys.platform == "win32") or (sys.platform == "cygwin") :	
# 			rowname = "windows"
# 			server_file = "..\\Setup\\Test_server_info.csv"
# 		with open(server_file, 'r') as f:
# 			file_read = csv.DictReader(f)
# 			flag = 0     
# 			for row in file_read:
# 				if row['PLATFORM'] == rowname :
# 					flag = 1
# 					res = row[col_name]
# 					break
# 			f.close()
#         	if flag :
#         		return res
#          	return None	
	
	def get_from_setup(self,rowname,column_name):
		if(sys.platform == "linux2"):
			setup_file = "..//Setup//Test1_Setup.csv"
		else:
			setup_file = "..\\Setup\\Test1_Setup.csv"
		with open(setup_file, 'r') as f:
			file_read = csv.DictReader(f)
			flag = 0     
			for row in file_read:
				if row['NAME'] == rowname :
					flag = 1
					res = row[column_name]
			f.close()
        	if flag :
        		return res
         	return None
			
	def check_positive_ping(self,pingstr):
		temppingstr=copy.copy(pingstr)
		temppingstr=temppingstr.replace('\n','')
		temppingstr=temppingstr.replace('.','')
		ReqtimeOut = re.match('(.*)(Request timed out){5,}(.*)',temppingstr,re.IGNORECASE|re.MULTILINE) 
		if ReqtimeOut:
			return "Fail" 
		DestUnreach = temppingstr.count("Destination host unreachable")
		if DestUnreach >=5 :
			return "Fail" 
		strlines = pingstr.splitlines()
		line = ''
		for line in strlines:
			if "Packets" in line:
				line = line.replace("Packets:","")
				line = line.replace(" ","")
				line = re.sub('\(.*\),$',"",line)
		return ("Success"+line)
	
	def execute_via_telnet(self,hostip, cmd,user= "root"):
		count = 0
		out = ''
		tn = telnetlib.Telnet(hostip,int(23),int(20))
		out = tn.read_until('OpenWrt')
		if "root" in out :
		    print "Login is not required.."
		else:
		    print "please login to continue"
		    tn.write(user + "\n")
		    time.sleep(1)
		out = ''
		if type(cmd) is str :
			tn.write(cmd + "\n")
			if re.search("ifconfig eth[0|1] down",cmd):
				while count < 2:
					tn.write(cmd + "\n")
					time.sleep(1)
					count = count + 1
			if re.search('ping',cmd,re.IGNORECASE):
					time.sleep(12)
			else :
					time.sleep(5)
		elif type(cmd) is list:
			for i in cmd :	
				print "\n telenet command executing is",i
				tn.write(i + "\n")
				if re.search("ifconfig eth[0|1] down",i):
					while count < 2:
						tn.write(i + "\n")
						time.sleep(2)
						count = count + 1
				if re.search('ping',i,re.IGNORECASE):
					time.sleep(13)
				else :
					time.sleep(2)
		while out.find('#') == -1:
			out = tn.read_very_eager()
		time.sleep(2)
		tn.close()
		return out
		
		
	def config_node_via_telnet(self,logFH,interface,hostip,infele,user= "root"):
		logobj = Log()
		out = ''
		tn = telnetlib.Telnet(hostip,int(23),int(20))
		out = tn.read_until('OpenWrt')
		if "root" in out :
		    print "Login is not required.."
		else:
		    print "please login to continue"
		    tn.write(user + "\n")
		    time.sleep(1)
 		out = ''
 		while out.find('#') == -1:
 			out = tn.read_very_eager()
 	
		logobj.AppendLog(logFH,"telnet connected successfully")
		logobj.AppendLog(logFH,out)
		time.sleep(2)
		tn.write("ifconfig mip0 \r \n")
		time.sleep(1)
		out = ''
		while out.find('#') == -1:
			out = tn.read_very_eager()
		print out				
		matobj = re.search('mip0\s*Link\s*encap:Ethernet\s*HWaddr\s*(([0-9A-F]{2}[:-]){5}([0-9A-F]{2}))',out,re.I)
		mac_add = matobj.group(1)
		meshid = infele['mesh_id']
	
 		for k,v in infele.iteritems():
 			v = str(v)
 			if k.lower() == 'mesh_id':
 				cmd = "alconfset mesh_id "+ v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
  				logobj.AppendLog(logFH,out)
 				time.sleep(2)
 			if k.lower() == 'pp_id':
 				if v == "" :
 					continue
 				cmd = "alconfset prefpar "+ v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
  				logobj.AppendLog(logFH,out)
 				time.sleep(2)
 			if k.lower() == 'name1223':#change name1223 to name later
 				cmd = "alconfset name "+ v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				time.sleep(2)
 				logobj.AppendLog(logFH,out)
 			if k.lower() == 'essid':
 				cmd = "alconfset essid "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				logobj.AppendLog(logFH,out)		
 			if k.lower() == 'channel':
 				cmd = "alconfset channel "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				logobj.AppendLog(logFH,out)
 			if k.lower() == 'rts':
 				cmd = "alconfset rts "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				logobj.AppendLog(logFH,out)
 			if k.lower() == 'frag':
				cmd = "alconfset frag "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				logobj.AppendLog(logFH,out)		
 				time.sleep(2)
 			if k.lower() == 'hidessid':
 				cmd = "alconfset hidessid "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				logobj.AppendLog(logFH,out)
 				time.sleep(2)
 			if k.lower() == 'subtype':
 				cmd = "alconfset subtype "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				logobj.AppendLog(logFH,out)
 				time.sleep(2)
 			if k.lower() == 'beacint':
 				cmd = "alconfset beacint "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				logobj.AppendLog(logFH,out)
 				time.sleep(2)
 			if k.lower() == 'dca':
 				cmd = "alconfset dca "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				logobj.AppendLog(logFH,out)
 			if k.lower() == 'preamble':
 				cmd = "alconfset preamble "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				logobj.AppendLog(logFH,out)
 			if k.lower() == 'acktime':
				cmd = "alconfset acktime "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
  				logobj.AppendLog(logFH,out)
 				time.sleep(2)	
 
 		cmd1 = 'patcher -k ' + meshid + ' ' + mac_add
 		print "#########",cmd1,"#############"
 		tn.write(cmd1 + "\r\n")
 		time.sleep(2)
 		out = ''
 		while out.find('#') == -1:
 		 	out = tn.read_very_eager()
 		logobj.AppendLog(logFH,out)
 		cmd = 'reboot\n'
 		tn.write(cmd + "\r\n")
 		time.sleep(2)
 		return
