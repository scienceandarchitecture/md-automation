from _collections import defaultdict
import csv
import json

import subprocess
import sys
import requests
import time


def POST_method(self, Url, header_items, data):
	header_items = {"content-type":"application/json"}
	# print header_items
	r = requests.post(url=Url, headers=header_items, data=data)
	status = r.status_code
	# jsondata = json.loads(r.text,'utf-8')
	respdata = r.text
	print "status_code for the api is : ", status
	if(status == 200):	
		return respdata
	if status == 204:
			return "No_Data"

	else:
		if(status == 401):
			return "Failed"


def Put_method(self, Url, header_items, data):
	header_items = {"content-type":"application/json"}
	# print header_items
	r = requests.put(url=Url, headers=header_items, data=data)
	status = r.status_code
	# jsondata = json.loads(r.text,'utf-8')
	time.sleep(13)
	respdata = r.text
	print "status_code for the api is : ", status
	
	if(status == 200):	
		return respdata
				
	if status == 204:
			return "No_Data"
			
	else:
		if(status == 401):
			
			return "Failed"
			


def DELETE_method(self, Url, header_items, data):
	header_items = {"content-type":"application/json"}
	# print header_items
	r = requests.delete(url=Url, headers=header_items, data=data)
	status = r.status_code
	# jsondata = json.loads(r.text,'utf-8')
	respdata = r.text
	print "status_code for the api is : ", status
	if(status == 200):	
		return respdata
	if status == 204:
			return "No_Data"

	else:
		if(status == 401):
			return "Failed"




def Get_Method(self, Url, header_items, data):
			r_get = requests.get(url=Url, headers = header_items)
			# print str(r_get)
			status = r_get.status_code
			# jsondata = json.loads(r.text,'utf-8')
			respdata = r_get.text
			#print respdata
			# print str(r.text)
			print "status_code for the api is : ", status
			
			if(status == 200):  # print("Testcase", tc, "Passed")
				respdata = json.loads(respdata,encoding='utf-8')	
				return respdata

			if(status == 204):
				execution_status = "Pass, No-content"
				# addOutput(desc, responsedata, status, tc, r_get, execution_status)	  
				return execution_status				
			else:
				if status >= "300" :
					print "Test case failed"
					return "Failed"

class CommonOperations():  

	def init(self, tc, Test_Desc, Test_Name, method, URL1, content_Type, input_data, exp_st_code, NMS_IP, MAC_Address, Channel_No, Interface_Name):
		tc = self.tc
		desc = self.API_Desc
		API_Name = self.API_Name
		Method = self.Method
		URL1 = self.URL1
		Input_Data = self.Input_Data   
# 		
	
	def commonOperations(self, tc, Method, Url, Content_Type, Arg1, Exp_st_code, Precondition, Supported_protocol, veri_data, Interface_Name, Network_Id, API_Server_IP):
		# Put operation
		
		if(Method == "POST"):
			header_items = {"content-type":"application/json"}
			req_val = POST_method(self, Url, header_items, Arg1)
			if req_val != "Failed" or req_val != "No_Data" :
				print "request processsed successfully"
				return req_val
			
			elif (req_val == 'No_Data'):
				print "Request Successfully Executed, But no data is returned "
				tc = int(tc) + 1
				return

			elif( req_val == "Failed"):
				print "Request failed"
				tc = int(tc) + 1
				return "Failed"

		if(Method == "PUT"):
			header_items = {"content-type":"application/json"}
			req_val = Put_method(self, Url, header_items, Arg1)
			
			
			if req_val != "Failed" or req_val != "No_Data" :
				print "request processsed successfully"
				print "response is", req_val
				return req_val
			
			elif (req_val == 'No_Data'):
				print "Request Successfully Executed, But no data is returned "
				tc = int(tc) + 1
				return

			elif( req_val == "Failed"):
				print "Request failed"
				tc = int(tc) + 1
				return "Failed"
		
		if(Method == "DELETE"):
			header_items = {"content-type":"application/json"}
			req_val = DELETE_method(self, Url, header_items, Arg1)
			
			
			if req_val != "Failed" or req_val != "No_Data" :
				print "request processsed successfully"
				print "response is", req_val
				return req_val
			
			elif (req_val == 'No_Data'):
				print "Request Successfully Executed, But no data is returned "
				tc = int(tc) + 1
				return

			elif( req_val == "Failed"):
				print "Request failed"
				tc = int(tc) + 1
				return "Failed"

		
		
		# Get operation
		if(Method == "GET"):
			header_items = {"content-type":"application/json"}
	   		resp = Get_Method(self, Url, header_items, Arg1)
	   		if resp != "Failed" or req_val != "No_Data" :
	   			print "Request Successfully executed"
	   			print resp
	   			return resp
	   			if veri_data != "" :
					tc = int(tc) + 1
					print resp[veri_data]					
					return resp
				else:
	   				tc = int(tc) + 1
	   				return
	   		else:
	   			return "Failed"
	   		
	   	if(Method == ''):
	   		print "No method provided, skipping this api request"	
	  		tc = int(tc) + 1
	  		return
# 	   
# def addOutput(desc, responsedata, status, no, r_get, execution_status):
# 	with open("./Output/output.txt", "a+") as myfile:
# 	
# 	 myfile.write("Tc Number is: " + no)
# 	 myfile.write('/t')	  
# 	 myfile.write("Tc Name is: " + desc)
# 	 myfile.write('/t')
# 	 myfile.write("Test-Output :  " + responsedata)
# 	 myfile.write('/t')	
# 	 myfile.write('/t')
# 	 myfile.write("The Http_Status code is " + str(status) + '\n')
# 	 if(status == 200 or status == 204):
# 		execution_status == "Passed, No contents"
# 		myfile.write("Testcase execution_status is  " + execution_status)
# 		myfile.write('/t')
#   
# 	 else:
# 		execution_status = "Failed"
# 		myfile.write("Testcase execution_status is  " + execution_status)
# 		myfile.write('/t')
# 		 
#   # myfile.write("The Test execution status is ", status, '\n')
# 		myfile.write("Test execution completed" + '\n')
# 		myfile.write('/t')
# 	
# 		myfile.write('\n')
# 		myfile.close()
# 		return