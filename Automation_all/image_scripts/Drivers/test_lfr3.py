import datetime
import time
import pandas as pd
import csv
import sys
#import dev_function_dev
from Common_lib  import *

import re
import json
#import Win_Sta
from dev_function import *
import os
import telnetlib
import copy
from subprocess import CalledProcessError, check_output




def make_etho_down(ipaddr):
    cmd = "ifconfig eth0 down"
    vl = execute_via_telnet (ipaddr, cmd)    
    time.sleep(10)
    print vl
        
        

def Get_node_mode(ipaddr):
        cmd = 'cat /proc/net/meshap/mesh/adhoc_mode'
        dev_node_mode = ''
        user= "root"        
        tn = telnetlib.Telnet(ipaddr)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
            #print "please login to continue"
            tn.write("root"+'\n')
            # print "login successfully"
            console_val = tn.read_until(':')
        
        
            if 'Password:' in console_val:
                tn.write("\r\n")
                print "Password entered..."
                #print log_val
        
        
        print "Login successfull..."  
        time.sleep(5)
        tn.write(cmd+'\n')
        time.sleep(20)
        data = ''
        while data.find('adhoc_mode') == -1:
            data = tn.read_until('root')
        print "from get node mode value is ",data
#         print valconsole
        
  
        if re.search("FFN",data,re.IGNORECASE|re.MULTILINE) :
            #print re.search("FFN",data,re.IGNORECASE|re.MULTILINE)
            return ("FFN")
        elif re.search("FFR",data,re.IGNORECASE|re.MULTILINE) :
            #print re.search("FFR",data,re.IGNORECASE|re.MULTILINE) 
            return ("FFR")
        elif re.search("LFR",data,re.IGNORECASE|re.MULTILINE) :
             #print re.search("LFR",data,re.IGNORECASE|re.MULTILINE) 
             return ("LFR")
        elif re.search("LFN",data,re.IGNORECASE|re.MULTILINE) :
             #print re.search("LFN",data,re.IGNORECASE|re.MULTILINE) 
             return ("LFN")
#         elif re.search("LFRS",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
#             return ("LFRS") 
        else:
            return "Not found" 












def get_from_setup(num_node, column_name):
    count =0
    
#     column_name = "MAC"
    maclist = []
    while count < num_node:
    
        data = pd.read_csv("..//Data//Test1_Setup.csv")
        NAME = data.NAME[count]
        MAC = data.MAC[count]
        maclist.append(MAC)
        count = count+1
        
        

    
    return maclist
    
#     with open("..//Data//Test1_Setup.csv", 'r') as f:
#         file_read = csv.DictReader(f)
#         flag = 0     
#         for row in file_read:
#             #print row
#             if row['NAME'] == rowname :
#                 flag = 1
#                 res = row[column_name]
#                 return res


def checktable (ipaddr, cmd):
        user= "root"
        count = 0
        tn = telnetlib.Telnet(ipaddr)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
            #print "please login to continue"
            tn.write("root"+'\n')
            # print "login successfully"
            console_val = tn.read_until(':')


            if 'Password:' in console_val:
                tn.write('\n')
                print "Passwprd entered"
                #print log_val
        
        
        print "Login successfull..."  
        
        print cmd
        #time.sleep(5)
        tn.write(cmd+'\n')
        data = ''
        while data.find('STATION') == -1:
            data = tn.read_until(':~#')
        print "TABLE DATA IS ",data
#         print valconsole
        return data



def execute_via_telnet (ipaddr, cmd):
        user= "root"
        count = 0
        tn = telnetlib.Telnet(ipaddr)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
            #print "please login to continue"
            tn.write("root"+'\n')
            # print "login successfully"
            console_val = tn.read_until(':')


            if 'Password:' in console_val:
                tn.write('\n')
                print " Password entered..."
        
        
        print "Login successfull..."  
        
        print cmd
        #time.sleep(5)
        tn.write(cmd)
        time.sleep(2)
        tn.write(cmd)
        time.sleep(2)
        tn.write(cmd+'\n')
        time.sleep(2)
        tn.write(cmd+'\n')
        tn.write(cmd+'\n')
        time.sleep(2)
        node_mode = Get_node_mode(ipaddr)
        #print "From execute via telnet, node mode is show as"+str(node_mode)+"for the ip "+str(ipaddr)
        return "Pass"





def check_node_status(ipadrr):
        toolbar_width = 41
        flag = cptr = 0 #cptr is cursor pointer
        cmd = "ping -c 5 " + ipadrr
        sys.stdout.write("[%s]" % (" " * toolbar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (toolbar_width+1))
        while cptr < (toolbar_width - 1) :
            pingstr = None
            try:
                pingstr = check_output(cmd , shell=True)
                returncode = 0
            except CalledProcessError as errorno:
                output = errorno.output
                returncode = errorno.returncode
                if returncode :
                    temppingstr = "Request timed out" * 3
            if pingstr is not None : 
                temppingstr=copy.copy(pingstr)
            ReqtimeOut = re.search('Request timed out',temppingstr,re.I)
            Destunreach = re.search('Destination host unreachable',temppingstr,re.I)
            if ReqtimeOut:
                cptr = cptr + 4
                sys.stdout.write("====")
                sys.stdout.flush()
                continue
            elif Destunreach:
                cptr = cptr + 4
                sys.stdout.write("====")
                sys.stdout.flush()
                continue
            else :
                flag = 1
                sys.stdout.write(("=" * (toolbar_width - cptr)))
                sys.stdout.flush()
                sys.stdout.write("\n")
                return "Success"
                #break
        if flag==1:
            return "Success"
        return "Timeout"

def find_Wlan2mac(ip, iface):
    #dev_obj = dev_function()
    
    cmd = 'ifconfig '+ iface +"\n" 
    print cmd  
    tn = telnetlib.Telnet(ip)
    tn.write(cmd)
    val = tn.read_until('Mask', 20) 
    print val
    for line in val.splitlines():
        mat=re.search(r'Link encap\:',line)
        if mat:
            mat1 = re.search(r'(HWaddr\s)(([0-9-A-Z]{1,2}\:)(\w{2}\:){4}([0-9-A-Z]{1,2}))',line)
            print mat1.group(2)


def get_config_values(count):
    
    df = pd.read_csv("..//Config//Three_Node_transition_Config.csv")    
    node_MeshId= df.MESH_ID[count]
    node_essid = df.ESSID[count]
    node_channel = df.CHANNEL[count]
    node_name = df.NAME[count]
    node_interface = df.INTERFACE[count]
    return [node_MeshId, node_essid, node_channel, node_name, node_interface]

def Setup_Parser_method(count):
    
    data = pd.read_csv("..//Data//Test1_Setup.csv")
    NAME = data.NAME[count]
    MAC = data.MAC[count]
    Dev_IP =  data.IP[count]
    TYPE = data.TYPE[count]
    MODEL = data.MODEL[count]
    NODE_USER = data.USER_NAME[count]
    Node_eth1_ip = data.ETH1_IP[count]
    setup_values = [NAME, MAC, Dev_IP, TYPE, MODEL, NODE_USER, Node_eth1_ip]
    return setup_values


class N_NodeTransition():
    def init(self):
        self.object = object


    try:
	    tc = 0
	    #logFH = logobj.CreateLog(scriptname)
	    configfname = "Three_Node_transition_Config"
	    #logobj.AppendLog(("\n Executing Test script "+scriptname))
	    
	    
	    
	    print "Testcase begins now"
	    sttime = datetime.datetime.now()
	    print "start time of testcase", sttime
	    data = pd.read_csv("..//Data//Test1_Setup.csv")
	    num_node = len(data)
	   
	    node = []
	    node_config=[]
	    print num_node
	    
	    row_num=0
	    node_setup = []
	    node_config=[]
	    row_num=0
	 
	    
	#     while row_num < num_node:
	#         #Config_Parser_method(row_num)
	#         val = Setup_Parser_method(row_num)
	#         NAME = "Name"+str(row_num) 
	#         NAME_val = val[0]
	#         Dev_MAC_Address = "Dev_MAC_Address"+str(row_num)
	#         Dev_MAC_Address_val = val[1]
	#         NMS_Device_IP = "NMS_Device_IP"+str(row_num)
	#         NMS_Device_IP_val = val[2]
	#         Type = "Type"+str(row_num)
	#         Type_val = val[3]
	#         Model = "Model"+str(row_num)
	#         Model_val = val[4]
	#         Node_User = "Node_User"
	#         Node_User_val = val[5]
	#         Node_eth1_ip = "Node_Eth1_ip"+str(row_num)
	#         Node_eth1_IP_val = val[6]
	#         nodeval="nodeval"
	#         nodeval = nodeval+str(row_num)
	#         nodeval = {NAME:NAME_val, Dev_MAC_Address:Dev_MAC_Address_val, NMS_Device_IP:NMS_Device_IP_val,Type:Type_val, Model:Model_val,Node_User:Node_User_val, Node_eth1_ip : Node_eth1_IP_val}
	#         ''' For all the node the value will be set in this loop '''
	#         
	#         node_setup.append(nodeval)
	#         print nodeval,"\n"
	#         
	#         conf_val = get_config_values(row_num)
	#         node_meshId = "Node_MeshId"+str(row_num)
	#         node_meshId_val = conf_val[0]
	#         node_essid = "node_essid"+str(row_num)
	#         node_essid_val = conf_val[1]
	#         node_channel = "node_channel"+str(row_num)
	#         node_channel_val = conf_val[2]
	#         node_interface = "node_interface"+str(row_num)
	#         node_interface_val = conf_val[4]
	#         
	#         node_info = {}
	#         node_info['mesh_id'] = node_meshId
	#         node_info['essid'] = node_essid
	#         
	#         #config = devobj.config_node_via_telnet(node_interface,Dev_MAC_Address, NMS_Device_IP, node_info,Node_User)
	#         node_info = "node_info"+str(row_num)
	#         node_info = {node_meshId:node_meshId_val, node_essid:node_essid_val, node_channel:node_channel_val, node_interface:node_interface_val}
	#         ''' all the config info of node will be set in this loop '''
	#         node_config.append(node_info)
	#         print node_info,"\n"
	#         row_num = row_num+1
	#     

	    ncount = 0
	    ''' Step1 starts here '''
	    while ncount < num_node:
		val = Setup_Parser_method(ncount)
		NAME_val = val[0]        
		Dev_MAC_Address_val = val[1]
		nmsip = val[2]
		Node_eth1_IP_val = val[6]
	    
		if ncount == 0:
		    print "First nodes etho should be up"
		    print "Checking the status of the node"
		    print Node_eth1_IP_val
		    node_mode = Get_node_mode(Node_eth1_IP_val)
		    print "Value of node mode in while looop is : ",node_mode
		    
		    print "For IP : ",Node_eth1_IP_val
		    print "Node mode is set to ",node_mode
		    if node_mode == str("FFR").strip():
		        print "node_mode set as FFR for the IP ", Node_eth1_IP_val
		    else:
		        print "Node mode is not set properly,making eth0 up ", node_mode
		        cmd= "ifconfig eth0 up"
		        print "For IP :",Node_eth1_IP_val
		        enableInterface = execute_via_telnet(Node_eth1_IP_val, cmd)
		        FFR_Node = Node_eth1_IP_val 
		        time.sleep(80)
		        print "Checking the node mode after enabling eth0..."        
		        node_mode = Get_node_mode(Node_eth1_IP_val)
		        print "after enabling from else part node mode is : ",node_mode
		        if node_mode == "FFR":
		            print "For IP :",Node_eth1_IP_val
		            print "Node set as expected..."
		        else:
		            print "For IP :",Node_eth1_IP_val
		            print "Node not set correctly.."
		else:            
		    print "Checking Wheter the nodes are set to FFN....."
		    make_etho_down(Node_eth1_IP_val)
		ncount=ncount+1
	    print "Eth0 of all the nodes is down now..."        


	    """ Checking the status of all node set to FFN or not """
	    ncount2 = 0
	    while ncount2 < num_node:
		val = Setup_Parser_method(ncount2)
		NAME_val = val[0]        
		Dev_MAC_Address_val = val[1]
		nmsip = val[2]
		Node_eth1_ip = val[6]
		print "Node ip adddress is :",Node_eth1_ip
		if ncount2 > 0:
		    print "Checking all the other nodes are set to FFN or not ....."
		    node_mode = Get_node_mode(Node_eth1_ip)
		    print "Node mode is ",node_mode
		    if node_mode == "FFN":
		        print "Node mode set to "+node_mode+" for IP"+str(Node_eth1_ip)
		
		    else:
		        print "Node mode not set correctly.."
		        time.sleep(40)
		        print "Again checking incase transition taking long time..."
		        node_mode = Get_node_mode(Node_eth1_ip)
		        print "Node mode is ",node_mode
		        if node_mode == "FFN":
		            print "Node mode set to "+node_mode+" for IP"+str(Node_eth1_ip)
		        else:
		            print "Node not set to correct mode..."
		            print "Node not behving properly, Check if transition taking place or not ...."
		else:
		     print "Ignoring the first node since it should be FFR ..."

		ncount2 = ncount2+1


	    print "step1 ends here"
	    print "=========**************==============***********============="
	    print '\n\n'
	    print "All the nodes are set to FFN and first node is set to FFR"
	    print "Checking LFR and LFN Case..."
		 


	    ncount4 = 0

	    while ncount4 < num_node:
		val = Setup_Parser_method(ncount4)
		NAME_val = val[0]        
		Dev_MAC_Address_val = val[1]
		nmsip = val[2]
		Node_eth1_ip = val[6]
	    
		if ncount4 == 0:
		    print "Checking the Mode of the first node"
		    node_mode = Get_node_mode(Node_eth1_ip)
		    print node_mode
		    if node_mode == "FFR":
		        print "node_mode set as FFR for the IP ", Node_eth1_ip
		        print "Making the node LFR by disabling the ETH0.."
		        cmd= "ifconfig eth0 down \n"
		        execstatus = execute_via_telnet(Node_eth1_ip, cmd)
		        FFR_Node = Node_eth1_ip 
		        time.sleep(30)
		        print "Checking the node mode after disabling eth0..."        
		        node_mode = Get_node_mode(Node_eth1_ip)
		        print "after disabling the eth0, node mode is : ",node_mode
		        if node_mode == "LFR":
		            print "Node set as expected..."
		            print "Now checking the LFN Nodes using Table command..."
		            cmd = "cat /proc/net/meshap/mesh/table"
		            opstr = checktable(Node_eth1_IP_val, cmd)
		            print opstr
		            for line in opstr.splitlines():
		                print line
		                mat = re.search(r'(([0-9-A-Z]{1,2}\:)(\w{2}\:){4}([0-9-A-Z]{1,2}))',line, re.I|re.M)
		                #print "mat", mat
		         
		                if mat != None:
		                    mat = mat.group()
		                    mat = str(mat).upper()
		                    print mat
		                    print " Node with mac address"+" "+mat+" is part of mesh network"
		#                 
		                
		        
		     
		  


		        else:
		            print "Node not set correctly.."
		            time.sleep(30)
		            print "In Else LFR case, Rechecking if the node is set to LFR in case Transition time is more then ususal time...."
		            node_mode = Get_node_mode(Node_eth1_ip)
		            print "After rechecking the node mode, the mode is showing as : ",node_mode
		            if node_mode == "LFR":
		                print "Node set as expected..."
		                print "Now checking the LFN Nodes using Table command..."
		                cmd = "cat /proc/net/meshap/mesh/table"
		                opstr = checktable(Node_eth1_ip, cmd)
		                print  "From chktale value returned is : ", opstr
		                for line in opstr.splitlines():
		                    print line
		                    mat = re.search(r'(([0-9-A-Z]{1,2}\:)(\w{2}\:){4}([0-9-A-Z]{1,2}))',line, re.I|re.M)
		                    print "Mat is ",mat.group()
		                    mat = mat.group()
		                    mat = str(mat).upper()
		                    print mat
		                    if mat != None:
		                        print " Node with mac address"+" "+mat+" is part of mesh network"

		                    

		else:
		    print "Checking the mode of the other nodes"    
		    node_mode = Get_node_mode(Node_eth1_ip)
		    print node_mode
		    if node_mode == "LFN":
		        print "Node Set to LFN..."
		        print "Node set as expected..."
		    else:
		        print " for else part Node mode is set to : ", node_mode
		    
		                               
		ncount4 = ncount4+1
		   
	    print "LFR LFN Validation completed...."
	    print "Now verifying that nodes mode sets to FFR after rebooting..."
	    node_count3 =0
	    while node_count3 < num_node:
		
		try:
		
		    #val = Setup_Parser_method(node_count3)
		    data = pd.read_csv("..//Data//Test1_Setup.csv")            
		    Node_eth1_IP_val = data.ETH1_IP[node_count3]
		    cmd= "reboot \n"
		    execute_via_telnet(Node_eth1_IP_val, cmd)
		    print "Node reboted: ",Node_eth1_IP_val
		    
		    node_count3 =node_count3+1
		    
		    time.sleep(45)
		except:
		    print "Node rebooting..."
		    node_count3= node_count3+1
		        
		    
	    print "All node rebooted, now cheking if they are set to ffr"    
	    node_count3 =0
	    while node_count3 < num_node:
		#val = Setup_Parser_method(node_count3)
		try:
		    data = pd.read_csv("..//Data//Test1_Setup.csv")
		     
		    Node_eth1_IP_val = data.ETH1_IP[node_count3]
		     
		     
		    #Node_eth1_IP_val_list=[Node_eth1_IP_val]        
		    time.sleep(50)
		    print "Status of rebooted node is ",check_node_status(Node_eth1_IP_val)
		    node_mode = Get_node_mode(Node_eth1_IP_val)
		    print node_mode
		    if node_mode == "FFR":
		        print "node_mode set as FFR for the IP ", Node_eth1_IP_val
		    else:
		        print "Node mode is ", node_mode
		    if check_node_status=="Success":
		        time.sleep(30)
		        node_mode = Get_node_mode(Node_eth1_IP_val)
		        print node_mode
		        if node_mode == "FFR":
		            print "node_mode set as FFR for the IP ", Node_eth1_IP_val
		        else:
		            print "Node mode is ", node_mode
		except:
		    print "Node rebooted successfully.. it is pingable but not yet reachable..."
		     
		node_count3 = node_count3+1
		 
	    endtime = datetime.datetime.now()
	    timetaken = endtime-sttime
	    print "End time of testcase", endtime
	    print "Total Execution Time is : ",timetaken
	    print "Step 5 Ends Now"
	    print "==========**************=============**************=============*********************"
	    print '\n' 
	    
	     
	    print "Transition cycle is completed.."
	    print "Test completed now.."
	    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

    except Exception as e:
	print "Execution interrupted... Please check if node is up.."
	print e





