import os
import sys
import re
from subprocess import check_output
import time
import telnetlib, copy
import time
import csv
from dev_function_dev import *
from Log import *
from Win_Sta import *
from Setup_Verify import *
import pandas as pd


def Get_highest_mac(mac1,mac2):
	tempmac1 = mac1
	tempmac2 = mac2
	mac1 = mac1.replace("-","")
	mac1 = mac1.replace(":","")
	mac1 = mac1.lower()
	mac2 = mac2.replace("-","")
	mac2 = mac2.replace(":","")
	mac2 = mac2.lower()
	if mac1 > mac2 :
		return tempmac1
	elif mac2 > mac1 :
		return tempmac2
	else:
		return "same"
	
def Get_nodes():
	df = pd.read_csv("..\\Config\\Two_node_Transition_Config.csv")
	my_list = df["NAME"].tolist()
	return my_list

def Config_Parser_method(logFH):
	devobj = dev_function()   
	df = pd.read_csv("..\\Config\\Two_node_Transition_Config.csv")
	with open("..\\Config\\Two_node_Transition_Config.csv", 'r') as f:
		file_read = csv.DictReader(f)	
		data = []
		for row in file_read:
						data.append(row)
		no_row = len(data)   
		count = 0
		while count < no_row :
			node_info = {}
			node_info['mesh_id'] = df.MESH_ID[count]
			node_info['essid'] = df.ESSID[count]
			node_info['channel'] = df.CHANNEL[count]
			node_info['name'] = df.NAME[count]
			node_interface = df.INTERFACE[count]
			node_ip = devobj.get_from_setup(node_info['name'],"IP")
			node_user = devobj.get_from_setup(node_info['name'],"USER_NAME")
			print"\n Please wait!! configuring node" + (str(count+1)) +" this may take time\n\n"
			config = devobj.config_node_via_telnet(logFH,node_interface,node_ip,node_info,node_user)
			if count != 0:
				time.sleep(2)
			count = count + 1
		return 

class Two_node_Transition(object):
		
	def init(self):
		self.object = object
	
	
	configfname = "Two_node_Transition_Config"
	logobj = Log()
	Nodes = Get_nodes()
	devobj = dev_function()
	setup = Setup_Verify()
	winstaob = Win_Sta()
	node_status = ''
	node1ip = devobj.get_from_setup(Nodes[0],"IP")
	node2ip = devobj.get_from_setup(Nodes[1],"IP")
	node1_mac = devobj.get_from_setup(Nodes[0], "MAC")
	node2_mac = devobj.get_from_setup(Nodes[1], "MAC")  
	user1 = devobj.get_from_setup(Nodes[0], "USER_NAME")
	user2 = devobj.get_from_setup(Nodes[1], "USER_NAME")
	node1ssid = devobj.get_from_config(configfname,Nodes[0], "ESSID")
	node1auth = devobj.get_from_config(configfname,Nodes[0], "AUTHENTICATION")
	node2ssid = devobj.get_from_config(configfname,Nodes[1], "ESSID")
	node2auth = devobj.get_from_config(configfname,Nodes[1], "AUTHENTICATION")
	scriptname = os.path.basename(__file__)
	logFH = logobj.CreateLog(scriptname)
	logobj.AppendLog(logFH,("\nExecuting Test script "+scriptname))
	logobj.AppendLog(logFH,"\nVerifying if Setup is ready... ")
		
	for i in Nodes :
		ipaddr = devobj.get_from_setup(i,"IP")
		check_setup = setup.setup_check(ipaddr)   
		if check_setup == "Ping Successfull" :
			logobj.AppendLog(logFH,(" Node "+ i +" is up"))										
		else:
			logobj.AppendLog(logFH,(" Node "+ i +" is Down"))
			exit
												
	logobj.AppendLog(logFH,("\n Configuring the Nodes"))
	#Config_Parser_method(logFH)
	print "\n Trying to connect to Nodes..!please wait\n"
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 failed to\
				 bootup within expected time")
		exit()
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 failed to \
				bootup within expected time")
		exit()					
	logobj.AppendLog(logFH,("\nVerifying nodes mode!!\n"))
	cmd = 'cat /proc/net/meshap/mesh/adhoc_mode'
	dev_node_mode = ''
	for i in Nodes :
		user = devobj.get_from_setup(i, "USER_NAME")
		ipaddr = devobj.get_from_setup(i,"IP")
		logobj.AppendLog(logFH,("Trying to telnet to" + ipaddr)) 
		dev_node_mode = devobj.execute_via_telnet(ipaddr,cmd)
		#time.sleep(2)
		if re.search("FFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("node" +(i)+" is FFR\n"))			
		else :
			logobj.AppendLog(logFH,("node" +(i)+" is not FFR\n"))
			exit	
							
	##############################################################
	''' Executing Step2 '''
	logobj.AppendLog(logFH,("Making eth0 of Node2 down \n"))
	cmd = 'ifconfig eth0 down'  
	dev_node_mode = devobj.execute_via_telnet(node2ip,cmd)
	time.sleep(2)
	#node1_mode = devobj.Get_node_mode(node1ip)
	#if node1_mode != "FFR" :
	#	print "\nStep2 failed,Node1 is not FFR!!"
	#	exit()
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=disabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(2)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(2)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(2)
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable")
		exit()  
	node2_mode = devobj.Get_node_mode(node2ip)
	if node2_mode != "FFN" :
		print "\nStep2 failed,Node2 is not FFN!!"
		exit()
	logobj.AppendLog(logFH,("\nVerifying the Mesh network formation!!\n"))
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable")
		exit()
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" +\
							 "\n Node1 is root and Node2 is relay"))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit()
	cmd = "ping -c 10 " + node2ip
	out = devobj.execute_via_telnet(node1ip, cmd)   
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"
	else:
		print "ping from node 2 to node 1 Fail!!"
		exit()
	logobj.AppendLog(logFH,("\n Step2 is verified successfully!"))
                     
	''' Executing Step3 '''
	''' Connecting to wlan2 of node 1 and downing eth0 and eth1 interface'''
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable")
		exit()
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 and eth1 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable")
		exit()
	cmd = "ifconfig eth1 down"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	#time.sleep(5)
	''' Connecting to wlan2 of node 2 and downing eth0 and eth1 interface'''
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable")
		exit()
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 and eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable")
		exit()
	cmd = "ifconfig eth1 down"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	logobj.AppendLog(logFH,"Please wait!!Mesh formation is in progress!!\n")
	time.sleep(10)
	highestmac = Get_highest_mac(node1_mac,node2_mac)
	if highestmac == "same" :
		print "\nError in setup file!!"
		exit()
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = ''
	if(highestmac == node1_mac):
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout" :
			print "\nSorry the Node1 is unreachable!!"
			exit()  
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFR" :
			print "\nNode1 is not LFR,despite being highestmac!!"
			exit()
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout" :
			print "\nSorry the Node2 is unreachable!!"
			exit()
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFN" :
			print "\nNode2 is not LFN!!"
			exit()
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout" :
			print "\nSorry the Node1 is unreachable!!"
			exit()			  
		dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
		if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" +\
								 "\n Node1 is LFR and Node2 is LFN"))
			logobj.AppendLog(logFH,("\n Step3 is verified successfully!"))
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()
	else :
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(4)
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout" :
			print "\nSorry the Node2 is unreachable!!"
			exit()
		node2_mode = devobj.Get_node_mode(node2ip)		  
		if node2_mode != "LFR" :
			print "\nNode2 is not LFR,despite being highestmac!!"
			exit()  
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(4)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout" :
			print "\nSorry the Node1 is unreachable!!"
			exit()
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFN" :
			print "\nNode1 is not LFN!!"
			exit() 
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout" :
			print "\nSorry the Node2 is unreachable!!"
			exit()	 
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		if re.search(node1_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" +\
								 "\n Node2 is LFR and Node1 is LFN"))
			
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()	
	
	cmd = "ping -c 10 " + node2ip
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)   
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE) :
		print "ping from node 1 to node 2 Success!!"
	else :
		print "ping from node 1 to node 2 Fail!!"
		exit()
	logobj.AppendLog(logFH,("\n Step3 is verified successfully!"))

	#######################################################################
	''' Executing Step4 '''
	''' Connecting to wlan2 of node 1 and enabling eth0 and eth1 interface'''
	logobj.AppendLog(logFH,("\n Executing Step4..!"))
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable")
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 and eth1 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	''' Connecting to wlan2 of node 2 and enabling eth0 and eth1 interface'''
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable")
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 and eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=enabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(5)
	print "\n Trying to connect to Nodes..!please wait\n"
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 failed to \
				bootup within expected time")
		exit()
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 failed to \
				bootup within expected time")
		exit()
							
	logobj.AppendLog(logFH,("\nVerifying nodes mode!!\n"))
	node2_mode = ''
	node2_mode = devobj.Get_node_mode(node2ip)
	if node2_mode != "FFR" :
		print "\nStep4 failed,Node2 is not FFR!!"
		exit()
	node1_mode = ''
	node1_mode = devobj.Get_node_mode(node1ip)
	if node1_mode != "FFR" :
		print "\nStep4 failed,Node1 is not FFR!!"
		exit()
	logobj.AppendLog(logFH,("\n Step4 is verified successfully!"))
	########################################################################
	''' Executing Step5 '''
	logobj.AppendLog(logFH,("Making eth0 of Node2 down \n"))
	cmd = 'ifconfig eth0 down'  
	dev_node_mode = devobj.execute_via_telnet(node2ip,cmd)
	time.sleep(2)
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=disabled"
	winstaob.Exec_Win_Cmd(cmd)
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable")
		exit()
	cmd = 'ifconfig eth1 down'  
	dev_node_mode = devobj.execute_via_telnet(node2ip,cmd)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2) 
	node2_mode = devobj.Get_node_mode(node2ip)
	if node2_mode != "FFN" :
		print "\nStep5 failed,Node2 is not FFN!!"
		exit()
	logobj.AppendLog(logFH,("\nVerifying the Mesh network formation!!\n"))
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable")
		exit()
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" +\
							 "\n Node1 is root and Node2 is relay"))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit()
	cmd = "ping -c 10 " + node2ip
	out = devobj.execute_via_telnet(node1ip, cmd)   
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"
	else:
		print "ping from node 2 to node 1 Fail!!"
		exit()
	logobj.AppendLog(logFH,("\n Step5 is verified successfully!"))
	#########################################################################
	''' Executing Step6 '''
	logobj.AppendLog(logFH,("\n Executing Step6..!"))
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable")
		exit()
	cmd = 'ifconfig eth0 up'  
	dev_node_mode = devobj.execute_via_telnet(node2ip,cmd)
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=enabled"
	winstaob.Exec_Win_Cmd(cmd)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable")
		exit()
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 of node1"
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable")
		exit()
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 of node2"
	out = ''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)	
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=disabled"
	winstaob.Exec_Win_Cmd(cmd)	
	logobj.AppendLog(logFH,"Please wait!!Mesh formation is in progress!!\n")
	time.sleep(2)
	highestmac = Get_highest_mac(node1_mac,node2_mac)
	if highestmac == "same" :
		print "\nError in setup file!!"
		exit()
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = ''
	if(highestmac == node1_mac):
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout" :
			print "\nSorry the Node1 is unreachable!!"
			exit()  
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFRS" :
			print "\nNode1 is not LFRS,despite being highestmac!!"
			exit()
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout" :
			print "\nSorry the Node2 is unreachable!!"
			exit()
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFN" :
			print "\nNode2 is not LFN!!"
			exit()
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout" :
			print "\nSorry the Node1 is unreachable!!"
			exit()			  
		dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
		if re.search(node2_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" +\
								 "\n Node1 is LFRS and Node2 is LFN"))
			logobj.AppendLog(logFH,("\n Step6 is verified successfully!"))
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()
	else :
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout" :
			print "\nSorry the Node2 is unreachable!!"
			exit()
		node2_mode = devobj.Get_node_mode(node2ip)		  
		if node2_mode != "LFRS" :
			print "\nNode2 is not LFR,despite being highestmac!!"
			exit()  
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout" :
			print "\nSorry the Node1 is unreachable!!"
			exit()
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFN" :
			print "\nNode1 is not LFN!!"
			exit() 
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(5)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout" :
			print "\nSorry the Node2 is unreachable!!"
			exit()	 
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		if re.search(node1_mac,dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" +\
								 "\n Node2 is LFRS and Node1 is LFN"))
			
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()	
	
	cmd = "ping -c 10 " + node2ip
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)   
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE) :
		print "ping from node 1 to node 2 Success!!"
	else :
		print "ping from node 1 to node 2 Fail!!"
		exit()
	logobj.AppendLog(logFH,("\n Step6 is verified successfully!"))
	#########################################################################
	''' Executing Step7 '''
	logobj.AppendLog(logFH,("\n Executing Step7..!"))
	if node1_mode == "LFRS":
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout" :
			print "\nSorry the Node1 is unreachable!!"
			exit()
		cmd = "ifconfig eth1 down"
		print "\n\n disabling eth1 of node1"
		out =''
		out = devobj.execute_via_telnet(node1ip, cmd)
		logobj.AppendLog(logFH,out)	
		logobj.AppendLog(logFH,"\nPlease wait Mesh formation is in progress!!")
		time.sleep(5)
		winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout" :
			print "\nSorry the Node1 is unreachable!!"
			exit()
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFN" :
			print "\nStep7 Failed!!Node1 is not LFN,after disconnection eth1"
			exit()
	 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	 	time.sleep(5)
	 	winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout" :
			print "\nSorry the Node2 is unreachable!!"
			exit()
	 	node2_mode = ''
	 	node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFRS" :
			print "\nStep7 Failed!!Node2 is not LFRS \
					after disconnecting eth1 of Node1"
			exit()
		dev_node_mode = ''	
		cmd = "cat /proc/net/meshap/mesh/table"
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		if re.search(node1_mac,dev_node_mode,re.IGNORECASE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" +\
								 "\n Node2 is LFRS and Node1 is LFN"))
			logobj.AppendLog(logFH,("\n Step7 is verified successfully!"))
	else:
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(5)
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout" :
			print "\nSorry the Node2 is unreachable!!"
			exit()
		cmd = "ifconfig eth1 down"
		print "\n\n disabling eth1 of node2"
		out =''
		out = devobj.execute_via_telnet(node2ip, cmd)
		logobj.AppendLog(logFH,out)	
		logobj.AppendLog(logFH,"\nPlease wait Mesh formation is in progress!!")
		winstaob.WinSTA_Connect(node2ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node2ip)
		if node_status == "Timeout" :
			print "\nSorry the Node2 is unreachable!!"
			exit()
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFN" :
			print "\nStep7 Failed!!Node2 is not LFN,after disconnection eth1"
			exit()
	 	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	 	time.sleep(5)
	 	winstaob.WinSTA_Connect(node1ssid)
		time.sleep(2)
		node_status = ''
		node_status = devobj.check_node_status(node1ip)
		if node_status == "Timeout" :
			print "\nSorry the Node1 is unreachable!!"
			exit()
	 	node1_mode = ''
	 	node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFRS" :
			print "\nStep7 Failed!!Node1 is not LFRS \
					after disconnecting eth1 of Node2"
			exit()
		dev_node_mode = ''	
		cmd = "cat /proc/net/meshap/mesh/table"
		dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
		if re.search(node2_mac,dev_node_mode,re.IGNORECASE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" +\
								 "\n Node1 is LFRS and Node2 is LFN"))
			logobj.AppendLog(logFH,("\n Step7 is verified successfully!"))
	
	cmd = "ping -c 10 " + node2ip
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"

	else:
		print "Step7 Failed,ping from node 2 to node 1 Fail!!"
		exit()	
	
	###############################################################
	'''Executing Step8'''
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node1ssid)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step8 failed!!cannot reach " + node1ip)
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	##enabling eth0 and eth1 interface of Node2
 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step8 failed!!cannot reach " + node2ip)
		exit()
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 and eth1 of node2"
	print "\n\n node2 is rebooting"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(2)
 	print "\npinging node1!!"		
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step8 failed!!Unable to connect to node1\n")
		exit()
	cmd = "ifconfig eth0 down"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	logobj.AppendLog(logFH,"\nPlease wait Mesh formation is in progress!!")
	#cmd = "netsh interface set interface name=\"Ethernet 6\" admin=disabled"
 	#winstaob.Exec_Win_Cmd(cmd)
	#winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	#time.sleep(5)
	#winstaob.WinSTA_Connect(node2ssid)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step8 failed!!cannot reach " + node2ip)
		exit()	
	node2_mode = devobj.Get_node_mode(node2ip)
	if node2_mode != "FFR" :
		print "\nStep8 Failed!!Node2 is not FFR!!"
		exit()		
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = ''	
	dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
	if re.search(node1_mac,dev_node_mode,re.IGNORECASE) :
		logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node2 is FFR and Node1 is FFN"))
		logobj.AppendLog(logFH,("\n Step8 is verified successfully!"))
	else :
		logobj.AppendLog(logFH,("\n Step8 Failed!!Mesh network is not formed!!"))
		exit()
	
	cmd = "ping -c 10 " + node1ip
	out = ''
	out = devobj.execute_via_telnet(node2ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"
	else:
		print "Step8 Failed,ping from node 2 to node 1 Fail!!"
		exit()
	#############################################
	#Executing Step9		  
	#####################################################
	''' Reset the Testbed '''
# 	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=disabled"
#  	winstaob.Exec_Win_Cmd(cmd)	
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node1ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 is unreachable")
		exit()
	cmd = "reboot"
	print "\n\n Rebooting node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(5)
	winstaob.WinSTA_Connect(node2ssid)
	time.sleep(2)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 is unreachable")
		exit()
	cmd = "reboot"
	print "\n\n Rebooting node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	cmd = "netsh interface set interface name=\"Ethernet 6\" admin=enabled"
	winstaob.Exec_Win_Cmd(cmd)
	time.sleep(2)
	logobj.AppendLog(logFH,"\n\n Test case execution complete!!!")
	logFH.close()  
	exit()
