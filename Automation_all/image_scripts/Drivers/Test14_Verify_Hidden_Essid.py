import json
from requests.packages import urllib3
import datetime
#from CommonOperations import *
import time
import pandas as pd
import unittest
import csv
import Common_lib
import dev_function
from Common_lib  import CommonOperations
from dev_function import *
import re

# Function to write log in the logfile

def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'w+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+NMS_Param])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return



def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//getInterface.csv")
    #d_excel("config_data.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    Supported_protocol = df.Supported_protcol[tc]
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]
    Interface2_Name = df.Interface2_Name[tc]

    test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, Supported_protocol, Interface_Name, Network_Name, API_Server_IP,Interface2_Name]
                 
    return test_params

def Setup_Parser_method(tc):
        count = 0
	while count<= tc:
		if count == tc:
			data = pd.read_csv("..//Data//Test1_Setup.csv")
			NAME = "NAME"+str()
			NAME = data.NAME[count]
			MAC = data.MAC[count]
			Dev_IP =  data.IP[count]
			TYPE = data.TYPE[count]
			MODEL = data.MODEL[count]
			NODE_USER = data.USER_NAME[count]
			setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
			return setup_values
			
			
		count = count+1
        
              
class Test14_Verify_Hidden_Essid():
    def init(self):
        self.object = object
    
    def test14_Verify_Hidden_Essid(self,intf1, tc):
        try:
            val = Setup_Parser_method(tc)
            #NAME, MAC, IP, TYPE, MODEL,NODE_USER
            NAME = val[0]
            Dev_MAC_Address = val[1]
            NMS_Device_IP = val[2]
            Type = val[3]
            Model = val[4]
            Node_User= val[5]
            api_data = Input_Data_Parser(tc)
            api_data[0]
            API_Name = api_data[0]
            Method = api_data[1]
            Content_Type = api_data[2]
            Arg1 = api_data[3]
            Exp_st_code= api_data[4]
            Precondition = api_data[5]
            Supported_protocol= api_data[6]
            #Interface_Name = api_data[7]
            Interface_Name = intf1
            Network_Name = api_data[8]
            API_Server_IP= api_data[9]
            #Interface2_Name = api_data[10]
            Interface2_Name = intf1
            url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?interfaceName="+Interface2_Name+"&networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
            print url
            
            Dev_IP= NMS_Device_IP 
            print "Test Details: "
            verify_api_param = 'hide_essid'
            #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
            s= CommonOperations()
            tc_strt = datetime.datetime.now()            
            p=s.commonOperations(tc, Method, url, Content_Type, Arg1, Exp_st_code, Precondition, Supported_protocol, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
            op = json.dumps(p,ensure_ascii=True)
            op1 = json.loads(op)
            NMS_Hidden_ESSID = op1["essidHidden"]
        
        #     print "Beaconinterval value from API Response : ", Beaconinterval
            
            """ Verification of Device Data """
            print "Device Data Verification Starts Now.."
            current_time = time.localtime()
            dev_Logfilename = "Test14_Verify_Hidden_Essid_Device_output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
            #dev_filepath = "..//Output//"+dev_Logfilename
            dev_filepath = "..//Output//"+dev_Logfilename
           
            telnet_user = "root"
            dev_obj = dev_function()
            
            print "Verification of Parameters in meshapp.conf"
            
            essid_hidden_dev=dev_obj.get_intf_hssid(Node_User,dev_filepath,NMS_Device_IP,Interface2_Name)
            print essid_hidden_dev
            
            if  str(NMS_Hidden_ESSID) == str(essid_hidden_dev):
                print "Values Matched..."
                test_status = "Pass"
                addOutput(dev_filepath,op, essid_hidden_dev, tc, test_status)
                return test_status
            else:
                print "Values not matched..."
                test_status = "Fail"
                addOutput(dev_filepath,op, essid_hidden_dev, tc, test_status)
                return test_status
        except Exception as e:
            #print ("Error ....sys.exec_info()[0], has occured....")
            print("Data is not received or Check the Connectivity")
            print(e)
            return "Fail"
