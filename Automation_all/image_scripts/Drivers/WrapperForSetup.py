import csv
import datetime
import time
from Common_lib import *
from dev_function import *
import pandas as pd
import json
from Test1_Verify_IPAddress import *
from Test2_Update_verifyCountryCode import *
from Test3_UpdateEssid_API_Device import *
from Test4_Update_Verify_nodeName import *
 
from Test5_Get_Beacon_Int import *
from Test6_getdot11e_cat import *
from Test7_getdot11e_en import *
from Test8_get_intf_channel import *
from Test9_get_intf_frag_thr import *
from Test10_get_intf_rts_thrs import *

from Test11_get_intf_tx_pow import *
from Test12_get_intf_tx_rate import *
from Test13_VerifyNode_Name import *
from Test14_Verify_Hidden_Essid import *
from Test15_HrtBt_Int_ThruGeneralConf import *
from Test16_CountryCode_Verification import *
import subprocess
import sys
''' ################################################# '''
from Test17_Verify_Description import *
import traceback
from Test18_MAC_Add_Validation import *
from Test20_Add_Update_Vlan_info import *
from Test22_Update_PreferredParent import *
from Test23_Delete_Vlan_info import *
from Test24_Medium_SubType import *
from Test33_Delete_ACL_info import *
from Test26_Subnet_Verification import *
#from Test29_Get_HeartBeat_info import *
# from Test30_CreateNetwork import *
from Test32_AddUPdateACLInfo import *
from Test36_UpdateInterAdvanceInfo import *
from Test35_Get_Vlan_infoESSID import *
from Test34_Get_Vlan_infoName import *
from Test50_Update_intf_txpower import *
from Test51_Update_Verify_IPaddress import *
from Test23_Delete_Vlan_info import *

import traceback
import sys
''' ################################################# '''

from Test39_UpdateNVerifyHost_Name import *
from Test42_General_Gateway_Verification import * 
#from Test43_Update_intf_frag_thrs import *
#from Test44_Update_Rts_thrs import *
#from Test45_HTUpdateInterAdvanceInfo import *
#from Test49_Update_ht_Rx_Stbc import *
#from Test50_Update_ht_Tx_Stbc import *
#from Test55_Update_intf_rts_thrs import *
'''#####################################################'''



class Wrapper(object):
	tcount =1
	def init(self, tcount):
		self.object = object
		self.tcount = tcount
		tcount = 1
		testresult = []
	def getInterface(self,row_num, tcount):
		print "Execution Starts for Testcase number ",tcount
		print "-----------Running Test16_Verifying_Country_Code Using getInterface API--------------:"
		test_ver6 = Test16_Verifying_Country_Code()
		val = test_ver6.test16_Verifying_Country_Code(row_num)		
		print "------------------------------------------------------------------------------"
		print "\n\n"   
		
		
		tcount = tcount +1
		testresult =(["test16_Verifying_Country_Code", val])
		print "------------------------------------------------------------------------------"
		print "\n\n"

		print "-----------Running Test15_Verify_HrtBt_Int_ThruGeneralConf Using getInterface API--------------:"
		print "Execution Starts for Testcase number ",tcount
		test_ver6 = Test15_Verify_HrtBt_Int_ThruGeneralConf()
		val=test_ver6.test15_Verify_HrtBt_Int_ThruGeneralConf(row_num) 
		testresult = ("test15_Verify_HrtBt_Int_ThruGeneralConf", val)
		tcount = tcount +1
		print "------------------------------------------------------------------------------"
		print "\n\n"

		
		print "-----------Running Test13_VerifyNode_Name: Using getInterface API--------------:"
		print "Execution Starts for Testcase number ",tcount
		test_ver6 = Test13_VerifyNode_Name()
		val = test_ver6.test13_VerifyNode_Name(row_num)
		testresult = ["test13_VerifyNode_Name", val]
		print "------------------------------------------------------------------------------"
		print "\n\n"


		
		tcount = tcount +1
		
		print "------------------------------------------------------------------------------"
		print "\n\n"

		print "-----------Running Test18_MAC_Add_Validation Using getInterface API--------------:"
		print "Execution Starts for Testcase number ",tcount
		test_ver6 = Test18_MAC_Add_Validation()
		val = test_ver6.test18_MAC_Add_Validation(row_num)
		testresult = ["test18_MAC_Add_Validation", val]
		print "------------------------------------------------------------------------------"
		print "\n\n"

		
		tcount = tcount +1
		Interface_Name1 = ['wlan0','wlan2']
 
		for i in Interface_Name1:
			print "Execution Starts for Testcase number ",tcount
			print "-----------Running Test10_get_rts_tx_thrs: Using getInterface API--------------:"
			test_ver6 = Test10_get_intf_rts_thrs()
			val = test_ver6.test10_get_intf_rts_thrs(i, row_num)			   
			testresult = ["test10_get_intf_rts_thrs", val]
			
			tcount = tcount +1
			print "------------------------------------------------------------------------------"
			print "\n\n"

			print "-----------Running Test9_get_intf_frag_thr Using getInterface API--------------:"
			print "Execution Starts for Testcase number ",tcount
			print "Calling getInterface API for :"
			test_ver6 = Test9_get_intf_frag_thr()
			val = test_ver6.test9_get_intf_frag_thr(i,row_num)
			testresult = ["test9_get_intf_frag_thr", val]  
 			
			print "------------------------------------------------------------------------------"
			print "\n\n"



 			tcount = tcount +1
			print "-----------Running Test11_Verify_Get_Intf_Tx_Pow Using getInterface API--------------:"
			print "Execution Starts for Testcase number ",tcount	
			test_ver6 = Test11_Verify_Get_Intf_Tx_Pow()
			val = test_ver6.test11_Verify_Get_Intf_Tx_Pow(i, row_num)
			testresult = ["test11_Verify_Get_Intf_Tx_Pow", val]
			
			print "------------------------------------------------------------------------------"
			print "\n\n"


			tcount = tcount +1
			
			print "-----------Running Test12_get_intf_tx_rate Using getInterface API--------------:"
			print "Execution Starts for Testcase number ",tcount	
			test_ver6 = Test12_Verify_Get_Intf_Tx_rate()
			val = test_ver6.test12_Verify_Get_Intf_Tx_rate(i, row_num)
			
			print "------------------------------------------------------------------------------"
			print "\n\n"


			tcount = tcount +1
			
			testresult = ["test12_Verify_Get_Intf_Tx_rate", val]
			print "-----------Running Test14_Verify_Hiddn_Essid Using getInterface API--------------:"
			print "Execution Starts for Testcase number ",tcount
			print "Calling getInterface API for Test14_Verify_Hiddn_Essid"
			test_ver6 = Test14_Verify_Hidden_Essid()
			val = test_ver6.test14_Verify_Hidden_Essid(i, row_num)
			testresult = ["test14_Verify_Hidden_Essid", val]
			
			print "------------------------------------------------------------------------------"
			print "\n\n"
			
			tcount = tcount +1
			
			
			print "-----------Running Test6_Dot11e_cat Using getInterface API--------------:"
			print "Execution Starts for Testcase number ",tcount
			test_ver6 = Test6_getdot11e_cat()
			val = test_ver6.test6_getdot11e_cat(i,row_num)
			testresult = ["test6_getdot11e_cat", val]
 			
			print "------------------------------------------------------------------------------"
			print "\n\n"
		 	tcount = tcount +1
			print "-----------Running Test5_Get_Beacon_Int Using getInterface API--------------:"
			print "Execution Starts for Testcase number ",tcount			
			test_ver6 = Test5_Get_Beacon_Int()
			val = test_ver6.test5_Get_Beacon_Int(i, row_num)			   
			testresult = ["test5_Get_Beacon_Int", val]
			

			print "------------------------------------------------------------------------------"
			print "\n\n"

		 	tcount = tcount +1 
			print "-----------Running Test7_getdot11e_en Using getInterface API--------------:"
			print "Execution Starts for Testcase number ",tcount
			test_ver6 = Test7_getdot11e_en()
			val = test_ver6.test7_getdot11e_en(i, row_num)
			
			print "------------------------------------------------------------------------------"
			print "\n\n"
		 	
			tcount = tcount +1 
			testresult = ["test7_getdot11e_en", val]
			print "-----------Running Test8_get_intf_channel Using getInterface API--------------:"
			print "Execution Starts for Testcase number ",tcount	
			test_ver6 = Test8_get_intf_channel()
			val = test_ver6.test8_get_intf_channel(i, row_num) 
			testresult = ["test8_get_intf_channel", val]
			print "------------------------------------------------------------------------------"
			print "\n\n"

		 	tcount = tcount +1
			
		return tcount
			
	def putInterface(self, row_num, tcount):
		#Interface_Name = ["v_wlan0","wlan2"]
		#for i in Interface_Name:
		    #print "Calling Put Interface for Test3_Update_ESSID_FrmAPI:"
		    #test_ver5 = Test15_put_ESSID_FrmAPI()
		    #test_ver5.test15_put_ESSID_FrmAPI(i,row_num)

		Interface_Name = ['wlan0','wlan2']
		for i in Interface_Name:
 			print "-----------Running the Test3_Update_ESSID_FrmAPI Using putInterface API------:"
			print "Execution Starts for Testcase number ",tcount
 			test_ver6 = Test3_Update_ESSID_FrmAPI()
 			val = test_ver6.test3_Update_ESSID_FrmAPI(i,row_num)
 			testresult = ["Test3_Update_ESSID_FrmAPI", val]
 			

			print "------------------------------------------------------------------------------"
			print "\n\n"

		 	tcount = tcount +1
# 			print "-----------Running Test24_Medium_SubType Using putInterface API--------------:"
# 			print "Calling Put Interface for Test24_Medium_SubType:"
# 			test_n12 = Test24_Medium_SubType()
# 			val = test_n12.test24_Medium_SubType(i,row_num)
# 			testresult = ["test24_Medium_SubType", val]
# 			print testresult

		return tcount

#  			
	def get_Vlan_InfoName(self, row_num, tcount):
		print "-----------Running Test35_Get_Vlan_infoESSID Using get_Vlan_InfoName API---------:"	
		print "Execution Starts for Testcase number ",tcount	
		test_ver6 = Test35_Get_Vlan_infoESSID()
		val = test_ver6.test35_Get_Vlan_infoESSID(row_num)
		testresult = {"test35_Get_Vlan_infoESSID", val}
		

		print "------------------------------------------------------------------------------"
		print "\n\n"

		tcount = tcount +1
		
		
		print "-----------Running Test34_Get_Vlan_infoName Using get_Vlan_InfoName API----------:"	
		print "Execution Starts for Testcase number ",tcount	
		test_ver6 = Test34_Get_Vlan_infoName()
		val = test_ver6.test34_Get_Vlan_infoName(row_num)
		testresult = {"test34_Get_Vlan_infoName", val}
		

		print "------------------------------------------------------------------------------"
		print "\n\n"

		tcount = tcount +1
		return tcount
		
	def GeneralConfiguration(self, row_num,tcount):

		Interface_Name = ['eth0','eth1']
	       	for i in Interface_Name:
			print "Execution Starts for Testcase number ",tcount
			test_ver = Test42_General_Gateway_Verification()
                        test_ver.test42_General_Gateway_Verification(i, row_num)
			tcount = tcount + 1


		print "######################################################################"
		print "--------Running Test1_Verify_IPAddress Using API-GeneralConfiguration--------"	
		print "Execution Starts for Testcase number ",tcount
		test_ver6 = Test1_Verify_IPAddress()
		val = test_ver6.test1_Verify_IPAddress(row_num)
		testresult = ["test1_Verify_IPAddress", val]
		print testresult
		print "------------------------------------------------------------------------------"
		print "\n\n"

		tcount = tcount +1
		
		print "-------Running Test26_Subnet_Verification Using GeneralConfiguration API-----:" 
		print "Execution Starts for Testcase number ",tcount
		test_ver6 = Test26_Subnet_Verification()
		val = test_ver6.test26_Subnet_Verification(row_num)	
		
		testresult = ["test26_Subnet_Verification", val]
		print testresult
		tcount = tcount + 1

		#print "------------------------------------------------------------------------------"
		#print "\n\n"

		#tcount = tcount +1
		
		#print "------Running Test16_Verifying_Country_Code Using GeneralConfiguration API---:"
		#test_ver6 = Test16_Verifying_Country_Code()
		#val =  test_ver6.test16_Verifying_Country_Code(row_num)
		#testresult = ["Test16_Verifying_Country_Code", val]
		#print testresult
		
		#print "------------------------------------------------------------------------------"
		#print "\n\n"
		


		#tcount = tcount +1
		return tcount
		
		
	def updateGeneralconfiguration(self,row_num, tcount):

		 interf = ['wlan0','wlan2']
                 tx_pow = [100,50]
		 
		 hostname_val = ["Test_val", "OpenWrt"]

		 for val in hostname_val:
			print "Executing test39_UpdateNVerifyHost_Name"             
			print "Execution Starts for Testcase number ",tcount
			test39 = Test39_UpdateNVerifyHost_Name()
	    	        val_testcase = test39.test39_UpdateNVerifyHost_Name(row_num, val)
			print "Testcase executed with hostname",val
			print "The status of the testcase is ", val_testcase

			print "------------------------------------------------------------------------------"
		       	print "\n\n"
		 	tcount = tcount +1








		 for i in interf:  				
			for t in tx_pow:
				print "Executing Test50_Update_intf_txpower"             
				print "Execution Starts for Testcase number ",tcount
                        	test50 = Test50_Update_intf_txpower()
                        	val = test50.test50_Update_intf_txpower(i,t,row_num)
		        	testresult = ["test50_Update_intf_txpower",val]
		        	print testresult
	                	print "------------------------------------------------------------------------------"
                        	print "\n\n"
	                 	tcount = tcount +1

		 data = pd.read_csv("..//Data//Test1_Setup.csv")
		 NAME = data.NAME[row_num]
		 MAC = data.MAC[row_num]
		 Dev_IP =  data.IP[row_num]
		 Ip_add =['192.168.0.100',Dev_IP]
		 for i in Ip_add:
			print "Execution Starts for Testcase number ",tcount	
			print "Executing Test51_Update_Verify_IPaddress"
			print "Executing for IP Address : ",str(i)
		 	test43 = Test51_Update_Verify_IPaddress()
                 	val = test43.test51_Update_Verify_IPaddress(i,row_num) 				 
			testresult = ["test51_Update_Verify_IPaddress",val]
			print testresult
			tcount = tcount + 1
			print "------------------------------------------------------------------------------"
			print "\n\n"
				




		 country_Code = [0]
		 for i in country_Code:
			print "##########################################################################"
			print "-------Running Test2_Update_VerifyCountry_Code & Calling Update Gen Conf API--:"
			print "Execution Starts for Testcase number ",tcount				
			test_ver6 = Test2_Update_verifyCountryCode()
			val = test_ver6.test2_Update_verifyCountryCode(i, row_num)
			testresult = ["test2_Update_verifyCountryCode", val]
			print testresult
			


			print "------------------------------------------------------------------------------"
			print "\n\n"
		 	

			tcount = tcount +1

		 print "\n\n"
		 print "##########################################################################"
		 print "Testcase for Preferred Parent Starting now...." 
   		 print "Execution Starts for Testcase number ",tcount 
		 valt = Test22_Update_PreferedParent()
		 valt.test22_Update_PreferedParent(row_num)
		 print "------------------------------------------------------------------------------"
		 print "\n\n"
		 tcount = tcount +1
		 return tcount
		


##############################################################
	def Add_Vlan(self,row_num, tcount):
		print "-----------Running Test20_Add_Update_Van_info Using Add_Vlan API-----:"
		print "Execution Starts for Testcase number ",tcount
		test_ver6 = Test20_Add_Update_Vlan_info()
		val = test_ver6.test20_Add_Update_Vlan_info(row_num)
		testresult = ["test20_Add_Update_Vlan_info", val]
		

		print "------------------------------------------------------------------------------"
		print "\n\n"

		tcount = tcount +1

		return tcount


		
	def AddACLInfo(self,row_num, tcount):
		print "-----------Running Test32_AddACLInfo Using AddACLInfo API-------------:"
		print "Execution Starts for Testcase number ",tcount
		test_ver6 = Test32_AddUPdateACLInfo()
		val = test_ver6.test32_AddUPdateACLInfo(row_num)   
		testresult = ["test32_AddUPdateACLInfo", val]
		
		print "------------------------------------------------------------------------------"
		print "\n\n"


		tcount = tcount +1
		return tcount		



			 
	def createNetwork(self,row_num, tcount):
		print "-----------Running Test30_CreateNetwork Using createNetwork API---------------:"
		print "Execution Starts for Testcase number ",tcount
		test_ver6 = Test30_CreateNetwork()
		val = test_ver6.Test30_CreateNewNetwork(row_num)
		testresult = ["Test30_CreateNewNetwork", val]
		

 		print "------------------------------------------------------------------------------"
		print "\n\n"

		tcount = tcount +1
		print testresult
		
		return tcount


	def Delete_Vlan(self,row_num, tcount):
		 print "-----------Running Test23_Delete_Vlan_info Using Delete_Vlan API--------------:"
  		 print "Execution Starts for Testcase number ",tcount
		 test_ver6 = Test23_Delete_Vlan_info()
		 val = test_ver6.test23_Delete_Vlan_info(row_num)
		 testresult = ["Test23_Delete_Vlan_info", val]
		 print "Test Number: {} ".format(tcount)+ "\n"+ "\n"+  "Test Execution Result is {}".format(val)

	         print "------------------------------------------------------------------------------"
		 print "\n\n"

		 tcount = tcount +1
		 return tcount		 	  



	def DeleteACLInfo(self,row_num, tcount):
		 print "----------Running Test33_Delete_ACL_info Using DeleteACLInfo API--------------:"
 		 print "Execution Starts for Testcase number ",tcount 		 
		 test_ver6 = Test33_Delete_ACL_info()
		 val = test_ver6.test33_Delete_ACL_info(row_num)		 
		 testresult = ["Test33_Delete_ACL_info", val]
		 tcount = tcount+1

		 print "------------------------------------------------------------------------------"
		 print "\n\n"
 		 return tcount
		 


# 	def HeartBeatInfo(self,row_num):
# 		Interface_Name = ['wlan0','wlan2',]
# 		for i in Interface_Name:
# 			print "-----------Running Test29_Get_HeartBeat_info Using HeartBeatInfo API--------------:"	
# 			test_n13 = Test29_Get_HeartBeat_info()
# 			val = test_n13.test29_Get_HeartBeat_info(i,row_num)
# 			testresult = ["Test29_Get_HeartBeat_info", val]
# 			print testresult
# 			return testresult
#	 def move_Node(self,row_num):
#		 print "-----------Running  Test31_MoveNode Using move_Node API----:"
#		 test_n14 = Test31_MoveNode()
#		 val = test_n14.test31_MoveNode(i,row_num)
#		   testresult = {"test31_MoveNode", val}  	



	def putInter_Advanceinfo(self,row_num, tcount):
		mediumSubType =[1,12,13,14]
		Interface_Name = ['wlan0','eth0']
	 	for i in Interface_Name:
	 		print "-----------Running Test36_UpdateInterAdvanceInfo Using putInterface API------:"	
			print "Execution Starts for Testcase number ",tcount
	 		test_ver6 = Test36_UpdateInterAdvanceInfo()
	 		val = test_ver6.test36_UpdateInterAdvanceInfo(i,row_num)
			testresult = ["Test36_UpdateInterAdvanceInfo", val]
			print "Test Number: {} ".format(tcount)+  "\n"+ " Test Execution Result is {}".format(val)
			print "------------------------------------------------------------------------------"
			print "\n\n"
		 	tcount = tcount +1
		return tcount
	

######################################################################

try:

	data = pd.read_csv("..//Data//Test1_Setup.csv")
	num_node = len(data)

	node = []
	node_config=[]
	tcount = 0
	row_num=0
	setup_handler = pd.read_csv("..//Data//Test1_Setup.csv")
	node_count = 0
	print "Total Number of node to be tested are :",num_node
	
	while row_num < num_node:
		current_time = datetime.datetime.now()
		print "Testing Started at: ",str(current_time)
		WrapOver = Wrapper()

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR Get Interface API,  EXECUTION BEGINS >>>>>>>>>>>>>>>>>>"
		print "For Node number : ",row_num
		print "\n\n"
		
		tcount = WrapOver.getInterface(row_num, tcount) 	


		print "<<<<<<<<<<<<<<<<< TESTCASE FOR Get Interface API, ENDS HERE >>>>>>>>>>>>>>>>>>"
		print "------------------------------------------------------------------------------"
		print "\n\n"

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR UPDATE GENERAL CONFIGURATION EXECUTION BEGINS >>>>>>>>>>>>>>>>>>"
		print "For Node number : ",row_num
		print "\n\n"
	   	tcount = WrapOver.updateGeneralconfiguration(row_num, tcount)
		print "<<<<<<<<<<<<<<<<< TESTCASE FOR UPDATE GENERAL CONFIGURATION ENDS HERE >>>>>>>>>>>>>>>>>>"
		print "------------------------------------------------------------------------------"
		print "\n\n"

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR putInterface EXECUTION BEGINS >>>>>>>>>>>>>>>>>>"
		print "For Node number : ",row_num
		print "\n\n"

	   	tcount = WrapOver.putInterface(row_num, tcount)

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR Put Interface ENDS HERE >>>>>>>>>>>>>>>>>>"
		print "------------------------------------------------------------------------------"
		print "\n\n"

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR Add_Vlan EXECUTION BEGINS >>>>>>>>>>>>>>>>>>"
		print "For Node number : ",row_num
		print "\n\n"

	 	tcount = WrapOver.Add_Vlan(row_num, tcount)
		print "<<<<<<<<<<<<<<<<< TESTCASE FOR Add_Vlans ENDS HERE >>>>>>>>>>>>>>>>>>"
		print "------------------------------------------------------------------------------"
		print "\n\n"

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR get_Vlan_InfoName BEGINS >>>>>>>>>>>>>>>>>>"
		print "For Node number : ",row_num
		print "\n\n"

	  	tcount = WrapOver.get_Vlan_InfoName(row_num, tcount) 	

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR get_Vlan_InfoName ENDS HERE >>>>>>>>>>>>>>>>>>"
		print "------------------------------------------------------------------------------"
		print "\n\n"	
		

		
	

		#print "<<<<<<<<<<<<<<<<< TESTCASE FOR HeartBeatInfo BEGINS >>>>>>>>>>>>>>>>>>"
		#print "For Node number : ",row_num
		#WrapOver.HeartBeatInfo(row_num)
		
		#print "<<<<<<<<<<<<<<<<< TESTCASE FOR HeartBeatInfo ENDS HERE >>>>>>>>>>>>>>>>>>"
		#print "------------------------------------------------------------------------------"
		#print "\n\n"
		
		"""  Add ACL Info API Is not working as expected, commenting the script... """
		print "<<<<<<<<<<<<<<<<< TESTCASE FOR AddACLInfo BEGINS >>>>>>>>>>>>>>>>>>"
		print "For Node number : ",row_num
		print "\n\n"

	  	WrapOver.AddACLInfo(row_num,tcount)	


		print "<<<<<<<<<<<<<<<<< TESTCASE FOR AddACLInfo ENDS HERE >>>>>>>>>>>>>>>>>>"
		print "------------------------------------------------------------------------------"
		print "\n\n"

		

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR GeneralConfiguration BEGINS >>>>>>>>>>>>>>>>>>"
		print "For Node number : ",row_num
		print "\n\n"


		tcount = WrapOver.GeneralConfiguration(row_num,tcount)		

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR GeneralConfiguration ENDS HERE >>>>>>>>>>>>>>>>>>"
		print "------------------------------------------------------------------------------"
		print "\n\n"

		
		print "<<<<<<<<<<<<<<<<< TESTCASE FOR Delete VLAN BEGINS >>>>>>>>>>>>>>>>>>"
		print "For Node number : ",row_num
		print "\n\n"

	  	tcount = WrapOver.Delete_Vlan(row_num, tcount) 	

		print "<<<<<<<<<<<<<<<<< TESTCASE FOR Delete VLAN ENDS HERE >>>>>>>>>>>>>>>>>>"
		print "------------------------------------------------------------------------------"
		print "\n\n"			


	
		#print "<<<<<<<<<<<<<<<<< TESTCASE FOR Delete ACL BEGINS >>>>>>>>>>>>>>>>>>"
		#print "For Node number : ",row_num
		#print "\n\n"

	  	#tcount = WrapOver.DeleteACLInfo(row_num, tcount) 	

		#print "<<<<<<<<<<<<<<<<< TESTCASE FOR Delete ACL ENDS HERE >>>>>>>>>>>>>>>>>>"
		#print "------------------------------------------------------------------------------"
		#print "\n\n"	




		print " +++++++++++++++++++++++++++++++++++++++++++++ "

		finish_time = datetime.datetime.now()
		print "Node number %d  testing completed"%(row_num)
		print "Total Testcase executed for the node are : ",tcount
	
		print "\n"
		print "\n"
		print "_____________________________________________________________________________________"
		print "_____________________________________________________________________________________"	
		print "_____________________________________________________________________________________"
		print "\n"
		row_num = row_num+1		
		
				
	#md_transition = "python /home/benison/MS/imageUpgrade/src/Mix/image_scripts/Drivers/MS4_Nnodes_script_mgmt_ip.py"
	
	
except Exception as e:
	print(e)
	traceback.print_exc()
	print" Traceback for the exception is:  ",(traceback.format_exc())
	print"Sys.exc_Info is  ",(sys.exc_info()[0])
