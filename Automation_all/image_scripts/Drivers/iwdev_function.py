import telnetlib
import time
import sys
import copy
import re
import csv
from pip._vendor.requests.packages.urllib3.util.timeout import current_time
from telnetlib import Telnet
from Log import *
from subprocess import check_output, CalledProcessError


class iwdev_function(object):   
    def read_from_iwconfig_intf(self,Node_User,dev_filepath,NMS_Device_IP,intf1):
        tn = telnetlib.Telnet(NMS_Device_IP)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
             #print "please login to continue"
            tn.write("root"+'\r\n')
             # print "login successfully"
            time.sleep(4)
            console_val = tn.read_until(':')
            print console_val
  
            if 'Password:' in console_val:
                tn.write(""+'\r\n')
            log_val = tn.read_until(':')
            print log_val
        print "Login successfull..."
        cmd_essid = "iwconfig "+intf1
        tn.write(cmd_essid+"\n")  
        tn.write("exit\n")
        time.sleep(2)
        cmd_res = tn.read_all()
        logfile = open(dev_filepath, 'a')
        logfile.write(cmd_essid)
        logfile.close()
        return cmd_res
        
        
        
    def get_intf_rts_thr_from_iwconfig(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
        iwdev_obj= iwdev_function()
        block_str = iwdev_obj.read_from_iwconfig_intf(Node_User,dev_filepath,NMS_Device_IP,Interface2_Name)
        #mat   = re.search(r'(RTS\sthr\:\s*)([1-9]{1,4})',block_str)
        mat   = re.search(r'(RTS\sthr\:)(\w+)',block_str)
        if mat:
            Rts_threshold_iw = mat.group(2)
        print " Rts threshold is :",Rts_threshold_iw
        return Rts_threshold_iw 
       
    
    
    
    
     
    def read_iwlist_for_tx(self, user, dev_filepath, ipaddress):
        print ipaddress
        tn = telnetlib.Telnet(ipaddress)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
             #print "please login to continue"
            tn.write("root"+'\r\n')
             # print "login successfully"
            time.sleep(4)
            console_val = tn.read_until(':')
            print console_val
  
            if 'Password:' in console_val:
                tn.write('\r\n')
            log_val = tn.read_until(':')
            print log_val
        print "Login successfull..."
        cmd_essid = "iwlist wlan0 txpower"
        tn.write(cmd_essid+"\n")  
        tn.write("exit\n")
        time.sleep(2)
        cmd_res = tn.read_all()
        logfile = open(dev_filepath, 'a')
        logfile.write(cmd_essid)
        logfile.close()
        return cmd_res
    
    ''' To Extract Txpower from iwlist wlan0 txpower  '''
    def read_iwlist(self, user, dev_filepath, ipaddress, intfl1):
        print ipaddress
        tn = telnetlib.Telnet(ipaddress)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
             #print "please login to continue"
            tn.write("root"+'\r\n')
             # print "login successfully"
            time.sleep(4)
            console_val = tn.read_until(':')
            print console_val
  
            if 'Password:' in console_val:
                tn.write('\r\n')
            log_val = tn.read_until(':')
            print log_val
        print "Login successfull..."
        cmd_essid = "iwlist " +intfl1 +" txpower"
        # iwlist wlan0 txpower
        tn.write(cmd_essid+"\n")  
        tn.write("exit\n")
        time.sleep(2)
        cmd_res = tn.read_all()
        logfile = open(dev_filepath, 'a')
        logfile.write(cmd_essid)
        logfile.close()
        return cmd_res

	'''  
	To Fetch values for Rts and Fragment Threshold
	 '''
    def read_iwconfig_fragthrs(self, user, dev_filepath, ipaddress, intfl1):
        print ipaddress
        tn = telnetlib.Telnet(ipaddress)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
             #print "please login to continue"
            tn.write("root"+'\r\n')
             # print "login successfully"
            time.sleep(4)
            console_val = tn.read_until(':')
            print console_val
  
            if 'Password:' in console_val:
                tn.write('\r\n')
            log_val = tn.read_until(':')
            print log_val
        print "Login successfull..."
        cmd_essid = "iwconfig " +intfl1
        tn.write(cmd_essid+"\n")  

        tn.write("exit\n")
        time.sleep(2)
        cmd_res = tn.read_all()
        logfile = open(dev_filepath, 'a')
        logfile.write(cmd_essid)
        logfile.close()
        return cmd_res





    
    ''' To Extract Essid from iw_wlan0 info'''
    def read_iw_v_wlan0(self, user, dev_filepath, ipaddress, intfl1):
        print ipaddress
        tn = telnetlib.Telnet(ipaddress)
        cmd_val = tn.read_until(':')
        if "root" in cmd_val :
            print "Login is not required.."
        else:
            time.sleep(2)
             #print "please login to continue"
            tn.write("root"+'\r\n')
             # print "login successfully"
            time.sleep(4)
            console_val = tn.read_until(':')
            print console_val
  
            if 'Password:' in console_val:
                tn.write('\r\n')
            log_val = tn.read_until(':')
            print log_val
        print "Login successfull..."
        
        cmd_essid = "iw " +intfl1 +" info"
        print cmd_essid
        #iw v_wlan0 info
        tn.write(cmd_essid+"\n")  
        tn.write("exit\n")
        time.sleep(2)
        cmd_res = tn.read_all()
        logfile = open(dev_filepath, 'a')
        logfile.write(cmd_essid)
        logfile.close()
        return cmd_res
    
    #def get_intf_tx_pow(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
    def read_tx_pow_from_iwlist_for_tx(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):    
        iwdev_obj= iwdev_function()
        block_str = iwdev_obj.read_iwlist(Node_User,dev_filepath,NMS_Device_IP)
        tx_power   = re.search(r'(Tx-Power)=([0-9]{1,3})',block_str,re.I)
        mat = re.search(r'(Tx-Power\=)([0-9]{1,3})',tx_power)
        if mat:
            print "Match is ",mat.group(2)
            txPower_dbm = mat.group(2)  
	else:
	    print "Tx power not found"
	    txPower_dbm = "None"

        print " tx_power  is :", txPower_dbm
        return txPower_dbm 

        ''' Fragment Threshold'''          
    

    def get_intf_frag_thr_from_iwconfig(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
        iwdev_obj= iwdev_function()
        block_str = iwdev_obj.read_iwconfig_fragthrs(Node_User,dev_filepath,NMS_Device_IP,Interface2_Name)
	print block_str

        mat   = re.search(r'(Fragmentation\sthreshold\:\s*)([1-9]{1,4})',block_str)
        if mat:
            Fragment_threshold_iw = mat.group(2)
	    print "from iw config frag threshold is ", Fragment_threshold_iw
            return Fragment_threshold_iw 	



 
    def get_intf_rts_thr_from_iwlist(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
        iwdev_obj= iwdev_function()
        block_str = iwdev_obj.read_iwlist(Node_User,dev_filepath,NMS_Device_IP, "wlan2")
        print block_str
        mat   = re.search(r'(RTS\sthreshold\:\s*)([1-9]{1,4})',block_str)
	
        if mat:
            Rts_threshold_iw = mat.group(2)
            print " RTS threshold is :",Rts_threshold_iw
            return Rts_threshold_iw 
       

        
