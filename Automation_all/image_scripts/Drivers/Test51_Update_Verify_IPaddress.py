import json
import datetime
import time
import pandas as pd
import csv
import Common_lib
import dev_function
from Common_lib  import CommonOperations
from dev_function import *
import re


def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'w+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+str(NMS_Param)])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return

def Get_mipIP(mgmtip,user="root"):
    out = ''
    temp = ''
    cmd = r"ifconfig mip0"
    try:
        tn = telnetlib.Telnet(mgmtip, int(23),int(50))
        cmd_val = tn.read_until('login')
        if "root" in cmd_val :
            pass
        else:
            tn.write(user+'\n')
            cmd_val = ''
            cmd_val = tn.read_until('password:',10)
    except Exception as e:
        print "Oops Something went wrong while starting telnet to ",mgmtip
        print(e)
        return "Fail"
    tn.write('\n\r')
    while temp.find('#') == -1:
        temp = tn.read_very_eager()
    tn.write(cmd+'\n\r')
    time.sleep(1)
    while out.find('#') == -1:
        out = tn.read_very_eager()
    time.sleep(1)
    tn.write("exit\n")
    tn.close()
    if not out :
        return "Fail"
    rexp = r'(([2][5][0-5]\.)|([2][0-4][0-9]\.)|([0-1]?[0-9]?[0-9]\.)){3}'+\
        '(([2][5][0-5])|([2][0-4][0-9])|([0-1]?[0-9]?[0-9]))'
    matobj = re.search(rexp,out,re.I)
    if matobj:
        return matobj.group()
    return "Fail"
     
def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//updateGeneralConfiguration.csv")
    #d_excel("config_data.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]
    #mip0_ip = df.MIP0_IP[tc]
    test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code,\
        Precondition, Interface_Name, Network_Name, API_Server_IP]
                 
    return test_params

def Setup_Parser_method(tc):
    count = tc
    while count<= tc:
        if count == tc:
            data = pd.read_csv("..//Data//Test1_Setup.csv")
            NAME = data.NAME[count]
            MAC = data.MAC[count]
            Dev_IP =  data.IP[count]
            TYPE = data.TYPE[count]
            MODEL = data.MODEL[count]
            NODE_USER = data.USER_NAME[count]
            #ETH1_IP = data.ETH1_IP[count]
            setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
            return setup_values
        count = count+1
                    
class Test51_Update_Verify_IPaddress(object):
    def init(self):
        self.object = object
    
    def test51_Update_Verify_IPaddress(self, req_ip, tc):
        try:
            val = Setup_Parser_method(tc)
            NAME = val[0]
            Dev_MAC_Address = val[1]
            NMS_Device_IP = val[2]
            Type = val[3]
            Model = val[4]
            Node_User= val[5]
            #Eth1_ip = val[6]
            api_data = Input_Data_Parser(tc)
            api_data[0]
            API_Name = api_data[0]
            Method = api_data[1]
            Content_Type = api_data[2]
            Arg1 = api_data[3]
            Exp_st_code= api_data[4]
            Precondition = api_data[5]
            #mip0_ip = api_data[6]
            Interface_Name = api_data[6]
            Network_Name = api_data[7]
            API_Server_IP= api_data[8]
            required_ip = req_ip
            url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
            print url
            Data_value = '{'+'"ipAddress"'+':'+" "+'"'+ str(required_ip)+'"'+" " +'}'
            print Data_value 
            #Dev_IP= Eth1_ip 
	    Dev_IP = required_ip	
            verify_api_param = 'ipAddress'
            print verify_api_param
            #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
            current_time = time.localtime()
            s= CommonOperations()
            tc_strt = datetime.datetime.now()            
            """here instead of interfacename we have modified the common 
            operation and instead sending the ipAddress"""
            #tc, Method, url, Content_Type, Data_value, Exp_st_code, Precondition, Supported_protocol, Data_value, Interface_Name, Network_Name, API_Server_IP) 
            p=s.commonOperations(tc, Method, url, Content_Type, Data_value,\
            Arg1, Exp_st_code, required_ip, verify_api_param,\
            Interface_Name, Network_Name, API_Server_IP)
            op = json.dumps(p,ensure_ascii=True)
            op1 = json.loads(op)
            print op
            mip0_ip = Dev_IP    
            if re.search('"rebootRequired":Yes',op, flags=re.IGNORECASE):
                rebootAPI="reboot"
                RebootMethod = "POST"
                url = "http://"+API_Server_IP+":8080"+"/NMS/"+\
                rebootAPI+"?networkName="+Network_Name+"&macAddress="+\
                Dev_MAC_Address
                print "Rebooting Node, please wait till node gets Up"
                s= CommonOperations()
                tc_strt = datetime.datetime.now()
                p=s.commonOperations(tc, RebootMethod, url, Content_Type,\
                    Data_value, Arg1, Exp_st_code, "test", verify_api_param,\
                    Interface_Name, Network_Name, API_Server_IP) 
                op = json.dumps(p, ensure_ascii=True)
                op1 = json.loads(op)
                  
                if op1.find('"Node rebooted  successfully"'):
                    print "Node Rebooting Now, please wait while node reboots..."
                devobj = dev_function()
                reboot_Node = devobj.check_node_status(Dev_IP)
                print reboot_Node
                if reboot_Node=="Success":
                    print "Node Rebooted successfully..."
                Verification_reqd = True    
            else:    
                if op.find("rebootRequired== null"):
                    current_time = time.localtime()    
                    filename = str(time.strftime('%Y_%m_%d_%H_%M', current_time))
                    nms_filename = "Test39_Update_Verify_IPaddress_NMS_Output"+filename
                    nms_filepath = "..//Output//"+nms_filename+'.csv'
                    nms_log = open(nms_filepath, 'w')
                    nms_log.write(op)
                    nms_log.close()
                    print "NMS output saved in logfile"
                    print "                           \n"                
                    
            """ Verification of Device Data """
            print "Device Data Verification Starts Now.."
            dev_Logfilename = "Test39_Update_Verify_IPaddress_Device_Output"\
            +str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
            dev_filepath = "..//Output//"+dev_Logfilename
            dev_mip_ip = Get_mipIP(Dev_IP,Node_User)
            #if dev_mip_ip.lower() == "fail" :
                #print "\n Couldn't get mip ip from device"
                #test_status = "Fail"
                #return test_status
            if str(dev_mip_ip.lower()) == str(required_ip):
                print "Success, ip address On NMS & Device Matched.."
                print "-----------------Test39_Update_Verify_IPaddress Test case is over-----"
                test_status = "Pass"
                addOutput(dev_filepath,op, str(dev_mip_ip), tc, test_status)
                return test_status

            else:
                print "Fail, ip address from NMS and Device are not matching"
                test_status = "Fail"
                addOutput(dev_filepath,op, str(dev_mip_ip), tc,test_status )
                return test_status
        except Exception as e:
            print(e)
