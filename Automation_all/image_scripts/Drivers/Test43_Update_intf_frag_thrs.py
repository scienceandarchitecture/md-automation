import csv
from Common_lib  import *
from dev_function import *
import re
import datetime
import json
import time
from dev_function import *
from iwdev_function import *
import re
from requests.packages import urllib3
import time
import time
import unittest

#from Common_lib  import CommonOperations
from Common_lib import *
from dev_function import *

from iwdev_function import *
import pandas as pd


#from CommonOperations import *
def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'a+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+NMS_Param])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return


#         
#         block_str = dev_obj.read_meshapp(Node_User,dev_filepath,NMS_Device_IP)
#         sub_para = find_between(block_str, Interface2_Name, "security" )
#         frag_thr   = re.search(r'(frag_th)=([1-9]{1,4})',sub_para,re.I)
#         Fragment_threshold = frag_thr.group(2)
#         print " Fragment threshold of eth0 is :",Fragment_threshold
#         return Fragment_threshold 
    
    
    

def Input_Data_Parser(tc):
    #df = pd.read_csv(".//..//API//getInterface.csv")
    df = pd.read_csv("..//API//updateGeneralConfiguration.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    updateCountryCode = df.updateCountryCode[tc]
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]
    API_fragThrs = df.fragThrs[tc]
    



    test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, updateCountryCode, Interface_Name, Network_Name, API_Server_IP, API_fragThrs]
    return test_params


def Setup_Parser_method(tc):
        count = 0
        while count<= tc:
            if count == tc:
                data = pd.read_csv(".//..//Data//Test1_Setup.csv")
                #data = pd.read_csv("..//Setup//Test1_Setup.csv")
                NAME = "NAME"+str()
                NAME = data.NAME[count]
                MAC = data.MAC[count]
                Dev_IP =  data.IP[count]
                TYPE = data.TYPE[count]
                MODEL = data.MODEL[count]
                NODE_USER = data.USER_NAME[count]
                setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
                return setup_values
            count = count+1
              
class Test43_Update_intf_frag_thrs(object):
    def init(self):
        self.object = object

    def test43_Update_intf_frag_thrs(self, intf, tc):
	
        #try:
            val = Setup_Parser_method(tc)
            #NAME, MAC, IP, TYPE, MODEL,NODE_USER
            NAME = val[0]
            Dev_MAC_Address = val[1]
            NMS_Device_IP = val[2]
            Type = val[3]
            Model = val[4]
            Node_User= val[5]
            
            
            
            
            print "Getting API values..."
            api_data = Input_Data_Parser(tc)
            api_data[0]
            API_Name = api_data[0]
            Method = api_data[1]
            Content_Type = api_data[2]
            Arg1 = api_data[3]
            Exp_st_code= api_data[4]
            Precondition = api_data[5]
            Supported_protocol= api_data[6]
            #Interface_Name = api_data[7]
            Interface_Name = intf
            Network_Name = api_data[8]
            API_Server_IP= api_data[9]
            #Interface2_Name = api_data[13]
            Interface2_Name = intf
            Fragment_Thrs= api_data[10]
            ''' Is this Fragment_Thrs correct , pl check the rts thrs file also'''
            
            url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?interfaceName="+Interface2_Name+"&networkName="+Network_Name+"&macAddress="+str(Dev_MAC_Address)
            print url
            Data_value = '{'+'"fragThreshold"'+':'+" "+ str(Fragment_Thrs)+" " +'}'
            # Data_value = '{'+ '"interfaceName"'+':'+'"'+Interface_Name+'"'+','+'"essid"'+':'+'"'+Arg1+'"'+'}'
            print Data_value             
            Dev_IP= NMS_Device_IP 
            print "Test Details: "
            verify_api_param = 'fragThreshold'
            #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
            s= CommonOperations()
            tc_strt = datetime.datetime.now()            
            p=s.commonOperations(tc, Method, url, Content_Type, Arg1, Exp_st_code, Precondition, Supported_protocol, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
            op = json.dumps(p,ensure_ascii=True)
            print "op is ",op
            ##op1 = json.loads(op)
            #NMS_API_ESSID = op1["essid"]
            #fragThreshold_NMS = op1["fragThreshold"]
            print "Fragment threshold value set through API is : ", str(Fragment_Thrs) 
    
            """ Verification of Device Data """
            print "Device Data Verification Starts Now.."
            current_time = time.localtime()
            dev_Logfilename = "Test_Result"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
            dev_filepath = "..//Output//"+dev_Logfilename
            dev_obj = dev_function()
            Node_User = "root"
            
            dev_obj = dev_function()
            print "Verification of Parameters in meshapp.conf"
            
            Frag_thrs_dev = dev_obj.get_intf_frag_thr(Node_User,dev_filepath, Dev_IP, intf)
	    
            print Frag_thrs_dev
            
            if str(Fragment_Thrs) == str(Frag_thrs_dev):
                print "Fragthreshold_NMS & Device Value matched successfully.."
                test_status1 = "Pass"
                addOutput(dev_filepath,op, Frag_thrs_dev, tc, test_status1) 
                #return test_status
            else:
                print "Fragthreshold_NMS & Device Value matched"
                test_status1 = "Fail"
                addOutput(dev_filepath,op, Frag_thrs_dev, tc, test_status1)
                #return test_status
            print "Veriying device values from Iwdev "
            iw_obj=iwdev_function()
            dev_frag_thrs_iw =iw_obj.get_intf_frag_thr_from_iwconfig(Node_User,dev_filepath, Dev_IP, Interface2_Name)
            print dev_frag_thrs_iw
                       
            
            if str(Fragment_Thrs) == str(frag_thrs_iw):
                print "Success, Fragment threshold  On NMS and the value from IW command Matched.."
                test_status2 = 1
                addOutput(dev_filepath,op, frag_thrs_iw, tc, test_status2)
                                    
            else:
                print "Failed, Fragment threshold On NMS & value from IW command not Matched.."
                test_status2 = 0
                addOutput(dev_filepath,op, frag_thrs_iw, tc, test_status2)
                
            
            if test_status1 >0 &  test_status2 >0:
                print "Success, Fragment threshold On NMS, IW command & Device Matched.."
                print "testcase passed"
            else:
                print "testcase failed"
                print "Failed, Fragment threshold On NMS & Device not Matched.."
                
            test_status = ['test_status1','test_status2'] 
            return test_status  
       
            
            
            
      
