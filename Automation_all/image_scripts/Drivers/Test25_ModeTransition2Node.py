import os
import sys
import re
from subprocess import check_output
import time
import telnetlib, copy
import time
import csv
from dev_function import *
from Log import *
from Win_Sta import *
from Setup_Verify import *
import pandas as pd


def Get_highest_mac(mac1,mac2):
	mac1 = mac1.replace("-","")
	mac1 = mac1.replace(":","")
	mac1 = mac1.lower()
	mac2 = mac2.replace("-","")
	mac2 = mac2.replace(":","")
	mac2 = mac2.lower()
	if mac1 > mac2 :
		return mac1
	elif mac2 > mac1 :
		return mac2
	else:
		return "same"

def Get_nodes():
	df = pd.read_csv("..\\Config\\Two_node_Transition_Config.csv")
	my_list = df["NAME"].tolist()
	return my_list

def Config_Parser_method(logFH):
	
	devobj = dev_function()	
	df = pd.read_csv("..\\Config\\Two_node_Transition_Config.csv")
	with open("..\\Config\\Two_node_Transition_Config.csv", 'r') as f:
		file_read = csv.DictReader(f)	 
		data = []
		for row in file_read:
			data.append(row)
		no_row = len(data)   
		count = 0
		while count < no_row :
			node_info = {}
			node_info['mesh_id'] = df.MESH_ID[count]
			node_info['essid'] = df.ESSID[count]
			node_info['channel'] = df.CHANNEL[count]
			node_info['name'] = df.NAME[count]
			node_interface = df.INTERFACE[count]
			node_ip = devobj.get_from_setup(node_info['name'],"IP")
			node_user = devobj.get_from_setup(node_info['name'],"USER_NAME")
			print"\n Please wait!! configuring node" + (str(count+1)) + " this may take time\n\n"
			
			config = devobj.config_node_via_telnet(logFH,node_interface,node_ip,node_info,node_user)
			if count != 0:
				#time.sleep(120)
				time.sleep(20)
			count = count + 1
		
		return 

class Two_node_Transition(object):
	
	def init(self):
		self.object = object
	
	#setupfname = "Two_node_Transition_Setup"
	configfname = "Two_node_Transition_Config"
	logobj = Log()
	Nodes = Get_nodes()
	devobj = dev_function()
	setup = Setup_Verify()
	winstaob = Win_Sta()
	node_status = ''
	node1ip = devobj.get_from_setup(Nodes[0],"IP")
	node2ip = devobj.get_from_setup(Nodes[1],"IP")
	node1_mac = devobj.get_from_setup(Nodes[0], "MAC")
	node2_mac = devobj.get_from_setup(Nodes[1], "MAC")	
	user1 = devobj.get_from_setup(Nodes[0], "USER_NAME")
	user2 = devobj.get_from_setup(Nodes[1], "USER_NAME")
	node1ssid = devobj.get_from_config(configfname,Nodes[0], "ESSID")
	node1auth = devobj.get_from_config(configfname,Nodes[0], "AUTHENTICATION")
	node2ssid = devobj.get_from_config(configfname,Nodes[1], "ESSID")
	node2auth = devobj.get_from_config(configfname,Nodes[1], "AUTHENTICATION")
	scriptname = os.path.basename(__file__)
	logFH = logobj.CreateLog(scriptname)
	logobj.AppendLog(logFH,("\nExecuting Test script "+scriptname))
	logobj.AppendLog(logFH,"\nVerifying if Setup is ready... ")
	
	
	for i in Nodes :
		ipaddr = devobj.get_from_setup(i,"IP")
		check_setup = setup.setup_check(ipaddr)   
		if check_setup == "Ping Successfull" :
			logobj.AppendLog(logFH,(" Node "+ i +" is up"))
				
		else:
			logobj.AppendLog(logFH,(" Node "+ i +" is Down"))
			exit
			
	logobj.AppendLog(logFH,("\n Configuring the Nodes"))
	Config_Parser_method(logFH)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node2 failed to bootup within expected time")
		exit()
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Sorry the Node1 failed to bootup within expected time")
		exit()		
	logobj.AppendLog(logFH,("\nVerifying nodes mode!!\n"))
	cmd = 'cat /proc/net/meshap/mesh/adhoc_mode'
	dev_node_mode = ''
	for i in Nodes :
		user = devobj.get_from_setup(i, "USER_NAME")
		ipaddr = devobj.get_from_setup(i,"IP")
		logobj.AppendLog(logFH,("Trying to telnet to" + ipaddr)) 
		dev_node_mode = devobj.execute_via_telnet(ipaddr,cmd)
		time.sleep(2)
		if re.search("FFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("node" +(i)+" is FFR\n"))
			
		else :
			logobj.AppendLog(logFH,("node" +(i)+" is not FFR\n"))
			exit	
			
	
	##############################################################
	
	''' Executing Step2 '''
	logobj.AppendLog(logFH,("Making eth0 of Node2 down \n"))
	cmd = 'ifconfig eth0 down'	
	dev_node_mode = devobj.execute_via_telnet(node2ip,cmd)
	time.sleep(5)
	node1_mode = devobj.Get_node_mode(node1ip)
	if node1_mode != "FFR" :
		print "\nStep2 failed,Node1 is not FFR!!"
		exit()
	cmd = "netsh interface set interface name=\"Ethernet\" admin=disabled"
	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)	
	node2_mode = devobj.Get_node_mode(node2ip)
	if node2_mode != "FFN" :
		print "\nStep2 failed,Node2 is not FFN!!"
		exit()
	
	logobj.AppendLog(logFH,(" Verifying the Mesh network formation!!\n"))
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	dev_node_mode = ''
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE) :
		logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node1 is root and Node2 is relay"))	
	else :
		logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
		exit()
 	
	cmd = "ping -c 10 " + node1ip
	out = devobj.execute_via_telnet(node2ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"

	else:
		print "ping from node 2 to node 1 Fail!!"
		exit()
	logobj.AppendLog(logFH,("\n Step2 is verified successfully!"))
	
	
	###########################################################
	''' Executing Step3 '''
	''' Connecting to wlan2 of node 1 and downing eth0 and eth1 interface'''
	#cmd = "netsh interface set interface name=\"Ethernet\" admin=disabled"
 	#winstaob.Exec_Win_Cmd(cmd)
 	#time.sleep(5)
 	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 and eth1 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	cmd = "ifconfig eth1 down"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	''' Connecting to wlan2 of node 2 and downing eth0 and eth1 interface'''
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(10)
 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 and eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	cmd = "ifconfig eth1 down"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	logobj.AppendLog(logFH,"Please wait!!Mesh formation is in progress!!\n")
	time.sleep(5)
	highestmac = Get_highest_mac(node1_mac,node2_mac)
	if highestmac == "same" :
		print "\nError in setup file!!"
		exit()
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = ''
	if(highestmac == node1_mac):	
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFR" :
			print "\nNode1 is not LFR,despite being highestmac!!"
			exit()
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFN" :
			print "\nNode2 is not LFN!!"
			exit()	
		dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
		if re.search(node2_mac,dev_node_mode,re.IGNORECASE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node1 is LFR and Node2 is LFN"))
			logobj.AppendLog(logFH,("\n Step3 is verified successfully!"))
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()
	else :	
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFR" :
			print "\nNode2 is not LFR,despite being highestmac!!"
			exit()
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFN" :
			print "\nNode1 is not LFN!!"
			exit()	
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		if re.search(node1_mac,dev_node_mode,re.IGNORECASE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node2 is LFR and Node1 is LFN"))
			logobj.AppendLog(logFH,("\n Step3 is verified successfully!"))
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()
	
	cmd = "ping -c 10 " + node1ip
	out = ''
	out = devobj.execute_via_telnet(node2ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"

	else:
		print "ping from node 2 to node 1 Fail!!"
		exit()	
	
	################################
	###Executing Step4
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	cmd = "ifconfig eth1 up"
	print "\n\n enabling eth1 of node1"
	print "\n\n Node1 is rebooting"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	##enabling eth0 and eth1 interface of Node2
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	cmd = "ifconfig eth1 up"
	print "\n\n enabling eth1 of node2"
	print "\n\n Node2 is rebooting"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	cmd = "netsh interface set interface name=\"Ethernet\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step4 failed!!Sorry the Node1 failed to bootup within expected time")
		exit()
	#winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	#time.sleep(5)
	cmd = "ifconfig eth0 up"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	print "\n\n enabling eth0 of node1"
	print "\n Node1 is rebooting!!!please wait."
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step4 failed!!Sorry the Node2 failed to bootup within expected time")
		exit()
	cmd = "ifconfig eth0 up"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	print "\n\n enabling eth0 of node2"
	print "\n Node2 is rebooting!!!please wait."
	cmd = 'cat /proc/net/meshap/mesh/adhoc_mode'
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step4 failed!!Sorry the Node1 failed to bootup within expected time")
		exit()
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step4 failed!!Sorry the Node2 failed to bootup within expected time")
		exit()
	for i in Nodes :
		ipaddr = devobj.get_from_setup(i,"IP")
		logobj.AppendLog(logFH,("Trying to telnet to" + ipaddr))
		dev_node_mode = '' 
		dev_node_mode = devobj.execute_via_telnet(ipaddr,cmd)
		time.sleep(2)
		if re.search("FFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
			logobj.AppendLog(logFH,("node" +(i)+" is FFR\n"))
			
		else :
			logobj.AppendLog(logFH,("node" +(i)+" is not FFR\n"))
			logobj.AppendLog(logFH,("Step 4 failed!!\n"))
			exit()
	
	logobj.AppendLog(logFH,("Step 4 Pass!!\n"))
		
	##############################################################
	#Executing Step5
	##disabling eth0 and eth1 interface of Node2
	cmd = "netsh interface set interface name=\"Ethernet\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	cmd = "ifconfig eth1 down"
	print "\n\n disabling eth0 and eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	cmd = "ifconfig eth0 down"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	logobj.AppendLog(logFH,"Please wait!!Mesh formation is in progress!!\n")
	time.sleep(5)
	node2_mode = devobj.Get_node_mode(node2ip)
	if node2_mode != "FFN" :
		print "\nStep5 Failed!!,Node2 is not a FFN"
		exit()
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)	
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = ''
	dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
	if re.search(node2_mac,dev_node_mode,re.IGNORECASE) :
		logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node1 is FFR and Node2 is FFN"))
	else :
		logobj.AppendLog(logFH,("\n Step5 Failed!!Mesh network is not formed"))
		exit()
	cmd = "ping -c 10 " + node2ip
	out = ''
	out = devobj.execute_via_telnet(node1ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"
		logobj.AppendLog(logFH,("\n Step5 is verified successfully!"))
	else:
		print "ping from node 2 to node 1 Fail!!"
		exit()
	##############################################################
	#Executing Step6
	''' Connecting to wlan2 of node 1 and downing eth0 interface'''
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	cmd = "ifconfig eth1 up"
	print "\n\n enabling eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	print "\n Node2 is rebooting!!"
 	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(90)
	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	print "\nchecking Node2 status!!"		
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step6 failed!!Sorry the Node2 failed to bootup within expected time")
		exit()
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	logobj.AppendLog(logFH,"Please wait!!Mesh formation is in progress!!\n")
	time.sleep(5)
	highestmac = Get_highest_mac(node1_mac,node2_mac)
	if highestmac == "same" :
		print "\nError in setup file!!"
		exit()		
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = ''
	if(highestmac == node1_mac):
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(10)	
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFRS" :
			print "\nNode1 is not LFRS,despite being highestmac!!"
			exit()
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFN" :
			print "\nStep6 Failed!!Node2 is not LFN!!"
			exit()	
		dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
		if re.search(node2_mac,dev_node_mode,re.IGNORECASE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node1 is LFRS and Node2 is LFN"))
			logobj.AppendLog(logFH,("\n Step6 is verified successfully!"))
		else :
			logobj.AppendLog(logFH,("\n Step6 Failed!!Mesh network is not formed!!"))
			exit()
	else :
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFRS" :
			print "\nStep6 Failed,Node2 is not LFRS,despite being highestmac!!"
			exit()
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFN" :
			print "\nStep6 Failed,Node1 is not LFN!!"
			exit()	
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		if re.search(node1_mac,dev_node_mode,re.IGNORECASE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node2 is LFR and Node1 is LFN"))
			logobj.AppendLog(logFH,("\n Step6 is verified successfully!"))
		else :
			logobj.AppendLog(logFH,("\n Step6 Failed,Mesh network is not formed!!"))
			exit()
	
	cmd = "ping -c 10 " + node1ip
	out = ''
	out = devobj.execute_via_telnet(node2ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"

	else:
		print "Step6 Failed,ping from node 2 to node 1 Fail!!"
		exit()
	####################final##########################################
	#Executing Step7
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	highestmac = ''
	dev_node_mode = ''
	highestmac = Get_highest_mac(node1_mac,node2_mac)
	if(highestmac == node1_mac):
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(10)
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFRS" :
			print "\nStep7 Failed!!Node1 is not LFRS"
			exit()
		else:
			cmd = "ifconfig eth1 down"
			print "\n\n disabling eth1 of node1"
			out =''
			out = devobj.execute_via_telnet(node1ip, cmd)
			logobj.AppendLog(logFH,out)	
			logobj.AppendLog(logFH,"\nPlease wait Mesh formation is in progress!!")
			time.sleep(15)
			node1_mode = devobj.Get_node_mode(node1ip)
			if node1_mode != "LFN" :
				print "\nStep7 Failed!!Node1 is not LFN,after disconnection eth1"
				exit()
			cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 			winstaob.Exec_Win_Cmd(cmd)
 	 		time.sleep(5)
	 	 	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 		 	winstaob.Exec_Win_Cmd(cmd)
 	 		time.sleep(5)
 	 		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		 	time.sleep(10)
		 	node2_mode = ''
		 	node2_mode = devobj.Get_node_mode(node2ip)
			if node2_mode != "LFRS" :
				print "\nStep7 Failed!!Node2 is not LFRS after disconnecting eth1 of Node1"
				exit()
			dev_node_mode = ''	
			cmd = "cat /proc/net/meshap/mesh/table"
			dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
			if re.search(node1_mac,dev_node_mode,re.IGNORECASE) :
				logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node2 is LFRS and Node1 is LFN"))
				logobj.AppendLog(logFH,("\n Step7 is verified successfully!"))
			else :
				logobj.AppendLog(logFH,("\n Step7 Failed,Mesh network is not formed!!"))
				exit()	
	else:
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(10)
		node2_mode = ''
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFRS" :
			print "\nStep7 Failed!!Node2 is not LFRS"
			exit()
		cmd = "ifconfig eth1 down"
		print "\n\n disabling eth1 of node2"
		out =''
		out = devobj.execute_via_telnet(node2ip, cmd)
		logobj.AppendLog(logFH,out)	
		logobj.AppendLog(logFH,"\nPlease wait Mesh formation is in progress!!")
		time.sleep(15)
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFN" :
			print "\nStep7 Failed!!Node2 is not LFN,after disconnection eth1"
			exit()
		cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 		winstaob.Exec_Win_Cmd(cmd)
 	 	time.sleep(5)
 	 	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 		winstaob.Exec_Win_Cmd(cmd)
 	 	time.sleep(5)
 		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
 		time.sleep(10)
 		node1_mode = ''
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFRS" :
			print "\nStep7 Failed!!Node1 is not LFRS after disconnecting eth1 of Node2"
			exit()
		dev_node_mode = ''
		cmd = "cat /proc/net/meshap/mesh/table"	
		dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
		if re.search(node2_mac,dev_node_mode,re.IGNORECASE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node1 is LFRS and Node2 is LFN"))
			logobj.AppendLog(logFH,("\n Step7 is verified successfully!"))
		else :
			logobj.AppendLog(logFH,("\n Step7 Failed,Mesh network is not formed!!"))
			exit()
	cmd = "ping -c 10 " + node1ip
	out = ''
	out = devobj.execute_via_telnet(node2ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"

	else:
		print "Step7 Failed,ping from node 2 to node 1 Fail!!"
		exit()	
	
	###############################################################
	#Executing Step8
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)		
	##enabling eth0 and eth1 interface of Node2
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	cmd = "ifconfig eth0 up"
	print "\n\n enabling eth0 and eth1 of node2"
	print "\n\n node2 is rebooting"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(90)
	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	print "\nchecking Node1 status!!"		
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step8 failed!!Sorry the Node1 failed to bootup within expected time")
		exit()
	cmd = "ifconfig eth0 down"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	time.sleep(5)
	logobj.AppendLog(logFH,out)
	node1_mode = devobj.Get_node_mode(node1ip)
	logobj.AppendLog(logFH,"\nPlease wait Mesh formation is in progress!!")
	time.sleep(15)
	if node1_mode != "FFN" :
		print "\nStep8 Failed!!Node1 is not FFN!!"
		exit()
 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)	
	node2_mode = devobj.Get_node_mode(node2ip)
	if node2_mode != "FFR" :
		print "\nStep8 Failed!!Node2 is not FFR!!"
		exit()		
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = ''	
	dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
	if re.search(node1_mac,dev_node_mode,re.IGNORECASE) :
		logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node2 is FFR and Node1 is FFN"))
		logobj.AppendLog(logFH,("\n Step8 is verified successfully!"))
	else :
		logobj.AppendLog(logFH,("\n Step8 Failed!!Mesh network is not formed!!"))
		exit()
	
	cmd = "ping -c 10 " + node1ip
	out = ''
	out = devobj.execute_via_telnet(node2ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"

	else:
		print "Step8 Failed,ping from node 2 to node 1 Fail!!"
		exit()
	
	#############################################
	#Executing Step9
	''' Connecting to wlan2 of node 1 and downing eth0 and eth1 interface'''
 	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
	time.sleep(10)
	node_status = ''
	node_status = devobj.check_node_status(node1ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step9 failed!!Sorry the Node1 failed to bootup within expected time")
		exit()
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 and eth1 of node1"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	cmd = "ifconfig eth1 down"
	out =''
	out = devobj.execute_via_telnet(node1ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	''' Connecting to wlan2 of node 2 and downing eth0 and eth1 interface'''
	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=disabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(5)
 	cmd = "netsh interface set interface name=\"Wi-Fi\" admin=enabled"
 	winstaob.Exec_Win_Cmd(cmd)
 	time.sleep(10)
 	winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
	time.sleep(10)
	node_status = ''
	node_status = devobj.check_node_status(node2ip)
	if node_status == "Timeout":
		logobj.AppendLog(logFH,"Step9 failed!!Sorry the Node1 failed to bootup within expected time")
		exit()
	cmd = "ifconfig eth0 down"
	print "\n\n disabling eth0 and eth1 of node2"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(5)
	cmd = "ifconfig eth1 down"
	out =''
	out = devobj.execute_via_telnet(node2ip, cmd)
	logobj.AppendLog(logFH,out)
	time.sleep(15)
	logobj.AppendLog(logFH,"Please wait!!Mesh formation is in progress!!\n")
	time.sleep(5)
	highestmac = Get_highest_mac(node1_mac,node2_mac)
	if highestmac == "same" :
		print "\nError in setup file!!"
		exit()
	cmd = "cat /proc/net/meshap/mesh/table"
	dev_node_mode = ''
	if(highestmac == node1_mac):
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(10)	
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFR" :
			print "\nNode1 is not LFR,despite being highestmac!!"
			exit()
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(10)	
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFN" :
			print "\nNode2 is not LFN!!"
			exit()	
		dev_node_mode = devobj.execute_via_telnet(node1ip, cmd)
		if re.search(node2_mac,dev_node_mode,re.IGNORECASE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node1 is LFR and Node2 is LFN"))
			logobj.AppendLog(logFH,("\n Step9 is verified successfully!"))
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()
	else :
		winstaob.Connect_Via_WinSTA(logFH,node2ssid,node2auth)
		time.sleep(10)	
		node2_mode = devobj.Get_node_mode(node2ip)
		if node2_mode != "LFR" :
			print "\nNode2 is not LFR,despite being highestmac!!"
			exit()
		winstaob.Connect_Via_WinSTA(logFH,node1ssid,node1auth)
		time.sleep(10)
		node1_mode = devobj.Get_node_mode(node1ip)
		if node1_mode != "LFN" :
			print "\nNode1 is not LFN!!"
			exit()	
		dev_node_mode = devobj.execute_via_telnet(node2ip, cmd)
		if re.search(node1_mac,dev_node_mode,re.IGNORECASE) :
			logobj.AppendLog(logFH,("\n Mesh is formed successfully!!" + "\n Node2 is LFR and Node1 is LFN"))
			logobj.AppendLog(logFH,("\n Step9 is verified successfully!"))
		else :
			logobj.AppendLog(logFH,("\n Mesh network is not formed!!"))
			exit()
	
	cmd = "ping -c 10 " + node1ip
	out = ''
	out = devobj.execute_via_telnet(node2ip, cmd)	
	out = devobj.check_positive_ping(out)
	if re.search("Success",out,re.IGNORECASE|re.MULTILINE):
		print "ping from node 2 to node 1 Success!!"

	else:
		print "ping from node 2 to node 1 Fail!!"
		exit()	
	#####################################################	
	print "\n\n Test case execution complete!!!"	
	exit()
