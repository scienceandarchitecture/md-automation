import json
import datetime
import time
import pandas as pd
import csv
import Common_lib
from dev_function import *
from Common_lib  import CommonOperations
from iwdev_function import *
import re

def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'w+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+str(NMS_Param)])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return

def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//updateGeneralConfiguration.csv")
    #d_excel("config_data.csv")
    tc = 0
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]
    #tx_pwr = df.Tx_Power[tc]
    test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code,\
        Precondition, Interface_Name, Network_Name, API_Server_IP]
                 
    return test_params


def Setup_Parser_method(tc):
    count = 0
    while count<= tc:
        if count == tc:
            data = pd.read_csv("..//Data//Test1_Setup.csv")
            NAME = data.NAME[count]
            MAC = data.MAC[count]
            Dev_IP =  data.IP[count]
            TYPE = data.TYPE[count]
            MODEL = data.MODEL[count]
            NODE_USER = data.USER_NAME[count]
            #ETH1_IP = data.ETH1_IP[count]
            setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
            return setup_values
        count = count+1
        
class Test50_Update_intf_txpower(object):
    def init(self):
        self.object = object
    
    def test50_Update_intf_txpower(self,intf1,txpower,tc):
        try:
            val = Setup_Parser_method(tc)
            NAME = val[0]
            Dev_MAC_Address = val[1]
            NMS_Device_IP = val[2]
            Type = val[3]
            Model = val[4]
            Node_User= val[5]
            Eth1_ip = val[2]
            api_data = Input_Data_Parser(tc)
            api_data[0]
            API_Name = api_data[0]
            Method = api_data[1]
            Content_Type = api_data[2]
            Arg1 = api_data[3]
            Exp_st_code= api_data[4]
            Precondition = api_data[5]
            Interface_Name = intf1
            Network_Name = api_data[7]
            API_Server_IP= api_data[8]
            update_txpwr = txpower
	    print update_txpwr
	    print "Debug",update_txpwr
	    #update_txpwr = api_data[10]
            url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+\
            "?interfaceName="+Interface_Name+"&networkName="+\
            Network_Name+"&macAddress="+Dev_MAC_Address
            print url
            Data_value = '{'+'"txPower"'+':'+" "+ str(update_txpwr)+" " +'}'
            print Data_value 
            Dev_IP= Eth1_ip 
            verify_api_param = 'txPower'
            print verify_api_param
            current_time = time.localtime()
           
            tc_strt = datetime.datetime.now() 
            """Here instead of interfacename we have modified the common 
            operation and instead sending the txpower"""
            #tc, Method, url, Content_Type, Data_value, Exp_st_code, Precondition, Supported_protocol, Data_value, Interface_Name, Network_Name, API_Server_IP) 

            s= CommonOperations()	
            p=s.commonOperations(tc, Method, url, Content_Type, Data_value, Exp_st_code, Precondition, verify_api_param, Data_value, Interface_Name, Network_Name, API_Server_IP) 
            op = json.dumps(p,ensure_ascii=True)
            op1 = json.loads(op)
            print op
            if re.search('"rebootRequired":Yes',op, flags=re.IGNORECASE):
                rebootAPI="reboot"
                RebootMethod = "POST"
                url = "http://"+API_Server_IP+":8080"+"/NMS/"+\
                rebootAPI+"?networkName="+Network_Name+"&macAddress="+\
                Dev_MAC_Address
                print "Rebooting Node, please wait till node gets Up"
                s= CommonOperations()
                tc_strt = datetime.datetime.now()
                p=s.commonOperations(tc, RebootMethod, url, Content_Type,\
                    Data_value, Arg1, Exp_st_code, "test", verify_api_param,\
                    Interface_Name, Network_Name, API_Server_IP) 
                op = json.dumps(p, ensure_ascii=True)
                op1 = json.loads(op)
                if op1.find('"Node rebooted  successfully"'):
                    print "Node Rebooting Now, please wait while node reboots..."
                devobj = dev_function()
                reboot_Node = devobj.check_node_status(Dev_IP)
                print reboot_Node
                if reboot_Node=="Success":
                    print "Node Rebooted successfully..."
                Verification_reqd = True    
            else:    
                if op.find("rebootRequired== null"):
                    current_time = time.localtime()    
                    filename = str(time.strftime('%Y_%m_%d_%H_%M', current_time))
                    nms_filename = "Test50_Update_intf_txpower_NMS_Output"+filename
                    nms_filepath = "..//Output//"+nms_filename+'.csv'
                    nms_log = open(nms_filepath, 'w')
                    nms_log.write(op)
                    nms_log.close()
                    print "NMS output saved in logfile"
                    print "                           \n"                    
            """ Verification of Device Data """
            print "Device Data Verification using Meshapp.conf starts Now.."
	    current_time = time.localtime()
            dev_Logfilename = "Test51_get_intf_tx_power_Device_output" \
            +str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
            dev_filepath = "..//Output//"+dev_Logfilename
            telnet_user = "root"
            dev_obj = dev_function()
            print "Verification of Parameters in meshapp.conf"
            tx_Power_dev = dev_obj.get_intf_tx_pow(Node_User, dev_filepath, NMS_Device_IP, Interface_Name)
            print "Debug", tx_Power_dev
            """ Comparing the values from NMS and device output
            If result matc_numhed , declaring Test case as pass /Fail"""
            if int(update_txpwr) ==  int(tx_Power_dev):
                print "Tx_power Matched successfully"
                test_status = "Pass"
                addOutput(dev_filepath, op, tx_Power_dev, tc, test_status)
                return test_status
            else:
                print "Tx_power doesn't match..."
                test_status = "Fail"
                
                addOutput(dev_filepath, op, tx_Power_dev, tc, test_status)
                return test_status
             




            dev_Logfilename = "Test50_Update_intf_txpower_Device_Output"\
            +str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
            dev_filepath = "..//Output//"+dev_Logfilename
	    print "Verification of Parameters in meshapp.conf"
            iwdev_obj = iwdev_function()
            tx_pwr_iwdev=iwdev_obj.read_iwlist(Node_User,dev_filepath, Dev_IP, Interface_Name)
            print tx_pwr_iwdev
            

	    print "Device Verification using IW command starts now..."

	    if update_txpwr == 100:

            	if tx_pwr_iwdev == 20:
                	print "Value of Tx power from NMS & device matched successfully..."
                	test_status = "Pass"
               		addOutput(dev_filepath,op, update_txrate, tc, test_status)
	                return test_status

            if update_txpwr == 50:
		if tx_pwr_iwdev == 10:
	            print "Value of Tx power from NMS & device matched successfully..."
        	    test_status = "Pass"
        	    addOutput(dev_filepath,op, update_txrate, tc, test_status)
        	    return test_status
            else:
                print "Value Tx power not matched......"
                test_status = "Fail"
                addOutput(dev_filepath,op, update_txrate, tc, test_status)
                return test_status
        except Exception as e:
            print(e)
