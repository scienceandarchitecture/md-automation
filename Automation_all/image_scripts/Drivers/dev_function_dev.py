from telnetlib import *
from nodentirals import *
import telnetlib
import time
import serial
import copy
import time
import serial
import copy
import re
import csv
from subprocess import check_output
from subprocess import CalledProcessError
#from twisted.spread.jelly import None_atom
from pip._vendor.requests.packages.urllib3.util.timeout import current_time
import os
from telnetlib import Telnet
#from Log import *
#from pip._vendor.requests.packages.urllib3.util.timeout import current_time
#from Log import *
import sys
import telnetlib
from socket import socket

def reboot_node(self, ipaddress):
		tn = telnetlib.Telnet(ipaddress)
		cmd_val = tn.read_until(':')
		if "root" in cmd_val :
			print "Login is not required.."
		else:
			time.sleep(2)
			#print "please login to continue"
			tn.write("root"+'\n')
			# print "login successfully"
			console_val = tn.read_until(':')


			if 'Password:' in console_val:
				tn.write('root'+"\n")
				log_val = tn.read_until(':')
				print log_val
		print "Login successfull..."
		cmd_essid = "reboot"
		tn.write(cmd_essid+"\n")  
		tn.write("exit\n")
		return "success"
	
	
	
def Get_node_mode(self,ipaddr):
	cmd = 'cat /proc/net/meshap/mesh/adhoc'
	dev_node_mode = ''
	dev_node_mode =self.execute_via_telnet(ipaddr,cmd)
	time.sleep(10)
	if re.search("FFN",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("FFN")
	elif re.search("FFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("FFR")
	elif re.search("LFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("LFR")
	elif re.search("LFN",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("LFN")
	elif re.search("LFRS",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("LFRS") 
	return ("Node mode cannot be determined")



# Machine_Name = regutil.win32api.GetComputerName()
# print "Name of the System on which this script is executing : ",Machine_Name


class dev_functions(object):	
	
	def init(self):
		object = self.object
		return
		
	
	
		
	
	def get_ping_status(self,user, dev_logfile, ipaddress):
		tn = telnetlib.Telnet(ipaddress)
		#tn1 = tn.open(host, port=23, timeout=None)
		readval = tn.read_until("login:  ")
		#tn.read_until("login: ")
		tn.write(user+"\r\n")
		#tn.write("ls\n")
		#tn.write("ifconfig mip0\n")
		cmd = "ping"+ ipaddress
		tn.write(cmd+"\n")		
		tn.write("exit\n")
		ping_res = tn.read_all()
		#ping_result = tn.read_until("bytes from")
		log_file  = dev_logfile+str(time.strftime('%Y_%m_%d_%H_%M', current_time))
		dev_log = open(log_file, 'w')
		dev_log.write(ping_res)
		dev_log.write('\n')
		dev_log.close()
		
		
		
	#		 logfile = open("E:\\mesh_dynamics\\Automation_framework\\src\\Output\\log_console.txt", 'a')
	#		 logfile.write(vals)
	#		 logfile.close()
		return ping_res
	
	
	
	def get_from_config(self,fname,rowname,column_name):
		filename = "..\\Config\\" + fname + ".csv"
		with open(filename, 'r') as f:
			file_read = csv.DictReader(f)
			flag = 0 
			res =''   
			for row in file_read:
				if (str(row['NAME'])) == (str(rowname)) :
					flag = 1
					res = row[column_name]
					break
			
			#f.close()	
			if flag :
				return res	
			else :
				return "Sorry Node name not found"

	def Get_node_mode(self,ipaddr):
		cmd = 'cat /proc/net/meshap/mesh/adhoc'
		dev_node_mode = ''
		dev_node_mode =self.execute_via_telnet(ipaddr,cmd)
		cmd = 'cat /proc/net/meshap/mesh/adhoc'
		print "\n in dev_function",dev_node_mode
		time.sleep(4)
		if re.search(r"\w FFN \w",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		    return 'FFN'
		elif re.search(r"\w FFR \w",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		    return 'FFR'
		elif re.search(r"\w LFR \w",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		    return 'LFR'
		elif re.search(r"\w LFN \w",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		    return 'LFN'
# elif re.search(r"\w LFRS \w",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
#     print  "LFRS"
    
		return "node mode cannot be determined"
	
	def get_from_setup(self,rowname,column_name):
		with open("..//Setup//Test1_Setup.csv", 'r') as f:
			file_read = csv.DictReader(f)
			flag = 0     
			for row in file_read:
				#print row
				if row['NAME'] == rowname :
					flag = 1
					res = row[column_name]
					return res
	
	
	def check_node_status(self,ipadrr):
		toolbar_width = 41
		flag = cptr = 0 #cptr is cursor pointer
		cmd = "ping -n 5 " + ipadrr
		sys.stdout.write("[%s]" % (" " * toolbar_width))
		sys.stdout.flush()
		sys.stdout.write("\b" * (toolbar_width+1))
		while cptr < (toolbar_width - 1) :
			pingstr = None
			try:
				pingstr = check_output(cmd , shell=True)
				returncode = 0
			except CalledProcessError as errorno:
				output = errorno.output
				returncode = errorno.returncode
				if returncode :
					temppingstr = "Request timed out" * 3
			
			if pingstr is not None :	
				temppingstr=copy.copy(pingstr)
				#temppingstr = temppingstr.replace('\n','')
				#temppingstr=temppingstr.replace('.','')
			ReqtimeOut = re.search('Request timed out',temppingstr,re.I)
			Destunreach = re.search('Destination host unreachable',temppingstr,re.I)
			if ReqtimeOut:
				cptr = cptr + 4
				sys.stdout.write("====")
				sys.stdout.flush()
				continue
			elif Destunreach:
				cptr = cptr + 4
				sys.stdout.write("====")
				sys.stdout.flush()
				continue
			else :
				flag = 1
				sys.stdout.write(("=" * (toolbar_width - cptr)))
				sys.stdout.flush()
				sys.stdout.write("\n")
				break
			
				
		if flag:
			return "Success"
		
		return "Timeout"
			
	def check_positive_ping(self,pingstr):
		temppingstr=copy.copy(pingstr)
		temppingstr=temppingstr.replace('\n','')
		temppingstr=temppingstr.replace('.','')
		ReqtimeOut = re.match('(.*)(Request timed out){5,}(.*)',temppingstr,re.IGNORECASE|re.MULTILINE) 
		if ReqtimeOut:
			return "Fail" 
		DestUnreach = temppingstr.count("Destination host unreachable")
		if DestUnreach >=5 :
			return "Fail" 
		strlines = pingstr.splitlines()
		line = ''
		for line in strlines:
			if "Packets" in line:
				line = line.replace("Packets:","")
				line = line.replace(" ","")
				line = re.sub('\(.*\),$',"",line)
		return ("Success"+line)
	
	def execute_via_telnet(self, ipaddr, cmd):
		user= "root"
		count = 0
		user= "root"
		tn = telnetlib.Telnet(ipaddr)
		cmd_val = tn.read_until(':')
		if "root" in cmd_val :
		    print "Login is not required.."
		else:
		    time.sleep(2)
		    #print "please login to continue"
		    tn.write("root"+'\n')
		    # print "login successfully"
		    console_val = tn.read_until(':')
		
		
		    if 'Password:' in console_val:
		        tn.write("\n")
		        log_val = tn.read_until('OpenWrt')
		        #print log_val
		
		
		print "Login successfull..."  
		
		print cmd
		time.sleep(5)
		tn.write(cmd+'\n')
		time.sleep(5)
		valconsole = tn.read_until('INDEX')
		#print valconsole
		return valconsole
		
		
		
		
	def config_node_via_telnet(self,interface,hostip,infele):
		#logobj = Log()
		tn = telnetlib.Telnet(hostip,int(23), int(20))
		cmd_val = tn.read_until('OpenWrt')
		out = ''
		print cmd_val
		if "root" in cmd_val :
		    print "Login is not required.."
		else:
		     time.sleep(2)
		     #print "please login to continue"
		     tn.write("root"+'\n')
		     # print "login successfully"
		     console_val = tn.read_until(':')


		     if 'Password:' in console_val:
				tn.write('root'+"\n")
				log_val = tn.read_until(':')
				print log_val
		print "Login successfull..."
		
		
 	
		#logobj.AppendLog(logFH,"telnet connected successfully")
		#logobj.AppendLog(logFH,out)
		time.sleep(2)
		tn.write("ifconfig mip0 \r \n")
		out = ''
		while out.find('#') == -1:
			out = tn.read_very_eager()
		print out				
		matobj = re.search('mip0\s*Link\s*encap:Ethernet\s*HWaddr\s*(([0-9A-F]{2}[:-]){5}([0-9A-F]{2}))',out,re.I)
		mac_add = matobj.group(1)
		meshid = infele['mesh_id']
 		for k,v in infele.iteritems():
 			v = str(v)
 			if k.lower() == 'mesh_id':
 				cmd = "alconfset mesh_id "+ v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
  				#logobj.AppendLog(logFH,out)
 				time.sleep(2)
 			if k.lower() == 'name1223':#change name1223 to name later
 				cmd = "alconfset name "+ v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				time.sleep(2)
 				#logobj.AppendLog(logFH,out)
 			if k.lower() == 'essid':
 				cmd = "alconfset essid "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				#logobj.AppendLog(logFH,out)		
 			if k.lower() == 'channel':
 				cmd = "alconfset channel "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				#logobj.AppendLog(logFH,out)
 			if k.lower() == 'rts':
 				cmd = "alconfset rts "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				#logobj.AppendLog(logFH,out)
 			if k.lower() == 'frag':
				cmd = "alconfset frag "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				#ogobj.AppendLog(logFH,out)		
 				time.sleep(2)
 			if k.lower() == 'hidessid':
 				cmd = "alconfset hidessid "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				#logobj.AppendLog(logFH,out)
 				time.sleep(2)
 			if k.lower() == 'subtype':
 				cmd = "alconfset subtype "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				#logobj.AppendLog(logFH,out)
 				time.sleep(2)
 			if k.lower() == 'beacint':
 				cmd = "alconfset beacint "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				#logobj.AppendLog(logFH,out)
 				time.sleep(2)
 			if k.lower() == 'dca':
 				cmd = "alconfset dca "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				#logobj.AppendLog(logFH,out)
 			if k.lower() == 'preamble':
 				cmd = "alconfset preamble "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				#logobj.AppendLog(logFH,out)
 			if k.lower() == 'acktime':
				cmd = "alconfset acktime "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
  				#logobj.AppendLog(logFH,out)
 				time.sleep(2)	
 
 		cmd1 = 'patcher -k ' + meshid + ' ' + mac_add
 		print "#########",cmd1,"#############"
 		tn.write(cmd1 + "\r\n")
 		time.sleep(2)
 		out = ''
 		while out.find('#') == -1:
 		 	out = tn.read_very_eager()
 		#logobj.AppendLog(logFH,out)
 		cmd = 'reboot\n'
 		tn.write(cmd + "\r\n")
 		time.sleep(2)
 		return


	def Disable_interface(self,hostip,interface):
		out = ''
		count = 0
		#logobj = Log()
		tn = telnetlib.Telnet(hostip)
		cmd_val = tn.read_until('OpenWrt')
		out = ''
		if "root" in cmd_val :
		    print "Login is not required.."
		else:
		     time.sleep(2)
		     #print "please login to continue"
		     tn.write("root"+'\n')
		     # print "login successfully"
		     console_val = tn.read_until(':')


		     if 'Password:' in console_val:
				tn.write(''+"\n")
				log_val = tn.read_until(':')
				print log_val
		print "Login successfull..."
		
		#if type(interface) is str :	
		cmd = "ifconfig " + interface + " down "
# 		while count < 2 :
		tn.write(cmd + "\n")
		#count = count + 1
		time.sleep(20)
		print "Command to down interface done"
# 		out = tn.read_until('OpenWrt')
# 		#while out.find('#') == -1:
# 		out = tn.read_very_eager()
# 		print out
		#logobj.AppendLog(logFH,out)	
		tn.close()	
		return "Pass"
# 			elif type(interface) is list :
# 				for i in interface :
# 					count = 0
# 					cmd = "ifconfig " + i + " down"
# 					while count < 2 :
# 						tn.write(cmd + "\n")
# 						count = count + 1
# 						time.sleep(0.5)
# 				out = ''	
# 				while out.find('#') == -1:
# 					out = tn.read_very_eager()
# 				#logobj.AppendLog(logFH,out)		
# 				return "Pass"		
# 		elif type(hostip) is list :
# 			for ip in hostip :
# 				try :
# 					tn = telnetlib.Telnet(ip,int(23),int(10))
# 				except socket.timeout:
# 					print "Sorry " + ip + " is unreachable!!"
# 					return "Fail"
# 				cmd_val = tn.read_until('OpenWrt')
# 				if "root" in cmd_val :
# 				    print "Login is not required.."
# 				else:
# 				    print "please login to continue"
# 				    tn.write("root"+'\n')
# 				    print "login successful"
# 				
# 				if type(interface) is str :
# 					cmd = "ifconfig " + interface + " down"
# 					count = 0
# 					while count < 2 :
# 						tn.write(cmd + "\n")
# 						count = count + 1
# 						time.sleep(0.5)
# 					out = ''	
# 					while out.find('#') == -1:
# 						out = tn.read_very_eager()
# 					#logobj.AppendLog(logFH,out)	
# 				elif type(interface) is list :
# 					for i in interface :
# 						count = 0
# 						cmd = "ifconfig " + i + " down"
# 						while count < 2 :
# 							tn.write(cmd + "\n")
# 							count = count + 1
# 							time.sleep(0.5)
# 						out = ''	
# 						while out.find('#') == -1:
# 							out = tn.read_very_eager()
# 							#logobj.AppendLog(logFH,out)	
# 				tn.close()
# 			return "Pass"	
# 		return "Fail"
	
# 	def Enable_interface(self, logFH, hostip, interface):
# 		out = ''
# 		logobj = Log()
# 		
# 		if type(hostip) is str :
# 			try :
# 				tn = telnetlib.Telnet(hostip, int(23), int(10))
# 			except socket.timeout:
# 				logobj.AppendLog(logFH, "Sorry " + hostip + \
# 									  "is unreachable!!")
# 				return "Fail"
# 			tn.read_until("OpenWrt")
# 			if type(interface) is str :	
# 				cmd = "ifconfig " + interface + " up"
# 				tn.write(cmd + "\n")
# 				time.sleep(1)
# 				out = ''	
# 				while out.find('#') == -1:
# 					out = tn.read_very_eager()
# 				logobj.AppendLog(logFH, out)
# 				tn.close()	
# 				return "Pass"
# 			elif type(interface) is list :
# 				for i in interface :
# 					cmd = "ifconfig " + i + " up"
# 					tn.write(cmd + "\n")
# 					time.sleep(1)
# 				out = ''	
# 				while out.find('#') == -1:
# 					out = tn.read_very_eager()
# 				logobj.AppendLog(logFH, out)	
# 				return "Pass"		
# 		elif type(hostip) is list :
# 			for ip in hostip :
# 				try :
# 					tn = telnetlib.Telnet(ip, int(23), int(10))
# 				except socket.timeout:
# 					logobj.AppendLog(logFH, "Sorry " + ip + \
# 									  "is unreachable!!")
# 					return "Fail"
# 				tn.read_until("OpenWrt")
# 				if type(interface) is str :
# 					cmd = "ifconfig " + interface + " up"
# 					tn.write(cmd + "\n")
# 					time.sleep(1)
# 					out = ''	
# 					while out.find('#') == -1:
# 						out = tn.read_very_eager()
# 					logobj.AppendLog(logFH, out)
# 				elif type(interface) is list :
# 					for i in interface :
# 						cmd = "ifconfig " + i + " down"
# 						tn.write(cmd + "\n")
# 						time.sleep(1)
# 						out = ''	
# 					while out.find('#') == -1:
# 						out = tn.read_very_eager()
# 					logobj.AppendLog(logFH, out)
# 				tn.close()
# 			return "Pass"	
# 		return "Fail"	
# 	
