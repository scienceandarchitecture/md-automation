import json
import datetime
import time
import pandas as pd
import csv


from Common_lib  import *
from dev_function import *
import re



def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//putInterface.csv")
    #d_excel("config_data.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]    
    test_params = [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, Interface_Name, Network_Name, API_Server_IP]
                 
    return test_params

def read_vlan_mesh(Node_User,dev_filepath, NMS_Device_IP, Vlan_Name):
    print Vlan_Name
    dev_obj= dev_function()
    block_str=dev_obj.read_meshapp(Node_User,dev_filepath, NMS_Device_IP)
    sub_para = find_between_including(block_str, Vlan_Name, "none" )
    return sub_para
    

def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'w+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+str(NMS_Param)])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return

def find_between_including( s, first, last ):
    try:
        start = s.index( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return "Error"


def Config_Tests_Parser(tc):
    df = pd.read_csv("..//API//configTests.csv")
    API_name = df.API_name[tc]
    InterfaceName = df.InterfaceName[tc]
    API_Parameter = df.API_Parameter[tc]   
    Verify_Parameter = df.Verify_Parameter[tc] 
    a = df.a[tc]
    b = df.b[tc]
    g = df.g[tc]
    n = df.n[tc]
    ac = df.ac[tc]
    bg = df.bg[tc]
    bgn = df.bgn[tc]
    an = df.an[tc]
    anac = df.anac[tc]  
    test_params = [API_name, InterfaceName, API_Parameter, Verify_Parameter, a, b, g, n, ac, bg, bgn, an, anac]                 
    return test_params

def Setup_Parser_method(tc):
    count = 0
    while count <= tc:
        if count == tc:
            data = pd.read_csv("..//Data//Test1_Setup.csv")
            NAME = data.NAME[count]
            MAC = data.MAC[count]
            Dev_IP =  data.IP[count]
            TYPE = data.TYPE[count]
            MODEL = data.MODEL[count]
            NODE_USER = data.USER_NAME[count]
            setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
            return setup_values
        count = count+1



"""
The preferred parent is updated using nms api and
then re-checking the value on Device after restarting the node """              
          
class Test24_Medium_SubType(object):
    def init(self):
        self.object = object
    def test24_Medium_SubType(self,tc,intf1):
        
        val = Setup_Parser_method(tc)
        NAME = val[0]
        Dev_MAC_Address = val[1]
        NMS_Device_IP = val[2]
        Type = val[3]
        Model = val[4]
        Node_User= val[5]
        Test_Result = {}
        api_data = Input_Data_Parser(tc)
        api_data[0]
        API_Name = api_data[0]
        Method = api_data[1]
        Content_Type = api_data[2]
        Arg1 = api_data[3]
        Exp_st_code= api_data[4]
        Precondition = api_data[5]
        #Interface_Name = api_data[6]
        Interface_Name = intf1
        Network_Name =  api_data[7]
        API_Server_IP= api_data[8]
        #API_name                a    b    g    n    ac    bg    bgn    an     anac    x
        with open("..\\\..\\src\\API\\configTests.csv", 'r') as f:
            file_read = csv.DictReader(f)     
            data = []
            for row in file_read:
                data.append(row)
            no_row = len(data)   
            tc = 0
            while tc <= no_row:
                
                print "Config Check for Test number :",tc+1
                print "======================================"
                api_data = Config_Tests_Parser(tc)
                api_data[0]
                API_name = api_data[0]
                InterfaceName = api_data[1]
                API_Parameter = api_data[2]
                Verify_Parameter = api_data[3]
                print "", API_Parameter, Verify_Parameter
                a_exp= api_data[4]
                b_exp = api_data[5]
                g_exp = api_data[6]
                n_exp =  api_data[7]
                ac_exp = api_data[8]
                bg_exp =    api_data[9]
                bgn_exp =    api_data[10]
                an_exp =    api_data[11]
                anac_exp =    api_data[12]
                #print API_name, InterfaceName, API_Parameter, Verify_Parameter, a, b, g, n, ac, bg, bgn, an, anac, x
                if API_Parameter in ['essid', 'mediumType']:
                    Verify_Parameter = str(Verify_Parameter) 
                else:
                    Verify_Parameter = int(Verify_Parameter)
                
                
                a_val = 1
                b_val =2
                g_val = 3
                n_val = 10
                ac_val = 11
                bg_val = 4
                bgn_val = 12
                an_val = 13
                anac_val = 14
    
                ms_exp = {'a':a_exp, 'b':b_exp, 'g':g_exp, 'n':n_exp, 'ac':ac_exp, 'bg':bg_exp, 'bgn':bgn_exp, 'an':an_exp, 'anac':anac_exp}
                ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                
                medium_subtype= [a_val, b_val, g_val,n_val,ac_val,bg_val,bgn_val, an_val,anac_val]
                #medium_subtype= [a_val, b_val]
    #             for value in [a, b, g,n,ac,bg,bgn, an,anac,x]:
    #                 if value != "No":
    #                     medium_subtype.append(value)
    #                 else:
    #                     print "Sub-Protocol not supported for this interface..:"
    #            
                url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_name+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
                print url
                
                for value in medium_subtype:
                    Verification_reqd= True
                    print "Interface Used In The Test Is : ", InterfaceName
                    print "MediumSubtype Value to be checked : ", value
                    print "Parameter used for modification : ", API_Parameter
                   
                    Data_value = '{'+'"interfaceName"'+':'+" "+'"'+ str(InterfaceName)+'"'+',' +'"mediumSubType"'+':'+ str(value)+','+ '"' +API_Parameter+'"'+':'+" "+str(Verify_Parameter)+'}'
                #Data_value = '{'+'"preferedParent"'+':'+" "+ str(preferedParent)+" " +'}'
                    print Data_value 
                    Dev_IP= NMS_Device_IP 
                    verify_api_param = 'networkId'
                    #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
                    current_time = time.localtime()
                    s= CommonOperations()
                    tc_strt = datetime.datetime.now()
                    #tc, Method, url, Content_Type, Data_value, Exp_st_code, Precondition, Supported_protocol, Data_value, Interface_Name, Network_Name, API_Server_IP)
                    p=s.commonOperations(tc, Method, url, Content_Type, Data_value, Arg1, Exp_st_code, "test", verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
                    op = json.dumps(p,ensure_ascii=True)
                    op1 = json.loads(op)
                                 
                    if re.findall('interfacedata updated successfully',op, flags=re.IGNORECASE):
                        print "Request processed successfully....",
                        Verification_reqd = True
                         
                        for k,v in ms_value.iteritems():
                            if v == value:
                                print k,"and interface ",InterfaceName
                     
                    elif re.findall('NO Changes in interfacedata', op, flags=re.IGNORECASE):
                        print "No updates requested for medium subtype",
                        Verification_reqd = False
                        for k,v in ms_value.iteritems():
                            if v == value:
                                print k,"and interface ",InterfaceName
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tr
                                Test_Result["Result"]="Not Run"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter, "Nothing to update"]
    #                             Test_output.append(Test_Result)
                                print Test_Result
                                 
                     
                    elif re.findall('interfacedata sent To Device,Not received response', op, flags=re.IGNORECASE):
                        print "No Response received from Device...",
                        Verification_reqd = False
                        for k,v in ms_value.iteritems():
                            if v == value:
                                print k,"and interface ",InterfaceName
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tr
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter,"No Response From Device"]
                                print Test_Result
    #                             Test_output.append(Test_Result)
    #                             count_tr=count_tr+1
    #                 
                    elif re.findall('mediumSubType is not valid value', op, flags=re.IGNORECASE):
                        print "This MediumSubType is not supported for this interface: MediumSubtype ",InterfaceName, value
                        Verification_reqd = False
                        for k,v in ms_value.iteritems():
                            if v == value:
                                print k
                         
                                exp_valtr = ms_exp[k]
                                print "medium subtype value is supported ?",exp_valtr
                                if exp_valtr=="No":
                                    print "Expected Output is matching with the Actual Output..."
                                    Test_Result["Result"]="Pass"
                                else:
                                    print "Expected output is not matching with the Actual Output..."
                                    Test_Result["Result"]="Failed"
                                     
                        Test_Result["Testcase"]=tc
                        Test_Result["values"] = [value, InterfaceName, API_Parameter, "Negative Testcase"]
                        print Test_Result
                        #Test_output.append(Test_Result)
                         
    #                 if re.search(r'\b'+'"rebootRequired":NO', op, flags=re.IGNORECASE):
    #                     print "Reboot is not required.."
    #                  
                    elif re.search('"rebootRequired":Yes',op, flags=re.IGNORECASE):
                        rebootAPI="reboot"
                        RebootMethod = "POST"
                        url = "http://"+API_Server_IP+":8080"+"/NMS/"+rebootAPI+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
                        print "Rebooting Node, please wait till node gets Up"
                        s= CommonOperations()
                        tc_strt = datetime.datetime.now()
                        p=s.commonOperations(tc, RebootMethod, url, Content_Type, Data_value, Arg1, Exp_st_code, "test", verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
                        op = json.dumps(p, ensure_ascii=True)
                        op1 = json.loads(op)
                          
                        if op1.find('"Node rebooted  successfully"'):
                            print "Node Rebooting Now, please wait while node reboots..."
                        devobj = dev_function()
                        reboot_Node = devobj.check_node_status(NMS_Device_IP)
                        print reboot_Node
                        if reboot_Node=="Success":
                            print "Node Rebooted successfully..."
                        Verification_reqd = True
                         
                    if Verification_reqd == True:    
                        dev_Logfilename = "Test24_Medium_SubType"\
                        +str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
                        dev_filepath = "..//Output//"+dev_Logfilename
                        dev_obj = dev_function()
                        block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
                        sub_para = find_between(block_str, InterfaceName, "security" )
                         
                        print "Checking for the set values for interface: ",InterfaceName
                        if API_Parameter == "txRate":
                            txrate   = re.search(r'(txrate)=(\d+)',sub_para,re.I)
                            dev_txrate = txrate.group(2) 
                            print "txRate on device is : ",dev_txrate
                            if str(dev_txrate) == str(Verify_Parameter):
                                print "Txrate updated successfully, Values on NMS and Device matched"
                            else:
                                print "Txrate updated not updated as per API..."
                                 
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            meduiumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",meduiumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':4, 'ac':5, 'bg':6, 'bgn':7, 'an':8, 'anac':9, 'x':10 }
                             
                            dev_medium_value = ms_value[meduiumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                 
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tr
                                Test_Result["Result"]="Passed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
                                print Test_Result
                                #Test_output.append(Test_Result)
                                 
                            else:
                                print "Value failed to update on Device"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tr
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                                print Test_Result
                                #Test_output.append(Test_Result)
                                 
                        elif API_Parameter == "txpower":
                            txpower   = re.search(r'(txpower)=(\d+)',sub_para,re.I)
                            dev_txpower = txpower.group(2) 
                            print "Device TxPower value is", dev_txpower
                             
                            if str(dev_txpower) == str(Verify_Parameter):
                                print "txpower updated successfully, Values on NMS and Device matched"
                            else:
                                print "TxPower is not updated as expected"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                                print Test_Result
                                #Test_output.append(Test_Result)
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            mediumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",mediumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                            dev_medium_value = ms_value[mediumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                print "_____________________________________________________________________"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
    #                             Test_output.append(Test_Result)
    #                             Test_output.append("\n")
    #                             
                            else:
                                print "MeduiumSubtype failed to update"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
                                
                        if API_Parameter == "ackTimeout":
                            ack_timeout   = re.search(r'(ack_timeout)=(\d+)',sub_para,re.I)
                            dev_ackTimeout= ack_timeout.group(2) 
                            print "Device ack_timeout value is", dev_txpower
                            
                            if str(ack_timeout) == str(dev_ackTimeout):
                                print "ack_timeout updated successfully, Values on NMS and Device matched"
                            else:
                                print "ack_timeout is not updated as expected"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                                print Test_Result
                                #Test_output.append(Test_Result)
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            mediumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",mediumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                            dev_medium_value = ms_value[mediumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                print "_____________________________________________________________________"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
    #                             Test_output.append(Test_Result)
    #                             Test_output.append("\n")
    #                             
                            else:
                                print "MeduiumSubtype failed to update"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                                
                        
                        if API_Parameter == "fragThreshold":
                            frag_th   = re.search(r'(frag_th)=(\d+)',sub_para,re.I)
                            dev_fragThreshold= frag_th.group(2) 
                            print "Device ack_timeout value is", dev_fragThreshold
                            
                            if str(Verify_Parameter) == str(dev_fragThreshold):
                                print "fragThreshold updated successfully, Values on NMS and Device matched"
                            else:
                                print "frag_th is not updated as expected"
                                Test_Result["Testcase"]=tc
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [dev_fragThreshold, InterfaceName, API_Parameter]                            
                                print Test_Result
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            mediumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",mediumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                            dev_medium_value = ms_value[mediumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                print "_____________________________________________________________________"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
    #                             Test_output.append(Test_Result)
    #                             Test_output.append("\n")
    #                             
                            else:
                                print "MeduiumSubtype failed to update"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                            
                                
                                
                                
                                
                        if API_Parameter == "rtsThreshold":
                            rts_th   = re.search(r'(rts_th)=(\d+)',sub_para,re.I)
                            dev_rtsThreshold=rts_th.group(2) 
                            print "Device rtsThreshold value is", dev_rtsThreshold
                             
                            if str(Verify_Parameter) == str(dev_rtsThreshold):
                                print "rts_th updated successfully, Values on NMS and Device matched"
                            else:
                                print "rts_th is not updated as expected"
                                Test_Result["Testcase"]=tc
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [dev_rtsThreshold, InterfaceName, API_Parameter]                            
                                print Test_Result
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            mediumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",mediumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                            dev_medium_value = ms_value[mediumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                print "_____________________________________________________________________"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
    #                             Test_output.append(Test_Result)
    #                             Test_output.append("\n")
    #                             
                            else:
                                print "MeduiumSubtype failed to update"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                                
                                
                        if API_Parameter == "essid":
                            essid   = re.search(r'(essid)=(\w+)',sub_para,re.I)
                            dev_essid=essid.group(2) 
                            print "Device essid value is", dev_essid
                             
                            if str(Verify_Parameter) == str(dev_essid):
                                print "essid updated successfully, Values on NMS and Device matched"
                            else:
                                print "essid is not updated as expected"
                                Test_Result["Testcase"]=tc
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [dev_essid, InterfaceName, API_Parameter]                            
                                print Test_Result
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            mediumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",mediumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                            dev_medium_value = ms_value[mediumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                print "_____________________________________________________________________"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
    #                             Test_output.append(Test_Result)
    #                             Test_output.append("\n")
    #                             
                            else:
                                print "MeduiumSubtype failed to update"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                            
             
             
                        #print "To verify hide_ssid "                   
                        if API_Parameter == "essidHidden":
                            hide_ssid   = re.search(r'(hide_ssid)=(\d+)',sub_para,re.I)
                            dev_hide_ssid=hide_ssid.group(2) 
                            print "Device hide_ssid value is", dev_hide_ssid
                             
                            if int(Verify_Parameter) == int(dev_hide_ssid):
                                print "Hide essid updated successfully, Values on NMS and Device matched"
                            else:
                                print "hidden_essid is not updated as expected"
                                Test_Result["Testcase"]=tc
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [dev_hide_ssid, InterfaceName, API_Parameter]                            
                                print Test_Result
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            mediumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",mediumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                            dev_medium_value = ms_value[mediumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                print "_____________________________________________________________________"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
    #                             Test_output.append(Test_Result)
    #                             Test_output.append("\n")
    #                             
                            else:
                                print "MeduiumSubtype failed to update"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                            
            
            
            
                             
                        if API_Parameter == "service":
                            service   = re.search(r'(service)=(\d+)',sub_para,re.I)
                            dev_service=service.group(2) 
                            print "Service value is", dev_service
                             
                            if int(Verify_Parameter) == int(dev_service):
                                print "service updated successfully, Values on NMS and Device matched"
                            else:
                                print "service is not updated as expected"
                                Test_Result["Testcase"]=tc
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [dev_service, InterfaceName, API_Parameter]                            
                                print Test_Result        
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            mediumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",mediumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                            dev_medium_value = ms_value[mediumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                print "_____________________________________________________________________"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
    #                             Test_output.append(Test_Result)
    #                             Test_output.append("\n")
    #                             
                            else:
                                print "MeduiumSubtype failed to update"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                            
                        
                        if API_Parameter == "mediumType":
                            mt = re.search(r'(medium_type)=(\w+)',sub_para,re.I)
                            dev_medium_type=mt.group(2) 
                            print "medium_type value is", dev_medium_type
                              
                            if str(Verify_Parameter) == str(dev_medium_type):
                                print "service updated successfully, Values on NMS and Device matched"
                            else:
                                print "mediumType is not updated as expected"
                                Test_Result["Testcase"]=tc
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [dev_medium_type, InterfaceName, API_Parameter]                            
                                print Test_Result        
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            mediumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",mediumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                            dev_medium_value = ms_value[mediumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                print "_____________________________________________________________________"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
    #                             Test_output.append(Test_Result)
    #                             Test_output.append("\n")
    #                             
                            else:
                                print "MeduiumSubtype failed to update"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
    
                        
                        
                        
                        
                        if API_Parameter == "preamble_type":
                            preamble_type   = re.search(r'(preamble_type)=(\w+)',sub_para,re.I)
                            dev_preamble_type=preamble_type.group(2) 
                            print "Preamble is:", dev_preamble_type
                             
                            if str(Verify_Parameter) == str(dev_preamble_type):
                                print "preamble_type updated successfully, Values on NMS and Device matched"
                            else:
                                print "preamble_type is not updated as expected"
                                Test_Result["Testcase"]=tc
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [dev_preamble_type, InterfaceName, API_Parameter]                            
                                print Test_Result             
                            ms_value=0
                            medi_s_type   = re.search(r'(medium_sub_type)=(\w+)',sub_para,re.I)
                            mediumSubtype = medi_s_type.group(2) 
                            print "meduiumSubtype is ",mediumSubtype
                            ms_value = {'a':1, 'b':2, 'g':3, 'n':10, 'ac':11, 'bg':4, 'bgn':12, 'an':13, 'anac':14}
                            dev_medium_value = ms_value[mediumSubtype]
                            print "medium subtype from device is",dev_medium_value
                            if  value ==  dev_medium_value:
                                print "meduiumSubtype updated successfully, Values on NMS and Device matched"
                                print "_____________________________________________________________________"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Pass"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]
    #                             Test_output.append(Test_Result)
    #                             Test_output.append("\n")
    #                             
                            else:
                                print "MeduiumSubtype failed to update"
                                Test_Result["Testcase"]=tc
                                #Test_Result["Test_Iter"]= count_tp
                                Test_Result["Result"]="Failed"
                                Test_Result["values"] = [value, InterfaceName, API_Parameter]                            
                            
                                
                
    
                    else:
                        #count_tp = count_tp +1
                        print "_____________________________________________________________________"
                        
                    
                tc = tc+1        
                print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"    
                
        
