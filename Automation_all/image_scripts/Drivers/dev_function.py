import telnetlib
import time
import sys
import copy
import re
import csv
from pip._vendor.requests.packages.urllib3.util.timeout import current_time
from telnetlib import Telnet
from Log import *
from subprocess import check_output, CalledProcessError


def get_ht_rxSTBC(self,  Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
        dev_obj= dev_function()
        block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
        sub_para = find_between(block_str, Interface2_Name, "lsig_txop" )
        rxSTBC  = re.search(r'(rx_stbc)=(\w+)',sub_para,re.I)
        rxSTBC=rxSTBC.group(2)
        print " ht rx_stbc is :",rxSTBC
        return rxSTBC 
    
    
def get_ht_txSTBC(self,  Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
        dev_obj= dev_function()
        block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
        sub_para = find_between(block_str, Interface2_Name, "lsig_txop" )
        tx_stbc  = re.search(r'(tx_stbc)=(\w+)',sub_para,re.I)
        tx_stbc=tx_stbc.group(2)
        print " ht tx_stbc is :",tx_stbc
        return tx_stbc 
		




def get_hostname_from_config_system(user,  dev_logfile, ipaddress):
	tn = telnetlib.Telnet(ipaddress)
	cmd_val = tn.read_until(':')
	if "root" in cmd_val :
		print "Login is not required.."
	else:
		time.sleep(2)
		#print "please login to continue"
		tn.write("root"+'\n')
		# print "login successfully"
		console_val = tn.read_until(':')
		
		if 'Password:' in console_val:
			tn.write(''+"\n")
			log_val = tn.read_until(':')
			print log_val
 
	print "Login successfull..."
	cmd1 = "cat /etc/config/system"
	tn.write(cmd1+"\n")
	tn.write("exit\n")
	dev_ip_add = tn.read_all()
	tn.close()
	logfile = open(dev_logfile, 'a')
	logfile.write(dev_ip_add)
	logfile.close()
	return dev_ip_add




# def get_hostname_from_config_system(self, user, dev_logfile, ipaddress):
# 	tn = telnetlib.Telnet(ipaddress)
# 	cmd_val = tn.read_until(':')
# 	if "root" in cmd_val :
# 		print "Login is not required.."
# 	else:
# 		time.sleep(2)
# 		#print "please login to continue"
# 		tn.write("root"+'\n')
# 		# print "login successfully"
# 		console_val = tn.read_until(':')
# 		
# 		if 'Password:' in console_val:
# 			tn.write('root'+"\n")
# 			log_val = tn.read_until(':')
# 			print log_val
# 	
# 		print "Login successfull..."
# 		cmd1 = "cat /etc/config/system"
# 		tn.write(cmd1+"\n")
# 		tn.write("exit\n")
# 		read_config = tn.read_all()
# 		tn.close()
# 		logfile = open(dev_logfile, 'a')
# 		logfile.write(read_config )
# 		logfile.close()
# 		return read_config 
	
	
	
''' Getting result of cat /etc/config/network for Extracting gateway'''
   

	
	
def get_ht_intolerant(self,  Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "lsig_txop" )
		ht_intolerant  = re.search(r'(intolerant)=(\w+)',sub_para,re.I)
		ht_intolerant=ht_intolerant.group(2)
		print " ht intolerant is :",ht_intolerant
		return ht_intolerant 
	
	
def get_ht_rxSTBC(self,  Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "lsig_txop" )
		ht_rxSTBC  = re.search(r'(rx_stbc)=(\w+)',sub_para,re.I)
		ht_rxSTBC=ht_rxSTBC.group(2)
		print " ht rx_stbc is :",ht_rxSTBC
		return ht_rxSTBC 
	
	
def get_ht_txSTBC(self,  Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "lsig_txop" )
		tx_stbc = re.search(r'(tx_stbc)=(\w+)',sub_para,re.I)
		tx_stbc=tx_stbc.group(2)
		print " ht tx_stbc is :",tx_stbc
		return tx_stbc 
	



def Get_node_mode(self,ipaddr):
	cmd = 'cat /proc/net/meshap/mesh/adhoc'
	dev_node_mode = ''
	dev_node_mode =self.execute_via_telnet(ipaddr,cmd)
	time.sleep(10)
	if re.search("FFN",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("FFN")
	elif re.search("FFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("FFR")
	elif re.search("LFR",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("LFR")
	elif re.search("LFN",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("LFN")
	elif re.search("LFRS",dev_node_mode,re.IGNORECASE|re.MULTILINE) :
		return ("LFRS") 
	return ("Node mode cannot be determined")



def find_between( s, first, last ):
	try:
		start = s.index( first ) + len( first )
		end = s.index( last, start )
		return s[start:end]
	except ValueError:
		return "Error"

"""# Machine_Name = regutil.win	32api.GetComputerName()# print 
"Name of the System on which this script is executing : ",Machine_Name"""
class dev_function(object):	



	def get_intf_frag_thr(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User,dev_filepath,NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		frag_thr   = re.search(r'(frag_th)=([1-9]{1,4})',sub_para,re.I)
		Fragment_threshold = frag_thr.group(2)
		print "for interface ",Interface2_Name
		print " Fragment threshold",Fragment_threshold
		return Fragment_threshold 

	def get_Gateway(self, user, dev_logfile, ipaddress, interfacename):	
		
		tn = telnetlib.Telnet(ipaddress)
		cmd_val = tn.read_until(':')
		if "root" in cmd_val :
			print "Login is not required.."
		else:
			time.sleep(2)
			#print "please login to continue"
			tn.write("root"+'\n')
			# print "login successfully"
			console_val = tn.read_until(':')
			
			if 'Password:' in console_val:
				tn.write("\n")
				log_val = tn.read_until(':')
				print log_val
	 
		print "Login successfull..."
		cmd1 = "cat /etc/config/network"
		interfacename = "mip0"
		#cmd1 = "ip route"
		#sed -n '/mip0/,/broadcast/p' cat /etc/config/network 
		cmd1  = "sed -n '/"+ str(interfacename)+"/,/broadcast/p'"+ " /etc/config/network"
		print cmd1
		tn.write(cmd1+"\n")
		tn.write("exit\n")
		time.sleep(5)
		dev_ip_add = tn.read_all()
		tn.close()
		
		block_str = dev_ip_add
		#sed -n '/mip0/,/broadcast/p' /etc/config/network
				
		
# 		
# 		sub_para = find_between(block_str, "gateway '", "'option broadcast" )
# 		print "filtered value is ",sub_para
		
#		 hide_ssid  = re.search(r'(hide_ssid)=(\d)',sub_para,re.I)
#		 HideSsid = hide_ssid.group(2)
#		 
		
		
		logfile = open(dev_logfile, 'a')
		logfile.write(block_str)
		logfile.close()
		return block_str


	def check_node_status(self, ipadrr):
		toolbar_width = 41
		flag = cptr = 0 #cptr is cursor pointer
		cmd = "ping -c 5 " + ipadrr
		sys.stdout.write("[%s]" % (" " * toolbar_width))
		sys.stdout.flush()
		sys.stdout.write("\b" * (toolbar_width+1))
		while cptr < (toolbar_width - 1) :
			pingstr = None
			try:
				pingstr = check_output(cmd , shell=True)
				returncode = 0
			except CalledProcessError as errorno:
				output = errorno.output
				returncode = errorno.returncode
				if returncode :
					temppingstr = "Request timed out"
			if pingstr is not None : 
				temppingstr=copy.copy(pingstr)
			ReqtimeOut = re.search('Request timed out',temppingstr,re.I)
			Destunreach = re.search('Destination host unreachable',temppingstr,re.I)
			if ReqtimeOut:
				cptr = cptr + 4
				sys.stdout.write("====")
				sys.stdout.flush()
				continue
			elif Destunreach:
				cptr = cptr + 4
				sys.stdout.write("====")
				sys.stdout.flush()
				continue
			else :
				flag = 1
				sys.stdout.write(("=" * (toolbar_width - cptr)))
				sys.stdout.flush()
				sys.stdout.write("\n")
				break
		if flag:
			return "Success"
		return "Timeout"
	
	def reboot_node(self, user, ipaddress):
		tn = telnetlib.Telnet(ipaddress)
		cmd_val = tn.read_until(':')
		if "root" in cmd_val :
			print "Login is not required.."
		else:
			time.sleep(2)
			#print "please login to continue"
			tn.write("root"+'\n')
			# print "login successfully"
			console_val = tn.read_until(':')


			if 'Password:' in console_val:
				tn.write(''+"\n")
				log_val = tn.read_until(':')
				print log_val
		print "Login successfull..."
		cmd_essid = "reboot"
		tn.write(cmd_essid+"\n")  
		tn.write("exit\n")
		return "success"
		
	
	
	
	def read_meshapp(self, user, dev_filepath, ipaddress):
		print ipaddress
		tn = telnetlib.Telnet(ipaddress)
		cmd_val = tn.read_until(':')
		if "root" in cmd_val :
			print "Login is not required.."
		else:
			 time.sleep(2)
			 #print "please login to continue"
			 tn.write("root"+'\r\n')
			 # print "login successfully"
			 time.sleep(4)
			 console_val = tn.read_until(':')
			 print console_val


			 if 'Password:' in console_val:
				tn.write(""+'\r\n')
				log_val = tn.read_until(':')
				print log_val
		print "Login successfull..."
		cmd_essid = "cat /etc/meshap.conf"
		tn.write(cmd_essid+"\n")  
		tn.write("exit\n")
		time.sleep(2)
		cmd_res = tn.read_all()
		logfile = open(dev_filepath, 'a')
		logfile.write(cmd_essid)
		logfile.close()
		return cmd_res

		
	def essid_verification(self, user, dev_filepath, ipaddress, verify_param, Interface_Name):
			tn = telnetlib.Telnet(ipaddress)
			cmd_val = tn.read_until(':')
			if "root" in cmd_val:
				print "Login is not required.."
			else:
				 time.sleep(2)
				 #print "please login to continue"
				 tn.write("root"+'\n')
				 # print "login successfully"
				 console_val = tn.read_until(':')


				 if 'Password:' in console_val:
					tn.write(''+"\n")
					log_val = tn.read_until(':')
					print log_val
			print "Login successfull..."
			cmd_essid = "cat /etc/meshap.conf"
			
			dev_obj= dev_function()
			block_str = dev_obj.read_meshapp(user,dev_filepath,ipaddress)
			sub_para = find_between(block_str, Interface_Name, "rts_th" )
			essid_dev   = re.search(r'(essid)=(\w+)',sub_para,re.I)
			essid_dev = essid_dev.group(2) 
			print " Essid is :", essid_dev
			return essid_dev 
		
			
			
			print cmd_essid
			tn.write(cmd_essid+"\n")		
			time.sleep(2)
			cmd_res = tn.read_all()
			print cmd_res
			logfile = open(dev_filepath, 'a')
			logfile.write(cmd_essid)
			logfile.close()
			dev_log = open(dev_filepath, 'w')
			dev_log.write(cmd_res+'\n')
			dev_log.close()
			matobj = re.search(verify_param+'\s*\=\s*([ -~]*)','meshapconf',re.IGNORECASE|re.MULTILINE)
			print matobj.group(1)
			if matobj:
				status = "Success"
				return status
				(matobj.group(1))
			else:
				status = "Failed"
				return status

	def get_node_name(self, Node_User,dev_filepath,NMS_Device_IP):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User ,dev_filepath,NMS_Device_IP)
		mat1=re.search(r'(name\s*=\s)(\w+)',block_str,re.I)
		name= mat1.group()
		print name
		return name
	
	def get_intf_beacon_int(self, Node_User,dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str=dev_obj.read_meshapp(Node_User,dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		beacon_int = re.search(r'(beacon_interval)=([0-9]{1,3})',sub_para,re.I)
		print " Beacon_interval is :",beacon_int.group(2)
		beacon_intv= beacon_int.group(2)  
		print beacon_intv
		return beacon_intv

	   
	def get_intf_bonding(self,Node_User,dev_filepath, NMS_Device_IP,Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User , dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security")
		bond = re.search(r'(bonding)=([sx])', sub_para, re.I)
		Bonding = bond.group(2)
		print " For interface ", Interface2_Name
		print " Beacon_interval is :",Bonding
		return Bonding


	def get_intf_essid(self,Node_User,dev_filepath, NMS_Device_IP,Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User , dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		essid_int = re.search(r'(essid)=(\w+)',sub_para,re.I)
		Essid = essid_int.group(2)
		print "for Interface ",Interface2_Name
		print " essid is :", Essid 
		return Essid 

	def get_intf_hssid(self,Node_User,dev_filepath, NMS_Device_IP,Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User,dev_filepath,NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		hide_ssid  = re.search(r'(hide_ssid)=(\d)',sub_para,re.I)
		HideSsid = hide_ssid.group(2)
		print " Hidden essid  is :",HideSsid
		return HideSsid

	def get_mesh_id(self, Node_User,dev_filepath,NMS_Device_IP):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User ,dev_filepath,NMS_Device_IP)
		print block_str
		mat1=re.search(r'(mesh_id\s*=\s)(\w+)',block_str,re.I)
		dev_mesh_id= mat1.group(2)
		print dev_mesh_id
		return dev_mesh_id

# 	def getMesh_Id(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
# 		dev_obj= dev_function()
# 		block_str = dev_obj.read_meshapp(Node_User,dev_filepath,NMS_Device_IP)
# 		sub_para = find_between(block_str, "mesh_id", "#" )
# 		print sub_para
# 		frag_thr   = re.search(r'(frag_th)=([1-9]{1,4})',sub_para,re.I)
# 		dev_mesh_id = frag_thr.group(2)
# 		print " Now the Mesh_Id is set as  :",dev_mesh_id
# 		return dev_mesh_id 
# 	
		
	#''' Fragment Threshold'''		  
	
	
	def get_intf_med_subType(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User,dev_filepath,NMS_Device_IP)
		sub_para = find_between(block_str,Interface2_Name, "security" )
		med_subTp  = re.search(r'(medium_sub_type)=((a|bg|x))',sub_para,re.I)
		medium_SubType = med_subTp.group(2)
		print " For interface ", Interface2_Name
		print " Medium Sub typ is :",medium_SubType
		return medium_SubType 
	
	
	def get_intf_med_Type(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User,dev_filepath,NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		med_Type   = re.search(r'(medium_type)=(\w)',sub_para,re.I)
		medium_Type = med_Type.group(2)
		print " For interface ", Interface2_Name
		print " Medium type is :", medium_Type
		return medium_Type 
	


	

	def get_intf_rts_thrs(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User,dev_filepath,NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		rts_thr	= re.search(r'(rts_th)=([1-9]{1,4})',sub_para,re.I)
		rts_Threshold = rts_thr.group(2)
		print " For interface ", Interface2_Name
		print " rts_thr is :", rts_Threshold
		return  rts_Threshold   

	def get_intf_service(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		serv   = re.search(r'(service)=(\d)',sub_para,re.I)
		Service=serv.group(2)
		print " For interface ", Interface2_Name
		print " Service  is :",Service
		return Service

	#Tx_power_group
	def get_intf_tx_pow(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User,dev_filepath,NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		tx_power   = re.search(r'(txpower)=([0-9]{1,3})',sub_para,re.I)
		txPower = tx_power.group(2) 
		print " For interface ", Interface2_Name
		print " tx_power is :", txPower
		return txPower 
	
	# tx_rate
	def get_intf_tx_rate(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User,dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		tx_rate	= re.search(r'(txrate)=(\d)',sub_para,re.I)
		txRate=tx_rate.group(2)
		print " tx_rate is :",txRate
		return txRate 
	
	
	
	def get_HeartbeatInterval(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		meshapconf = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
		if (Interface2_Name == "global"):
			matchObj = re.search('\s*heartbeat_interval\s+\=\s(.*)', meshapconf, re.M|re.I)
			if matchObj:
				return matchObj.group(1)
			return None	
		
	'''#usage_Tp'''
	def get_intf_usage_Tp(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		usage_Tp   = re.search(r'(usage_type)=(ds|pmon|wm|ap)',sub_para,re.I)
		usage_Type=usage_Tp.group(2)
		print " For interface ", Interface2_Name
		print " usage_Tp  is :",usage_Type
		return usage_Type
		 
	#dot11e_en 
	def get_dot11e_en(self,  Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		dot11e_en  = re.search(r'( dot11e_if_enabled)=(\d)',sub_para,re.I)
		dot11e_enabled=dot11e_en.group(2)
		print " For interface ", Interface2_Name
		print " dot11e_en is :",dot11e_enabled
		return dot11e_enabled 
	
	'''#dot11e_cat'''   
	def get_dot11e_cat(self, Node_User, dev_filepath, NMS_Device_IP, Interface2_Name):
		dev_obj= dev_function()
		block_str = dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
		dot11e_cat = re.search(r'(dot11e_if_category)=(\d)',sub_para,re.I)
		dot11e_Category = dot11e_cat.group(2) 
		print " For interface ", Interface2_Name
		print " dot11e_cat is :",dot11e_Category
		return dot11e_Category 
	
	#Get interface Channel from interface file  of device		   
	def get_intf_channel(self, Node_User, dev_obj, dev_filepath, NMS_Device_IP, Interface2_Name):	
		block_str=dev_obj.read_meshapp(Node_User, dev_filepath, NMS_Device_IP)
		sub_para = find_between(block_str, Interface2_Name, "security" )
	#	 mat = re.search(r'(eth0\s*\{)(([ -~])|\n\s.*)*',sub_para,re.IGNORECASE|re.MULTILINE)  
	#	 sub_para=mat.group()
	#	 
	#	 if mat:
		chn =re.search(r'(channel)=([0-9]{1,4})',sub_para,re.I)
		print " For interface ", Interface2_Name
		print " Channel is :",chn.group(2)
		channel = chn.group(2)
		return channel
	''' checking ping status'''
	def get_ping_status(self,user, dev_logfile, ipaddress):
		tn = telnetlib.Telnet(ipaddress)
		cmd_val = tn.read_until(':')
		if "root" in cmd_val :
			print "Login is not required.."
		else:
			 time.sleep(2)
			 #print "please login to continue"
			 tn.write("root"+'\n')
			 # print "login successfully"
			 console_val = tn.read_until(':')
			 
			 if 'Password:' in console_val:
				tn.write(''+"\n")
				log_val = tn.read_until(':')
				print log_val
		
		print "Login successfull..."
		#tn.write("ls\n")
		#tn.write("ifconfig mip0\n")
		cmd = "ping"+ ipaddress
		tn.write(cmd+"\n")		
		tn.write("exit\n")
		ping_res = tn.read_all()
		#ping_result = tn.read_until("bytes from")
		log_file  = dev_logfile+str(time.strftime('%Y_%m_%d_%H_%M', current_time))
		dev_log = open(log_file, 'w')
		dev_log.write(ping_res)
		dev_log.write('\n')
		dev_log.close()
		return ping_res
	
	''' Getting result of ifconfig '''
	def get_Ifconfig(self, user,  dev_logfile, ipaddress):	
		tn = telnetlib.Telnet(ipaddress)
		cmd_val = tn.read_until(':')
		if "root" in cmd_val :
			print "Login is not required.."
		else:
			time.sleep(2)
			#print "please login to continue"
			tn.write("root"+'\n')
			# print "login successfully"
			console_val = tn.read_until(':')


			if 'Password:' in console_val:
				tn.write(''+"\n")
				log_val = tn.read_until(':')
				print log_val

		print "Login successfull..."
		cmd1 = "ifconfig mip0"
		tn.write(cmd1+"\n")
		tn.write("exit\n")
		dev_ip_add = tn.read_all()
		#tn.write("exit\n")
		#print dev_ip_add
		tn.close()
		logfile = open(dev_logfile, 'a')
		logfile.write(dev_ip_add)
		logfile.close()
		return dev_ip_add
		
	def get_MAC_Address(self, host, user,  dev_logfile, ipaddress):
		tn = telnetlib.Telnet(ipaddress)
		cmd_val = tn.read_until('OpenWrt')
		if "root" in cmd_val :
			print "Login is not required.."
		else:
			 time.sleep(2)
			 #print "please login to continue"
			 tn.write("root"+'\n')
			 # print "login successfully"
			 console_val = tn.read_until(':')
			 
			 if 'Password:' in console_val:
				tn.write(''+"\n")
				log_val = tn.read_until(':')
				print log_val
		
		print "Login successfull..."
		
		cmd_mac = "ifconfig mip0"
		tn.write(user+"\n")
		tn.write(cmd_mac+"\n")
		tn.write("exit\n")
		time.sleep(2)
		dev_ifconfig = tn.read_all()
		
		#tn.write("exit\n")
		print dev_ifconfig
		tn.close()
		logfile = open(dev_logfile, 'a')
		logfile.write(ipaddress)
		logfile.close()
		return cmd_mac
	
	'''Fetching config value'''
	def get_from_config(self, fname, rowname, column_name):		
		
		filename = "..\\Config\\" + fname + ".csv"
		with open(filename, 'r') as f:
			file_read = csv.DictReader(f)
			flag = 0 
			res =''   
			for row in file_read:
				if (str(row['NAME'])) == (str(rowname)) :
					flag = 1
					res = row[column_name]
					break
			if flag :
				return res	
			else :
				return "Sorry Node name not found"
	''' Fetching setup value'''
	def get_from_setup(self, rowname,column_name):
		with open("..\\..\\src\\Setup\\Test1_Setup.csv", 'r') as f:
			file_read = csv.DictReader(f) 
			for row in file_read:
				if (str(row['NAME'])) == (str(rowname)) :
					res = row[column_name]
					print res,"+++++++++"
					f.close()
					return res		
	'''Checking ping result '''
	def check_positive_ping(self,pingstr):
		temppingstr=copy.copy(pingstr)
		temppingstr=temppingstr.replace('\n','')
		temppingstr=temppingstr.replace('.','')
		ReqtimeOut = re.match('(.*)(Request timed out){5,}(.*)',temppingstr,re.IGNORECASE|re.MULTILINE) 
		if ReqtimeOut:
			return "Fail" 
		DestUnreach = temppingstr.count("Destination host unreachable")
		if DestUnreach >=5 :
			return "Fail" 
		strlines = pingstr.splitlines()
		line = ''
		for line in strlines:
			if "Packets" in line:
				line = line.replace("Packets:","")
				line = line.replace(" ","")
				line = re.sub('\(.*\),$',"",line)
		return ("Success"+line)
	
	def execute_via_telnet (ipaddr, cmd):
		user= "root"
		count = 0
		tn = telnetlib.Telnet(ipaddr)
		cmd_val = tn.read_until(':')
		if "root" in cmd_val :
		    print "Login is not required.."
		else:
		    time.sleep(2)
		    #print "please login to continue"
		    tn.write("root"+'\n')
		    # print "login successfully"
		    console_val = tn.read_until(':')


		    if 'Password:' in console_val:
		        tn.write(""+"\n")
		        log_val = tn.read_until('OpenWrt')
		        #print log_val
		
		
		print "Login successfull..."  
		
		print cmd
		#time.sleep(5)
		tn.write(cmd)
		time.sleep(2)
	#         valconsole = tn.read_until('INDEX')
	#         print valconsole
		return "Pass"

		
		
		

	def config_node_via_telnet(self, interface,mac, hostip, infele, user):
		user= "root"
		print hostip
		out = ''
		tn = telnetlib.Telnet(hostip)
		cmd_val = tn.read_until(':')
		if "root" in cmd_val :
			print "Login is not required.."
		else:
			 time.sleep(2)
			 #print "please login to continue"
			 tn.write("root"+'\n')
			 # print "login successfully"
			 console_val = tn.read_until(':')


			 if 'Password:' in console_val:
				tn.write(''+"\n")
				log_val = tn.read_until(':')
				print log_val
		print "Login successfull..."
 		while out.find('#') == -1:
 			out = tn.read_very_eager()
 		time.sleep(2)
		tn.write("ifconfig mip0 \r \n")
		out = ''
		while out.find('#') == -1:
			out = tn.read_very_eager()
		print out				
		meshid = infele['mesh_id']
 		for k,v in infele.iteritems():
 			v = str(v)
 			if k.lower() == 'mesh_id':
 				cmd = "alconfset mesh_id "+ v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 					print out
  				
 				time.sleep(2)
 			if k.lower() == 'mesh_name':#change name1223 to name later
 				cmd = "alconfset name "+ v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				time.sleep(2)
 				
 			if k.lower() == 'essid':
 				cmd = "alconfset essid "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 					print out
 						
 			if k.lower() == 'channel':
 				cmd = "alconfset channel "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				
 			if k.lower() == 'rts':
 				cmd = "alconfset rts "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				
 			if k.lower() == 'frag':
				cmd = "alconfset frag "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				time.sleep(2)
 			if k.lower() == 'hidessid':
 				cmd = "alconfset hidessid "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				time.sleep(2)
 			if k.lower() == 'subtype':
 				cmd = "alconfset subtype "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				time.sleep(2)
 			if k.lower() == 'beacint':
 				cmd = "alconfset beacint "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				while out.find('#') == -1:
 					out = tn.read_very_eager()	
 				
 				time.sleep(2)
 			if k.lower() == 'dca':
 				cmd = "alconfset dca "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				
 			if k.lower() == 'preamble':
 				cmd = "alconfset preamble "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				
 			if k.lower() == 'acktime':
				cmd = "alconfset acktime "+ interface + "  " + v
 				tn.write(cmd + "\n")
 				time.sleep(2)
 				out = ''
 				while out.find('#') == -1:
 					out = tn.read_very_eager()
 				time.sleep(2)	
 
 		cmd1 = 'patcher -k ' + meshid + ' ' + mac
 		print "#########",cmd1,"#############"
 		tn.write(cmd1 + "\r\n")
 		time.sleep(2)
 		out = ''
 		while out.find('#') == -1:
 		 	out = tn.read_very_eager()
 		
 		cmd = 'reboot\n'
 		tn.write(cmd + "\r\n")
 		time.sleep(2)
 		return
 	
 	
 	''' Funtion to fetch the preferred parent field from meshapp.conf'''
 	def Get_Preferred_Parent(self, telnet_user, dev_filepath, NMS_Device_IP, interface):
 		dev_obj = dev_function()
		meshapconf = dev_obj.read_meshapp(telnet_user , dev_filepath, NMS_Device_IP)
		
		matchObj = re.search( '\s*preferred_parent\s+\=\s(.*)', meshapconf, re.M|re.I)
		if matchObj:
			return matchObj.group(1)
		return None
 	
 	''' Funtion to fetch the Country Code parent field from meshapp.conf'''
	def Get_Country_Code(self, telnet_user, dev_filepath, NMS_Device_IP, interface): 
		dev_obj = dev_function()
		meshapconf = dev_obj.read_meshapp(telnet_user, dev_filepath, NMS_Device_IP)
		if (interface == "global"):
			matchObj = re.search( '\s*country_code\s+\=\s(.*)', meshapconf, re.M|re.I)
			if matchObj:
				return matchObj.group(1)
			return None	
		
	def Get_Description(self,telnet_user, dev_filepath, NMS_Device_IP, interface="global"):
		dev_obj = dev_function()
		
		meshapconf = dev_obj.read_meshapp(telnet_user, dev_filepath, NMS_Device_IP)
		if (interface == "global"):
			matchObj = re.search('\s*description\s+\=\s(.*)', meshapconf, re.M|re.I)
			if matchObj:
				return matchObj.group(1)
			return None
	def Get_Heartbeat_Interval(self, telnet_user, dev_filepath, NMS_Device_IP, interface="global"):
		dev_obj = dev_function()
		meshapconf = dev_obj.read_meshapp(telnet_user, dev_filepath, NMS_Device_IP)
		if (interface == "global"):
			matchObj = re.search('\s*heartbeat_interval\s+\=\s(.*)', meshapconf, re.M|re.I)
			if matchObj:
				return matchObj.group(1)
			return None
	
	def get_from_ACL(self, user, dev_filepath, ipaddress, node_hostname):
		print ipaddress
		tn = telnetlib.Telnet(ipaddress)
		cmd_val = tn.read_until(node_hostname)
		if "root" in cmd_val :
		    print "Login is not required.."
		else:
		    time.sleep(2)
		    #print "please login to continue"
		    tn.write("root"+'\n')
		    time.sleep(2)
		    # print "login successfully"
		    console_val = tn.read_until(':',10)


		    if 'Password:' in console_val:
		        tn.write("\n")
		        #log_val = tn.read_until(':~#')
		        #print log_val
		
		
		print "Login successfull..."
		
		
		print "Now sending acl cat cmd"
		acl_info = "cat /etc/acl.conf"
		tn.write(acl_info+'\n')  
		tn.write("exit\n")
		print "cmd entered"
		
		data = ''
		while data.find('acl_info') == -1:
		    data = tn.read_until(':~#', 10)
		    #print "Data value is ", data
		#print data			
		return data
