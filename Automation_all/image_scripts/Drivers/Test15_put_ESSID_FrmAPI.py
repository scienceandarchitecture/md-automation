"""
essid with iw cmd used only for v_wlan0 and wlan2
"""
import pandas as pd
import unittest
import csv
from Common_lib  import *
from dev_function import *
import re
import datetime
import json
import time
from dev_function import *
from iwdev_function import *




def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return "Error"


def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'a+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+str(NMS_Param)])
    outp.writerow(["Test Execution Status"+':'+str(test_status)])
    outp_file.close()
    return

def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//putInterface.csv")
    #df = pd.read_csv("..//..//src//Test_Data//Config//GeneralConfiguration.csv")
    #d_excel("config_data.csv")
    tc = 0        
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    Supported_protocol = df.Supported_protcol[tc]
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]

    test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition, Supported_protocol, Interface_Name, Network_Name, API_Server_IP]
                 
    return test_params

def Setup_Parser_method(tc):
        count = 0
      	while count<= tc:
    		if count == tc:        
    			data = pd.read_csv("..//Data//Test1_Setup.csv")
            
    			NAME = data.NAME[count]
    			MAC = data.MAC[count]
    			Dev_IP =  data.IP[count]
    			TYPE = data.TYPE[count]
    			MODEL = data.MODEL[count]
    			NODE_USER = data.USER_NAME[count]
    			setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
    			return setup_values


		count = count+1                        
class Test15_put_ESSID_FrmAPI(object):
    def init(self):
        self.object = object
    def test15_put_ESSID_FrmAPI(self,intf1, tc):
        #try:
            val = Setup_Parser_method(tc)
            NAME = val[0]
            Dev_MAC_Address = val[1]
            NMS_Device_IP = val[2]
            dev_Type = val[3]
            Model = val[4]
            Node_User= val[5]
        
            api_data = Input_Data_Parser(tc)
            api_data[0]
            API_Name = api_data[0]
            Method = api_data[1]
            Content_Type = api_data[2]
            Arg1 = api_data[3]
            ''' Arg1 is Essid'''
            
            Exp_st_code= api_data[4]
            Precondition = api_data[5]
            Supported_protocol= api_data[6]
            #Interface_Name = api_data[7]
            Interface_Name = intf1
            Network_Name = api_data[8]
            API_Server_IP= api_data[9]
    
            #http://localhost:8080/NMS/updateGeneralConfiguration?networkName=AutoTestRun&macAddress=30:14:4A:EA:88:61
            
            Data_value = '{'+ '"interfaceName"'+':'+'"'+Interface_Name+'"'+','+'"essid"'+':'+'"'+Arg1+'"'+'}'
            #Data_value = '"'+'{'+'"name"+':'+' '"' +Interface_Name+'"'+":"+"essid" +":"+'"'+ Arg1+'"'+'}'
            print Data_value
            #http://localhost:8080/NMS/getGeneralConfiguration?networkName=default&macAddress=30:14:4A:D8:89:49
            Verify_param = Arg1
            url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?networkName="+Network_Name+"&deviceType="+dev_Type+"&macAddress="+Dev_MAC_Address
            print url
            essid_NMS=Arg1
            Dev_IP= NMS_Device_IP 
            print "Test Details: "
            #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
            s= CommonOperations()
            tc_strt = datetime.datetime.now()            
            p=s.commonOperations(tc, Method, url, Content_Type, Data_value, Exp_st_code, Precondition, Supported_protocol, Data_value, Interface_Name, Network_Name, API_Server_IP) 
            op = json.dumps(p,ensure_ascii=True)
            op1 = json.loads(op)
            if op.find("interface data updated successfully"):
                print "ESSID value set through API is =", Arg1
            else:
                print "API updation failed"

            current_time = time.localtime()    
            filename = str(time.strftime('%Y_%m_%d_%H_%M', current_time))
            nms_filename = "Test_NMS_UpdateESSID_"+filename
            nms_filepath = ".//Output//"+nms_filename+'.csv'
            nms_log = open(nms_filepath, 'w')
            nms_log.write(op)
            nms_log.close()
            print "NMS output saved in logfile "
            print "                           \n"
            """ Verification of Device Data """
            print "Device Data Verification Starts Now.."
            
            dev_Logfilename =  "Test_Result"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
            dev_filepath = ".//Output//"+dev_Logfilename
            verify_param = Arg1
            telnet_user = "root"
            dev_obj = dev_function()
            dev_essid =dev_obj.essid_verification(telnet_user, dev_filepath,Dev_IP, verify_param, Interface_Name)
            
            if str(dev_essid)==str(essid_NMS):
                print "Value of essid on NMS and Device, matched successfully.."
                test_status1 = "Pass"
                addOutput(dev_filepath,op, Arg1, tc,test_status1)
                #return test_status1         
            else:
                print "Value of essid doesn't matched ..."
                test_status1 = "Fail"
                addOutput(dev_filepath,op, Arg1, tc,test_status1)
               # return test_status1
            
            print "-----------------------------------------------------------"
            print "Verifying device values from Iwdev "
            iw_obj = iwdev_function()
            essid_iw =iw_obj.read_iw_v_wlan0(Node_User,dev_filepath, NMS_Device_IP, intf1)
            mat = re.search(r'(ssid\s)(\w+)',essid_iw)
            #ssid StructuredMesh_v_wlan0
            if mat:
                essid_iw = mat.group(2)
            print essid_iw
                       
            
            if str(essid_NMS) == str(essid_iw):
                print "Success, essid On NMS and the value from IW command Matched.."
                test_status2 = 1
                addOutput(dev_filepath,op, essid_iw, tc, test_status2)
                return test_status2                    
            else:
                print "Failed, essid On NMS and the value from IW command not Matched.."
                test_status2 = 0
                addOutput(dev_filepath,op, essid_iw, tc, test_status2)
                return test_status2
            
            if test_status1 > 0 &  test_status2 >0:
                print "Success, essid On NMS, IW command & Device Matched.."
                print "testcase passed"
            else:
                print "testcase failed"
                print "Failed, essid On NMS & IW not Matched.."
                
            """ Validating iw output and if it matches then test status2 to be set as 1 else 0
            finally if test staTUS 2  AND TESTSTATUS1 > O THEN TC PASS ELSE FAIL. """
            
            test_status = ['test_status1','test_status2'] 
            return test_status            
#         except Exception as e:
#             #print ("Error ....sys.exec_info()[0], has occured....")
#             print("Data is not received or Check the Connectivity")
#             print(e)
#             return "Fail"