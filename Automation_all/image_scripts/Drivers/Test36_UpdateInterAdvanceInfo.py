import json
import datetime
import time
import pandas as pd
import csv
#from test.test_descrtut import defaultdict
import Common_lib
import dev_function
from Common_lib  import CommonOperations
from dev_function import *
import re


def find_between_including( s, first, last ):
    try:
        start = s.index( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return "Error"

def read_vlan_mesh(Node_User,dev_filepath, NMS_Device_IP, Vlan_Name):
    print Vlan_Name
    dev_obj= dev_function()
    block_str=dev_obj.read_meshapp(Node_User,dev_filepath, NMS_Device_IP)
    sub_para = find_between_including(block_str, Vlan_Name, "none" )
    return sub_para

def addOutput(filepath,op, NMS_Param, tc, test_status):
    outp_file = open(filepath, 'w+')
    outp = csv.writer(outp_file)
    outp.writerow(["Test_No: "+str(tc)])
    outp.writerow(["Device output"+':'+op])
    outp.writerow(["NMS output"+':'+str(NMS_Param)])
    outp.writerow(["Test Execution Status"+':'+test_status])
    outp_file.close()
    return



def Input_Data_Parser(tc):
    df = pd.read_csv("..//API//putInterf_Advanceinfo.csv")
    #d_excel("config_data.csv")
    tc = 0
    API_Name = df.API_Name[tc]    
    Method = df.Method[tc]    
    Content_Type = df.Content_Type[tc]    
    arg1= df.arg1[tc]    
    Exp_st_code = df.Exp_st_code[tc]    
    Precondition = df.Precondition[tc]    
    mediumSubType = df.mediumSubType[tc]
    Interface_Name = df.Interface_Name[tc]
    Network_Name = df.Network_Name[tc]
    API_Server_IP = df.API_Server_IP[tc]
    channelBandwidth = df.channelBandwidth[tc]
    secondaryChannelPosition = df.secondaryChannelPosition[tc]
    guardInterval_40 = df.guardInterval_40[tc]
    guardInterval_20 = df.guardInterval_20[tc]
    MaxAMPDU = df.MaxAMPDU[tc]
    MaxAMSDU = df.MaxAMSDU[tc]
    txSTBC = df.txSTBC[tc]
    rxSTBC = df.rxSTBC[tc]
    ldpc = df.ldpc[tc]
    gfMode = df.gfMode[tc]
    coexistence = df.coexistence[tc]
    test_params= [API_Name, Method, Content_Type, arg1, Exp_st_code, Precondition,mediumSubType, Interface_Name, Network_Name, API_Server_IP,  channelBandwidth, secondaryChannelPosition, guardInterval_40, guardInterval_20, MaxAMPDU, MaxAMSDU, txSTBC, rxSTBC, ldpc, gfMode, coexistence]
                 
    return test_params

def Setup_Parser_method(tc):
	count = 0
	while count<= tc:
		if count == tc:
			data = pd.read_csv("..//Data//Test1_Setup.csv")
			NAME = data.NAME[count]
			MAC = data.MAC[count]
			Dev_IP =  data.IP[count]
			TYPE = data.TYPE[count]
			MODEL = data.MODEL[count]
			NODE_USER = data.USER_NAME[count]
			setup_values=[NAME, MAC, Dev_IP, TYPE, MODEL,NODE_USER]
			return setup_values
		count = count+1


"""
The Addvance info (ht and vht capabilities are updated using nms api and
then re-checking the value on Device after restarting the node """              
          
class Test36_UpdateInterAdvanceInfo(object):
    def init(self):
        self.object = object
    
    def test36_UpdateInterAdvanceInfo(self, intf1, tc):
        try : 
            val = Setup_Parser_method(tc)
    	    NAME = val[0]
    	    Dev_MAC_Address = val[1]
    	    NMS_Device_IP = val[2]
    	    Type = val[3]
    	    Model = val[4]
    	    Node_User= val[5]
    	    api_data = Input_Data_Parser(tc)
    	    api_data[0]
    	    API_Name = api_data[0]
    	    Method = api_data[1]
    	    Content_Type = api_data[2]
    	    Arg1 = api_data[3]
    	    Exp_st_code = api_data[4]
    	    Precondition = api_data[5]
    	    mediumSubType = api_data[6]
            Interface_Name = intf1 
    	    Network_Name = api_data[8]
    	    API_Server_IP= api_data[9]
    	    channelBandwidth = api_data[10]
    	    secondaryChannelPosition = api_data[11]
    	    guardInterval_40 = api_data[12]
    	    guardInterval_20 = api_data[13]
    	    MaxAMPDU = api_data[14]
    	    MaxAMSDU = api_data[15]
    	    txSTBC = api_data[16]
    	    rxSTBC = api_data[17]
    	    ldpc = api_data[18]
    	    gfMode = api_data[19]
    	    coexistence = api_data[20]
    	    
    	    
    	    url = "http://"+API_Server_IP+":8080"+"/NMS/"+API_Name+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
    	    print url
            Arg1 = Arg1+Interface_Name
    	    Data_value = '{ '+ '"interfaceName"'+': '+ '"'+(Interface_Name)+'"'+ ','+ '"mediumSubType"'+': '+ str(mediumSubType)+ ' , ' \
    	    + '"advanceInfo"' +' : ' +' { '+ '"channelBandwidth"'+': '+ str(channelBandwidth)+ ',' \
    	    +'"secondaryChannelPosition"'+': '+str(secondaryChannelPosition)+ ',' \
    	    +'"guardInterval_40"'+': '+str(guardInterval_40)+ ','+ '"guardInterval_20"'+': '+str(guardInterval_20) + ',' \
    	    +'"MaxAMPDU"'+': '+str(MaxAMPDU) + ','+'"MaxAMSDU"'+': '+str(MaxAMSDU)+ ',' + \
    	    '"txSTBC"'+': '+str(txSTBC)+',' + '"rxSTBC"'+': '+str(rxSTBC)+ ','+ '"ldpc"'+': '+str(ldpc)+ ',' + \
    	    '"gfMode"'+': '+str(gfMode)+', '+'"coexistence"'+': '+str(coexistence) +' } ' + ' } '
    	    print Data_value 
    	    
    	    Dev_IP= NMS_Device_IP 
    	    verify_api_param = 'preferedParent'
    	    #print "Test_No : {0}, Test_Name : {1},  Method : {2} : ".format(tc, Test_Name, method)
    	    current_time = time.localtime()
    	    s= CommonOperations()
    	    tc_strt = datetime.datetime.now()            
    	    """here instead of interface name we have modified the common 
    	    operation and instead sending the PP"""
    	    #tc, Method, url, Content_Type, Data_value, Exp_st_code, Precondition, Supported_protocol, Data_value, Interface_Name, Network_Name, API_Server_IP)
    	    p=s.commonOperations(tc, Method, url, Content_Type, Data_value, Arg1, Exp_st_code, mediumSubType, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
    	    op = json.dumps(p,ensure_ascii=True)
    	    op1 = json.loads(op)
    	    #if op.find('"rebootRequired":YES') | op.find('"rebootRequired":"YES"'):
    	    #if re.search(r''["rebootRequired":"Yes"',op1, flags=re.IGNORECASE) or re.search('rebootRequired":Yes' ,op1, flags=re.IGNORECASE):
    	    if re.search(r'["rebootRequired":YES | "rebootRequired":"YES"]', op, re.IGNORECASE):
    		print "Rebooting the node..."
    		rebootAPI="reboot"
    		RebootMethod = "POST"
    		url = "http://"+API_Server_IP+":8080"+"/NMS/"+rebootAPI+"?networkName="+Network_Name+"&macAddress="+Dev_MAC_Address
    		print "Rebooting Node, please wait till node gets Up"
    		print url
    		s= CommonOperations()
    		tc_strt = datetime.datetime.now()
    		p=s.commonOperations(tc, RebootMethod, url, Content_Type, Data_value, Arg1, Exp_st_code, Exp_st_code, verify_api_param, Interface_Name, Network_Name, API_Server_IP) 
    		op = json.dumps(p, ensure_ascii=True)
    		op1 = json.loads(op)
    		
    		if op1.find('"Node rebooted  successfully"'):
    		    print "Node Rebooting Now, please wait while node reboots..."
    		devobj = dev_function()
    		reboot_Node = devobj.check_node_status(NMS_Device_IP)
    		print reboot_Node
    		if reboot_Node=="Success":
    		    print "Node Rebooted successfully..."
    		    
    		    #dev_Logfilename = "Test21_UpdateCountryCode_NMS_Output"+str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
    		
    	    elif re.search(r'["rebootRequired":"No" |"rebootRequired":No]', op, flags=re.IGNORECASE):
    		print "reboot is not required"
    		        
    	    """ Verification of Device Data """
    	    print "Device Data Verification Starts Now.."
    	    dev_Logfilename = "Test36_UpdateInterAdvanceInfo_NMS_Output" +str(time.strftime('%Y_%m_%d_%H_%M', current_time))+".txt"
    	    # dev_filepath = "..//Output//"+"output.txt"
    	    dev_filepath = "..//Output//"+dev_Logfilename  
    	    telnet_user = "root"
    	    interface = "global"
    	    devobj = dev_function()
    	    block_str = devobj.read_meshapp(Node_User,dev_filepath,NMS_Device_IP)
    	    ht_cap = find_between_including(block_str, Interface_Name, "vht_capab" )
    	    
    	    
    	#     ht_cap = find_between_including(sub_para,"ht_capab","vht_capab")
    	#     print ht_cap
    	    
    	    dev_ldpc1 = re.search(r'(ldpc)\=(\w+)',ht_cap)
    	    dev_ldpc = dev_ldpc1.group(2)
    	    print "ldpc:", dev_ldpc 
    	    if int(ldpc) ==  1:
    		if dev_ldpc.strip() == "enabled":
    		    print "dev_ldpc matched with the set value of ldpc"
    		else:
    		    print "The ldpc value is not matching with the ldpc value set on NMS.."
    		    
    	    if int(ldpc) == 0:
    		if dev_ldpc.strip() == "disabled":
    		    print "dev_ldpc matched with the set value of ldpc on NMS"
    		else:
    		    print "The ldpc value is not matching with the ldpc value set on NMS.."
    
    		    
    	    print "\n"
    	    dev_ht_band = re.search(r'(ht_bandwidth)\=(\w+)',ht_cap)
    	    dev_ht_band = dev_ht_band.group(2)
    	    
    	    print "The accepted value for Channel bandwitdth is 1,2, 3 and 3 is for 80 Mhz which is not supported currently.."
    	    channelBandwidth = 10 * channelBandwidth
    	    
    	    print  "ht_bandwidth:",  dev_ht_band
    	    if int(channelBandwidth) == int(dev_ht_band):
    		print "Channel Bandwidth value matched with the value on device..."
    	    else:
    		print "Channel bandwidth not matcing with the value on device..."
    	    
    	    print "\n"
    	    dev_gfmode = re.search(r'(gfmode)\=(\w+)',ht_cap)
    	    dev_gfmode = dev_gfmode.group(2)
    	    if gfMode == 1:
    		if dev_gfmode == "enabled":
    		    print "GFMODE matched with the set value of gfmode"
    		else:
    		    print "The GFMODE value is not matching with the gfmode value set on NMS.."
    		    
    	    if gfMode == 0:
    		if dev_gfmode == "disabled":
    		    print "GFMODE matched with the set value of gfMode on NMS"
    		else:
    		    print "The GFMODE value is not matching with the gfMode value set on NMS.."
    
    	    print "\n"
    	    
    	    
    	    
    	    if channelBandwidth == 2:
    		dev_gi_20 = re.search(r'(gi_20)\=(\w+)',ht_cap)
    		dev_gi_20 = dev_gi_20.group(2)
    		print " GuardInterval_20 value is ",dev_gi_20
    	    
    	    
    		if guardInterval_20 ==2 :
    		    if dev_gi_20 == "short":
    		        print "Vaalues matched on Device and NMS..for value 2,  NMS should display short ..."
    		    else:
    		        print "Values not matching on device and NMS, for value 2,  NMS should display short"
    	    
    		if guardInterval_20 ==1 :
    		    if dev_gi_20 == "auto":
    		        print "Values matched on Device and NMS..for value 0,  NMS should display Auto ..."
    		    else:
    		        print "Values not matching on device and NMS, for value 0,  NMS should display Auto"
    		
    		if guardInterval_20 ==0 :
    		    if dev_gi_20 == "long":
    		        print "Vaalues matched on Device and NMS..for value 1,  NMS should display long ..."
    		    else:
    		        print "Values not matching on device and NMS, for value 1,  NMS should display long"
    		
    		print "\n"
    		
    	    
    	    if channelBandwidth ==1:
    		dev_gi_40 = re.search(r'(gi_40)\=(\w+)',ht_cap)
    		dev_gi_40 = dev_gi_40.group(2)
    		print  "gi_40:",  dev_gi_40
    		
    		if guardInterval_40 ==2 :
    		    if dev_gi_40 == "short":
    		        print "Vaalues matched on Device and NMS..for value 2,  NMS should display short ..."
    		    else:
    		        print "Values not matching on device and NMS, for value 2,  NMS should display short"
    	    
    		if guardInterval_40 ==0 :
    		    if dev_gi_40 == "auto":
    		        print "Values matched on Device and NMS..for value 0,  NMS should display Auto ..."
    		    else:
    		        print "Values not matching on device and NMS, for value 0,  NMS should display Auto"
    		
    		if guardInterval_40 ==1 :
    		    if dev_gi_40 == "long":
    		        print "Vaalues matched on Device and NMS..for value 1,  NMS should display long ..."
    		    else:
    		        print "Values not matching on device and NMS, for value 1,  NMS should display long"
    	       
    		print "\n"
    	    
    	    dev_tx_stbc = re.search(r'(tx_stbc)\=(\w+)',ht_cap)
    	    dev_tx_stbc = dev_tx_stbc.group(2)
    	    print "tx_stbc:",dev_tx_stbc
    	    
    	    print "\n"
    	    
    	    if txSTBC == 1:
    		if dev_tx_stbc == "enabled":
    		    print "txSTBC matched with the set value of txSTBC"
    		else:
    		    print "The txSTBC value is not matching with the txSTBC value set on NMS.."
    		    
    	    if txSTBC == 0:
    		if dev_tx_stbc == "disabled":
    		    print "txSTBC matched with the set value of txSTBC on NMS"
    		else:
    		    print "The ldpc value is not matching with the ldpc value set on NMS.."
    
    	    print "\n"
    	    
    	    dev_rx_stbc = re.search(r'(rx_stbc)\=(\w+)',ht_cap)
    	    dev_rx_stbc = dev_rx_stbc.group(2)
    	    print "rx_stbc:",dev_rx_stbc
    	    if rxSTBC == 1:
    		if dev_rx_stbc == "enabled":
    		    print "rxSTBC matched with the set value of rxSTBC"
    		else:
    		    print "The rxSTBC value is not matching with the rxSTBC value set on NMS.."
    		    
    	    if rxSTBC == 0:
    		if dev_tx_stbc == "disabled":
    		    print "rxSTBC matched with the set value of RxSTBC on NMS"
    		else:
    		    print "The rxSTBC value is not matching with the RxSTBC value set on NMS.."
    
    	    
    	    
    	    print "\n"
    	    dev_max_amsdu_len = re.search(r'(max_amsdu_len)\=(\w+)',ht_cap)
    	    dev_max_amsdu_len = dev_max_amsdu_len.group(2)
    	    print "max_amsdu_len:", dev_max_amsdu_len
    	    
    	    if int(MaxAMSDU) == int(dev_max_amsdu_len):
    		print "MaxAMSDU value on device and nms matched..."
    	    else:
    		"Max AMSDU value not matching on device and NMS..."
    	    
    	    print "\n"
    	    dev_max_ampdu_len = re.search(r'(max_ampdu_len)\=(\w+)',ht_cap)
    	    dev_max_ampdu_len = dev_max_ampdu_len.group(2)
    	    print "max_ampdu_len is :", dev_max_ampdu_len
    	    if int(MaxAMPDU) == int(dev_max_ampdu_len):
    		print "MaxAMPDU value on device and nms matched..."
    	    else:
    		"Max AMSDU value not matching on device and NMS..."
        
        
        
        except Exception as e:
            print(e)
                
